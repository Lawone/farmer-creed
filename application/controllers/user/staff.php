<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('staff_model'));
	}
	
	// Registration
	public function index() {
		//$this->load->view('user/sorting_view');
	}
	
	//user add for notification
	public function add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('firstname', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('username', 'Login ID', 'trim|required|callback_alreadyExistUserName|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|sha1');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->staff_model->add();
			$this->session->set_flashdata('message', 'Task has been added successfully.');
			redirect(base_url().'user/staff/add', 'refresh');
		}
		
		$this->load->view('user/staff_add');
	}
	
	//user edit for notification
	public function edit($id) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('firstname', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('username', 'Login ID', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|sha1');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->staff_model->edit($id);
			$this->session->set_flashdata('message', 'Staff details has been changed successfully.');
			redirect(base_url().'user/staff/view', 'refresh');
		}
		$data['staff_e']=$this->staff_model->getTask($id);
		$this->load->view('user/staff_edit',$data);
	}
	
	// domain view method
	public function view() {
		
		$this->db->select('*')->from('tbl_users')->where('parent_uid', $this->session->userdata('uid'));
		
		// data subset
		$query = $this->db->get();
		$data['staff_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/staff_view', $data);
	}
	
	//user delete method
	public function delete($id){
		
		$this->staff_model->delete($id);
		$this->session->set_flashdata('message', 'Staff has been deleted successfully.');
		redirect(base_url().'user/staff/view', 'refresh');
	}
	
	public function alreadyExistUserName()
	{
		$this->db->select('username')->from('tbl_users')->where(array('username' => $this->input->post('username')));
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$this->form_validation->set_message('alreadyExistUserName', 'Staff UserName already exists.');
			return false;
		}else{
			return true;
		}
	}
	
}
