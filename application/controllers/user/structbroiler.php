<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Structbroiler extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('structbroiler_model'));
	}
	
	// Registration
	public function index() {
		//$this->load->view('user/sorting_view');
	}
	
	//user add for notification
	public function add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('pen_name','Pen Name','trim|required|callback_alreadyExistPenName|xss_clean');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('size', 'Size', 'trim|required|xss_clean');
				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->structbroiler_model->add();
			$this->session->set_flashdata('message', 'Broiler has been added successfully.');
			redirect(base_url().'user/structbroiler/add', 'refresh');
		}
		
		$this->load->view('user/structbroiler_add');
	}
	
	//user edit for notification
	public function edit($uid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('pen_name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('size', 'Size', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->structbroiler_model->edit($uid);
			$this->session->set_flashdata('message', 'Broiler has been changed successfully.');
			redirect(base_url().'user/structbroiler/view', 'refresh');
		}
		$data['structbroiler_e']=$this->structbroiler_model->getStructbroiler($uid);
		$this->load->view('user/structbroiler_edit',$data);
	}
	
	// domain view method
	public function view() {
		$this->db->select('*')->from('tbl_structure_broiler')->where('uid', $this->session->userdata('uid'));
		
		// data subset
		$query = $this->db->get();
		$data['structbroiler_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/structbroiler_view', $data);
	}
	
	//user delete method
	public function delete($sbid){
		
		$this->structbroiler_model->delete($sbid);
		$this->session->set_flashdata('message', 'Broiler has been deleted successfully.');
		redirect(base_url().'user/structbroiler/view', 'refresh');
	}
	
	public function alreadyExistPenName()
	{
		$this->db->select('pen_name')->from('tbl_structure_broiler')->where(array('pen_name' => $this->input->post('pen_name'),'uid' => $this->session->userdata('uid')));
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$this->form_validation->set_message('alreadyExistPenName', 'Pen Name already exists.');
			return false;
		}else{
			return true;
		}
	}
	
}
