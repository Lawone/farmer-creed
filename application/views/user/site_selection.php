<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>

</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px" >

			<!-- MAIN CONTENT -->
			<div id="content" class="container" >

				<div class="row" >
					<!-- NEW COL START -->
					<?php
					if($this->session->flashdata('message')): echo '<div class="alert alert-success" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
					if($this->session->flashdata('error_message')): echo '<div class="alert alert-danger" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('error_message').'</div>'; endif;
		?>
					<center>
						<article class="col-sm-12 col-md-12 col-lg-6" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
				
							<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2 style="font-size: 100%">Please answer the questions to determine the  suitablability of your location for farming.</h2>
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >
							<fieldset>
									<div>
									<section class="col col-6">
									<label>Access to a reliable source of water?</label>
									</section>
									<section class="col col-6">
									 <label>
									 <input type="checkbox" name="access_reliable_source_water" id="access_reliable_source_water"  value="1" <?php echo set_checkbox('access_reliable_source_water', '1',(['access_reliable_source_water']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
									<section class="col col-6">
									<label>Is the water of Good Quality?</label>
									</section>
									<section class="col col-6">
										<label>
										<input type="checkbox" name="waterof_good_quality" id="waterof_good_quality"  value="1" <?php echo set_checkbox('waterof_good_quality', '1',(['waterof_good_quality']=='1') ? TRUE : FALSE); ?>/>
											</label>
										</section>
									<section class="col col-6">
									<label>Access to drainage for waste water?</label>
									</section>
									<section class="col col-6">
										<label>
											<input type="checkbox" name="access_drainage_waster_water" id="access_drainage_waster_water"  value="1" <?php echo set_checkbox('access_drainage_waster_water', '1',(['access_drainage_waster_water']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
									<section class="col col-6">
									<label>Are you in close proximity to market?</label>
									</section>
									<section class="col col-6">
										<label>
										<input type="checkbox" name="close_proximity_market" id="close_proximity_market "  value="1" <?php echo set_checkbox('close_proximity_market', '1',(['close_proximity_market']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
									<section class="col col-6">
									<label>Secured from pilfering and predators?</label>
									</section>
									<section class="col col-6">
										<label>
										<input type="checkbox" name="secured_pilfering_predators" id="secured_pilfering_predators"  value="1" <?php echo set_checkbox('secured_pilfering_predators', '1',(['secured_pilfering_predators']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>			
										</div>
										</fieldset>
																	<footer>
								<a class="btn btn-primary" href="<?php echo base_url().'user/settings/structure_selection'?>">Next</a>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" class="btn btn-default" type="submit">
													Submit
												</button>
							</footer>
										</form>
										
										
<!--<script type="text/javascript">
		
			runAllForms();

			$(function() {
			
			
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						access_reliable_source_water  : {
							required : true
													},
						waterof_good_quality : {
							required : true
													},
						access_drainage_waster_water : {
							required : true
													},
						close_proximity_market : {
							required : true
							},
						secured_pilfering_predators : {
							required : true
							}
					},

					// Messages for form validation
					messages : {
						access_reliable_source_water  : {
							required : 'Please Enter The Access to a reliable source of water',
							},
						waterof_good_quality : {
							required : 'Please Enter The Is the water of Good Quality',
							},
						access_drainage_waster_water : {
							required : 'Please Enter The Access to drainage for waste water',
							},
						close_proximity_market : {
							required : 'Please Enter The Are you in close proximity to market',
							},
						secured_pilfering_predators : {
							required : 'Please Enter The Secured from pilfering and predators',
								}
						},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>-->