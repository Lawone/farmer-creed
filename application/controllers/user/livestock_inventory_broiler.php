<?php 
error_reporting(0);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class livestock_inventory_broiler extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('broilersales_model'));
	}
	
	// Registration
	public function index() {
		
	}
		
	// domain view method
	public function broiler_view() {
            
	    if(isset($_REQUEST['search'])){
			$f_arr = array();
                 $date_arr = array();
                 $sql_query = "SELECT SUM( `mortality` ) as  mortality,date(created_date) as created_date FROM `tbl_broiler_mortality` WHERE `uid` = '".$this->session->userdata('uid')."' AND ";
                 		 
                 $sales = "SELECT sum(poultry) as total_cost,date(created_date) as created_date FROM `tbl_broilersales` WHERE  `uid` = '".$this->session->userdata('uid')."' AND ";
                 $purchase="SELECT sum(total_number) as total_purchase,date(created_date) as created_date FROM `tbl_chickpurchased` WHERE  `uid` = '".$this->session->userdata('uid')."' AND ";
                 
                 if((isset($_POST['pen_name']))  &&  (empty($_POST['start-date']))  &&  (empty($_POST['end-date'])) ){
                    $sql_query .=  "  `sbid` = '".$_POST['pen_name']."' ";
                   
                    $sales .= " pen_name='".$_POST['pen_name']."' ";
                    //$totalstocked="SELECT (`total_stocked`+`stock_addition`)- (`stock_deletion`+`mortality_addition`) as total_stocked FROM `tbl_livestock_catfish` WHERE scid='".$_POST['pen_name']."'";
                    
   		    $purchase .=" pen_name='".$_POST['pen_name']."'";
                    
                 }else if((!empty($_POST['pen_name'])) &&  (!empty($_POST['start-date']))  &&  (!empty($_POST['end-date']))) {
          
                    $sql_query .=  "   (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
                                        
                    $sales .=" pen_name='".$_POST['pen_name']."' AND (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
   		    $purchase .=" pen_name='".$_POST['pen_name']."' AND (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."') ";
                 }
                 
                    $sql_query  .= " GROUP BY date( `created_date` )";
                    $sales  .= " GROUP BY date( `created_date` )";
                    $purchase  .= " GROUP BY date( `created_date` )";
                    //echo "<br />";
                    $query = $this->db->query($sql_query);
                    $result = $query->result_array();
                    
                    //$totalstockedquery= $this->db->query($totalstocked);
                    //$totalstockedresult = $totalstockedquery->result_array();
		    //$data['totalstockedresult_v'] = $totalstockedresult;
                    
                    $mortality_present = 0;
                    $total_purchase_present = 0;
                    $total_cost_present = 0;
                    
                    
                    
                    foreach ($result as $key => $value) {
                        $now_count = count($f_arr);
                        $date_arr[$now_count] = $value['created_date'];
                        $f_arr[$now_count]['date'] = $value['created_date'];
                        $mortality_present += $f_arr[$now_count]['mortality'] = $value['mortality'];
						$f_arr[$now_count]['total_purchase'] = 0;
						$f_arr[$now_count]['total_cost'] = 0;
                    }
                    

		// sales result 
                    $sales_query = $this->db->query($sales);
                    $salesresult = $sales_query->result_array();
                   // print_r($incomeresult);
                    	
                    foreach ($salesresult as $key => $value) {
                        

			 if(in_array($value['created_date'], $date_arr)){
                            $now_count = array_search ($value['created_date'], $date_arr);
                        }  else {
                            $now_count = count($f_arr);
                            $date_arr[$now_count] = $value['created_date'];
                            $f_arr[$now_count]['mortality'] = 0;
                            $f_arr[$now_count]['total_purchase'] = 0;
                        }

                        
                        $f_arr[$now_count]['date'] = $value['created_date'];
                        $total_cost_present += $f_arr[$now_count]['total_cost'] = $value['total_cost'];


                        
 			
                        
                    }
                    // purchase result 
                   
                    $purchase_query = $this->db->query($purchase);
                    $purchaseresult = $purchase_query->result_array();
                   // print_r($incomeresult);
                    	
                    foreach ($purchaseresult as $key => $value) {
                        
                        //$now_count = count($f_arr);
                        //$date_arr[$now_count] = $value['created_date'];
			
			
			 if(in_array($value['created_date'], $date_arr)){
                            $now_count = array_search ($value['created_date'], $date_arr);
                        }  else {
                            $now_count = count($f_arr);
                            $date_arr[$now_count] = $value['created_date'];
                            
                            $f_arr[$now_count]['mortality'] = 0;
                            $f_arr[$now_count]['total_cost'] = 0;
                        }
                        $f_arr[$now_count]['date'] = $value['created_date'];
                        $total_purchase_present += $f_arr[$now_count]['total_purchase'] = $value['total_purchase'];


                        
                    }
                    
                    $totalstocked="SELECT `total_stocked` FROM `tbl_livestock_broiler` WHERE  `uid` = '".$this->session->userdata('uid')."' AND sbid='".$_POST['pen_name']."'";
                    $totalstockedquery= $this->db->query($totalstocked);
                    $totalstockedresult = $totalstockedquery->result_array();
		    $data['totalstockedresult_v'] = $totalstockedresult;

                    $present_stock = $totalstockedresult[0]['total_stocked'];
                    
                    $present_stock -= $mortality_present;
                    $present_stock += $total_purchase_present;
                    $present_stock -= $total_cost_present;
                    
                    //echo $totalstockedresult[0]['total_stocked']." - ".$mortality_present." - ".$in_present." - ".$out_present." - ".$total_purchase_present." - ".$total_cost_present." - ".$present_stock;
                    //echo "<br />";
                    $data['totalstockedresult_v'] = $present_stock;
                    

                    //echo "dfg";
                    //echo "<pre>"; print_r($f_arr);   echo "</pre>";
                    //exit();
                    $data['broilerincome_v'] = $f_arr;
                    
		}
		
        $data['getpondname']=$this->broilersales_model->getBroilersalespen();
		$this->load->view('user/livestock_inventory_broiler', $data);
	}
	
	
}
