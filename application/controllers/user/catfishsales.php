<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catfishsales extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('catfishsales_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function catfishsales_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');		
		$this->form_validation->set_rules('pond_name', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('no_of_fish', 'No of Fish', 'trim|required|callback_checkMortality|xss_clean');
		$this->form_validation->set_rules('total_weight', 'Total Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->catfishsales_model->catfishsales_add();
			$this->session->set_flashdata('message', 'Catfish Sales has been added successfully.');
			redirect(base_url().'user/catfishsales/catfishsales_add', 'refresh');
		}
		$data['getpondname']=$this->catfishsales_model->getcatfishsalespond();
		$this->load->view('user/catfishsales_add',$data);
	}
	public function checkMortality()
	{
		$this->db->select('')->from('tbl_livestock_catfish')->where(array('scid' => $this->input->post('pond_name')));
		$noofMortality = $this->db->get();
		$Mortality = $noofMortality->row_array();
		
		if	($Mortality['total_stocked'] > $this->input->post('no_of_fish')){
			return true;
			}
		else{
			$this->form_validation->set_message('checkMortality', 'No of Fish is greater than total no of stocked.');
			return false;
		}
	}
	
	//user edit for notification
	public function catfishsales_edit($csid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');	
		$this->form_validation->set_rules('pond_name', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('no_of_fish', 'No of Fish', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_weight', 'Total Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->catfishsales_model->catfishsales_edit($csid);
			$this->session->set_flashdata('message', 'Catfish Income has been changed successfully.');
			redirect(base_url().'user/catfishsales/catfishsales_view', 'refresh');
		}
		$data['catfishsales_e']=$this->catfishsales_model->getcatfishsales($csid);
		$data['getpondname']=$this->catfishsales_model->getcatfishsalespond();
		$this->load->view('user/catfishsales_edit',$data);
	}
	
	// domain view method
	public function catfishsales_view() {
		$this->db->select('cs.csid,date(cs.date) as date,cs.no_of_fish,cs.total_weight,cs.total_cost,cs.user_type,sc.pond_name,cs.staff_id')->from('tbl_catfishsales cs');
		$this->db->join('tbl_structure_catfish sc','cs.pond_name = sc.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('cs.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('cs.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['catfishsales_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/catfishsales_view', $data);
	}
	
	//user delete method
	public function catfishsales_delete($csid){
		$this->catfishsales_model->catfishsales_delete($csid);
		$this->session->set_flashdata('message', 'Catfish Sales has been deleted successfully.');
		redirect(base_url().'user/catfishsales/catfishsales_view', 'refresh');
		}
}
