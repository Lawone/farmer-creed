<?php
Class User_model extends CI_Model
{

    //var $CI;

    function __construct()
    {
        parent::__construct();
		$this->load->model(array('mail_model'));
    }
	
	public function check_login() {
		$result = $this->db->query("select * from tbl_users where username='". $this->input->post('username') ."' and password=sha1('".$this->input->post('password')."') ");
		// if user avail
		if($result->num_rows()>0 && $result->row()->username == $this->input->post('username') && $result->row()->password == sha1($this->input->post('password')))
		{
			if($result->row()->status == 1)
			{
				// set session
				$user_data=array(
					'user_login' => true,
					'email' => $result->row()->email,
					'firstname' => ucfirst($result->row()->firstname),
					'farmname' => ucfirst($result->row()->farmname),
					'uid' => $result->row()->id,
					'username' => $result->row()->username,
					'livestock' => $result->row()->livestock,
					'user_type' => $result->row()->user_type,
					'parent_uid' => $result->row()->parent_uid,
				);
				$this->session->set_userdata($user_data);
				redirect(base_url()."user/dashboard/home");
			}else{
				$this->session->set_flashdata('user_login_error', '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-times"></i>&nbsp&nbsp&nbsp<strong>Error!</strong> Access denied! Unauthorized User.  </div>');
				redirect(base_url()."user/dashboard/login", "refresh");
			}
		}else{
			// set flashdata for error msg
			$this->session->set_flashdata('user_login_error', '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-times"></i>&nbsp&nbsp&nbsp<strong>Error!</strong> Invalid Username (or) Password  </div>');
			redirect(base_url()."user/dashboard/login", "refresh");
		}
	}
    
	function save($user)
    {
        if ($user['id'])
        {
            $this->db->where('id', $user['id']);
            $this->db->update('users', $user);
            return $user['id'];
        }
        else
        {
            $this->db->insert('users', $user);
            return $this->db->insert_id();
        }
    }
    
    function logout()
    {
        $this->session->unset_userdata('user');
        //force expire the cookie
        $this->generateCookie('[]', time()-3600);
        $this->go_cart->destroy(false);
    }
    
    function login($username, $password, $remember=false)
    {
        $this->db->select('*');
        $this->db->where('username', $username);
        //$this->db->where('active', 1);
        $this->db->where('password',  $password);
        $this->db->limit(1);
        $result = $this->db->get('users');
        $result   = $result->row_array();
        
        if (sizeof($result) > 0)
        {
			$user = array();
            $user['user'] = array();
            $user['user']['id'] = $result['id'];
            $user['user']['user_type'] = $result['user_type'];
            $user['user']['firstname'] = $result['firstname'];
            $user['user']['lastname'] = $result['lastname'];
            $user['user']['email'] = $result['email'];
            $user['user']['username'] = $result['username'];
			
            if($remember)
            {
                $loginCred = json_encode(array('username'=>$username, 'password'=>$password));
                $loginCred = base64_encode($this->aes256Encrypt($loginCred));
                //remember the user for 6 months
                $this->generateCookie($loginCred, strtotime('+6 months'));
            }
            
			$this->session->set_userdata($user);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function reset_password($email)
    {
        $this->load->library('encrypt');
        $user = $this->get_user_by_email($email);
        if ($user)
        {
            $this->load->helper('string');
            $this->load->library('email');
            
            $new_password       = random_string('alnum', 8);
            $user['password']   = sha1($new_password);
            $this->save($user);
            
            $this->email->from($this->config->item('email'), $this->config->item('site_name'));
            $this->email->to($email);
            $this->email->subject($this->config->item('site_name').': Password Reset');
            $this->email->message('Your password has been reset to <strong>'. $new_password .'</strong>.');
            $this->email->send();
            
            return true;
        }
        else
        {
            return false;
        }
    }
    
	public function register($arr)
	{
		$this->db->insert('tbl_users',$arr);

		
		/* $adminTo = $arr['email'];
		$adminSubject = "New user has been registered";
		$adminInfo = array('firstname' => $arr['firstname'],'username' => $arr['username'],'userType' => $userType,'email' => $arr['email']);
		$adminMessage = $this->mail_model->regInfoAdmin($adminInfo);
		$adminRegInfo = array('to' => $adminTo,'subject' => $adminSubject,'message' => $adminMessage); */
		//$this->mail_model->sendEmail($adminRegInfo);
		return $this->db->insert_id();
	}
	
	//email verified status check
	public function emailVerificationCheck($email_verification_code)
	{
		$data = array();
		$this->db->select('status,verify_code')->from('tbl_users')->where('verify_code',$email_verification_code);
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function emailStatusChange($email_verification_code)
	{
		$this->db->where('verify_code', $email_verification_code);
		$this->db->update('tbl_users',array('status' => 1));
	}
	
	public function edit($id)
	{
		$arr['email'] = $this->input->post('email');
		$arr['firstname'] = $this->input->post('firstname');
		$arr['farmname'] = $this->input->post('farmname');
		$arr['phone'] = $this->input->post('phone');
		$arr['address1'] = $this->input->post('address1');
		if($this->input->post('password') != ''){
			$arr['password'] = $this->input->post('password');
		}
			
		$this->db->where('id', $id);
		$this->db->update('tbl_users',$arr);
	}
	function process(){
			if($_POST['submit']){
                    $catfish = $this->input->post('catfish');
                    $layers = $this->input->post('layers');
					$broilers = $this->input->post('broilers');
                    $result = "insert into tbl_existing_farmer (catfish,layers,broilers) values ('$catfish','$layers','$broilers')";
					}
					}
	public function getProfile($id)
	{
		$where = array('id' => $id);
		$this->db->select('*')->from('tbl_users');
		$this->db->where($where);
		$query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->row_array() : false;
	}

	public function getUser($id)
	{
		$this->db->select('*')->from('tbl_users')->where(array('id' => $id));
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function activate($id){
		$this->db->where(array('id' => $id));
		$this->db->update('tbl_users',array('status' => 1));
		$user = $this->getUser($id);
		$info = array('firstname' => $user['firstname']);
		
		$to = $info['email'];
		//$to = "muthukumar.4991@gmail.com";
		$subject = "Welcome to Farmscreed - Account approved";
	    $message = $this->mail_model->regTemp($info);
	    $regInfo = array('to' => $to,'subject' => $subject,'message' => $message);
		$emailStatus = $this->mail_model->sendEmail($regInfo);
		
		return array('id' => $id,'status' => 'success','email' => $emailStatus);
	}
	
	//forget_password
	public function recover_password()
	{
		$this->db->select('*')->from('tbl_users')->where(array('email' => $this->input->post('email'),'username' => $this->input->post('username')));
		$result=$this->db->get();
		if($result->num_rows()>0 && $result->row()->status == 1)
		{			
			$to = $this->input->post('email');
			$newPwd1       = random_string('alnum');
			$newPwd       = sha1($newPwd1);
			$subject = "Farmer`s Creed - Reset password";
			$info = array('firstname' => $result->row()->firstname,'password' => $newPwd1);
			$message = $this->mail_model->fgtPwdTemp($info);
			$pwdRecInfo = array('to' => $to,'subject' => $subject,'message' => $message);
			$emailStatus = $this->mail_model->sendEmail($pwdRecInfo);
			
			$this->db->where(array('username' => $this->input->post('username'),'email' => $this->input->post('email')));
			$this->db->update('tbl_users',array('password' => $newPwd));
			
		}else if($result->num_rows()>0 && $result->row()->status == 0) {
			$this->session->set_flashdata('failure', 'Access denied! Unauthorized User.');
			redirect(base_url().'user/dashboard/forgot_password', 'refresh');
		} else {
			// set flashdata for error msg
			$this->session->set_flashdata('failure', 'Email or Username is invalid.');
			redirect(base_url().'user/dashboard/forgot_password', 'refresh');
		}
	}
	
}
