<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:62%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-7" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2>Edit Staff</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >

							<fieldset>
								<div class="row">
									<section class="col col-10">
									<label class="label">Name</label>
									<label class="input">
										<input type="text" name="firstname" id="firstname" value="<?php echo set_value('firstname', isset($staff_e['firstname']) ? $staff_e['firstname'] : ''); ?>">
									</label>
									<?php echo form_error('firstname'); ?>
									</section>
								</div>
								<div class="row">
									<section class="col col-10">
									<label class="label">Login ID</label>
									<label class="input">
										<input type="text" name="username" id="username" value="<?php echo set_value('username', isset($staff_e['username']) ? $staff_e['username'] : ''); ?>">
								 	</label>
									<?php echo form_error('username'); ?>
									</section>
								</div>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary">
									Save
								</button>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" onclick="window.history.back();" class="btn btn-default" type="button">
													Back
												</button>
							</footer>
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>
			</div>

		</div>
		<script type="text/javascript">
		
			runAllForms();

			
			// Validation
			$(function() {
				jQuery.validator.addMethod(
					'ContainsAtLeastOneDigit',
					function (value) { 
					 return /[0-9]/.test(value); 
					},  
					'Your password must contain at least one digit.'
				   );  
				   
				   jQuery.validator.addMethod(
					'ContainsAtLeastOneCapitalLetter',
					function (value) { 
					 return /[A-Z]/.test(value); 
					},  
					'Your password must contain at least one capital letter.'
				   );
				   
				   jQuery.validator.addMethod(
					'ContainsAtLeastOneSymbol',
					function (value) { 
					 return /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(value); 
					},  
					'Your password must contain at least one special character.'
				   );
			
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						firstname : {
							required : true
						},
						username : {
							required : true
						},
						password : {
							required : true,
							minlength : 8,
						   	maxlength : 20,
						  	ContainsAtLeastOneDigit: true,
						   	ContainsAtLeastOneCapitalLetter: true,
						  	ContainsAtLeastOneSymbol: true
						},
					},

					// Messages for form validation
					messages : {
						firstname : {
							required : 'Please enter Name'
						},
						username : {
							required : 'Please enter Login ID'
						},
						password : {
							required : 'Please enter Password'
						},
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
		
