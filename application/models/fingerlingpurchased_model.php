<?php 
Class Fingerlingpurchased_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function fingerlingpurchased_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$fingerlingpurchased_add = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'pond_name' => $this->input->post('pond_name'),
			'total_number' => $this->input->post('total_number'),
                        'total_cost' => $this->input->post('total_cost'),
			'avg_weight' => $this->input->post('avg_weight'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),	
		);
		
		$this->db->insert('tbl_fingerlingpurchased', $fingerlingpurchased_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function fingerlingpurchased_edit($flpid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$fingerlingpurchased_edit = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'pond_name' => $this->input->post('pond_name'),
			'total_number' => $this->input->post('total_number'),
                        'total_cost' => $this->input->post('total_cost'),
			'avg_weight' => $this->input->post('avg_weight'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
		);
			
		$this->db->where('flpid', $flpid);
		$this->db->update('tbl_fingerlingpurchased',$fingerlingpurchased_edit);
		
	}
	
	public function getFingerpurchased($flpid)
	{	
		$this->db->select('*')->from('tbl_fingerlingpurchased')->where('flpid', $flpid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}	
	public function getFingerlingpurchasedpond()
	{	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('uid' => $this->session->userdata('parent_uid')));
		}
		$this->db->select('scid,pond_name')->from('tbl_structure_catfish');
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}	
	
	public function fingerlingpurchased_delete($flpid){
		
		$where = array('flpid' => $flpid);
		$this->db->where($where);
		$this->db->delete('tbl_fingerlingpurchased');
		
		
	}
	public function getStaffname($staff_id){
	
		$this->db->select('id,firstname')->from('tbl_users')->where('id', $staff_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;		
	}
}
