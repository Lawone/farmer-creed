<?php
Class Staff_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function add() {
		$staff_info = array(
			'firstname' => $this->input->post('firstname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'livestock' => $this->session->userdata('livestock'),
			'user_type' => 1,
			'status' => 1,
			'parent_uid' => $this->session->userdata('uid')
		);
		
		$this->db->insert('tbl_users', $staff_info);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function edit($id) {
		$staff_edit = array(
			'firstname' => $this->input->post('firstname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'livestock' => $this->session->userdata('livestock'),
			'user_type' => 1,
			'status' => 1,
			'parent_uid' => $this->session->userdata('uid')
		);
			
		$this->db->where('id', $id);
		$this->db->update('tbl_users',$staff_edit);
		
	}
	//change profile
	public function getTask($id)
	{
		$this->db->select('*')->from('tbl_users')->where('id', $id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function delete($id){
		
		$where = array('id' => $id);
		$this->db->where($where);
		$this->db->delete('tbl_users');
		
	}
	
}
