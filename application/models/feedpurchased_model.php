<?php
Class Feedpurchased_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function feedpurchased_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$avlWeight = $this->input->post('no_of_bags') * $this->input->post('weight');
		$feedpurchased_add = array(
			'uid' => $uid,
			'no_of_bags' => $this->input->post('no_of_bags'),
			'size' => $this->input->post('size'),
			'weight' => $this->input->post('weight'),
                        'avl_weight' => $avlWeight,
			'brand' => $this->input->post('brand'),
			'total_cost' => $this->input->post('total_cost'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id
		);
		
		$this->db->insert('tbl_feedpurchased', $feedpurchased_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function feedpurchased_edit($fpid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$avlWeight = $this->input->post('no_of_bags') * $this->input->post('weight');
		$feedpurchased_edit = array(
			'uid' => $uid,
			'no_of_bags' => $this->input->post('no_of_bags'),
			'size' => $this->input->post('size'),
			'weight' => $this->input->post('weight'),
			'avl_weight' => $avlWeight,
			'brand' => $this->input->post('brand'),
			'total_cost' => $this->input->post('total_cost'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id
		);
			
		$this->db->where('fpid', $fpid);
		$this->db->update('tbl_feedpurchased',$feedpurchased_edit);
		
	}
	
	public function getFeedpurchased($fpid)
	{	
		$this->db->select('*')->from('tbl_feedpurchased')->where('fpid', $fpid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}	
	
	public function feedpurchased_delete($fpid){
		
		$where = array('fpid' => $fpid);
		$this->db->where($where);
		$this->db->delete('tbl_feedpurchased');
		
	}
}
