<!DOCTYPE html>
<html lang="en-us">
    <head>
        <title> Farmerscreed </title>
        <?php echo $this->load->view('user/includes_view'); ?>
        <!-- EASY PIE CHARTS -->
        <script src="<?php echo base_url(); ?>js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
        <script src="<?php echo base_url(); ?>js/plugin/easy-pie-chart/jquery.flot.pie.js"></script>
    </head>
    <style>
        .copy {

            text-align:center;
            margin: auto;
            color:#000;
            font-family: open sans;
        }
    </style>
    <body id="" class="animated fadeInDown" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
        <?php echo $this->load->view('user/header'); ?>
        <?php echo $this->load->view('user/sidebar_menu'); ?>
        <div id="main"  role="main">

            <!-- MAIN CONTENT -->
            <div id="content" >
                
                <article class="col-sm-12 col-md-12 col-lg-12" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
                    
                    <?php
                    if ($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp' . $this->session->flashdata('message') . '</div>';
                    endif;
                    ?>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
                        <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                
                                data-widget-colorbutton="false"	
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true" 
                                data-widget-sortable="false"
                                
                        -->
                        <header style="color:#fff;background:#3399ff; border-color:#3399ff;">
                            <h2>Dashboard</h2>				

                        </header>
						
						<?php
                        $arrLivestock = explode(",", $this->session->userdata('livestock'));

                        for ($i = 0; $i < count($arrLivestock); $i++) {
                            if ($arrLivestock[$i] == 1) { ?>
                        <!-- widget div-->
                        <div>
							<h2 class="row-seperator-header"><i class="glyphicon glyphicon-picture"></i> Income and Expenses & Business Expenses (Catfish)</h2>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <div class="smart-form" >
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">Pond Name</label>
                                                <label class="select">
                                                    <select name="pond_name" id="pond_name">
                                                        <option value="" selected="" disabled="">Please select Pond name</option>
                                                        <?php
                                                        if ($getpondname != false) {
                                                            foreach ($getpondname as $getpondnames) {
                                                                echo '<option value="' . $getpondnames['scid'] . '" >' . $getpondnames['pond_name'] . '</option>';
                                                            }
                                                        }
                                                        ?>
                                                    </select><i></i>
                                            </section>
											<?php
												$lastYearDate = date('Y-m-d', strtotime("-1 year"));
												$currDate = date('Y-m-d');
											?>
											<input type="hidden" name="start-date" id="start-date" class="form-control datepicker" data-dateformat="yy-mm-dd" value="<?php echo $lastYearDate; ?>">
                                            <input type="hidden" name="end-date" id="end-date" class="form-control datepicker" data-dateformat="yy-mm-dd" value="<?php echo $currDate; ?>">
											<!--<section class="col col-8">

                                            </section>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    <label>Start Date</label>
                                                    <div class="input-group">
                                                        <input type="text" name="start-date" id="start-date" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    <label>End Date</label>
                                                    <div class="input-group">
                                                        <input type="text" name="end-date" id="end-date" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </section>-->
                                        </div>
                                        <div class="row pull-right" style="margin-right:6px">
                                            <button type="button" id="incomeExpBusiExpCfish" name="search" style="padding: 4px;" class="btn btn-primary">
                                                Search
                                            </button>
                                            <button  style="padding: 4px;" onclick="window.history.back();" class="btn btn-danger" type="button">
                                                Cancel
                                            </button>
                                        </div>
                                    </fieldset>
                                    </div>
                            </div>
							<div id="incomeExpCatfish" style="min-width: auto; height:300px; margin: 0 auto;display:none"></div>
							<div id="businessExpCatfish" style="min-width: auto; height:300px; margin: 0 auto;display:none"></div>
						</div>
                        <!-- end widget div -->
						
						<?php } else if ($arrLivestock[$i] == 2) { ?>
						<!-- widget div-->
                        <div>
							<h2 class="row-seperator-header"><i class="glyphicon glyphicon-picture"></i> Income and Expenses & Business Expenses (Broiler)</h2>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <div class="smart-form" >
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">Pen Name</label>
                                                <label class="select">
                                                    <select name="pen_name" id="pen_name">
														<option value="" selected="" disabled="">Please select Pen name</option>
														<?php
														if ($getpenname != false) {
															foreach ($getpenname as $getpennames) {
																echo '<option value="' . $getpennames['sbid'] . '">' . $getpennames['pen_name'] . '</option>';
															}
															}
														?>
													</select><i></i>
                                            </section>
                                            <section class="col col-8">

                                            </section>
											<?php
												$lastYearDate = date('Y-m-d', strtotime("-1 year"));
												$currDate = date('Y-m-d');
											?>
											<input type="hidden" name="broiler_start-date" id="broiler_start-date" class="form-control datepicker" data-dateformat="yy-mm-dd" value="<?php echo $lastYearDate; ?>">
											<input type="hidden" name="broiler_end-date" id="broiler_end-date" class="form-control datepicker" data-dateformat="yy-mm-dd" value="<?php echo $currDate; ?>">
                                            <!--<section class="col col-6">
                                                <div class="form-group">
                                                    <label>Start Date</label>
                                                    <div class="input-group">
                                                        <input type="text" name="broiler_start-date" id="broiler_start-date" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    <label>End Date</label>
                                                    <div class="input-group">
                                                        <input type="text" name="broiler_end-date" id="broiler_end-date" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </section>-->
                                        </div>
                                        <div class="row pull-right" style="margin-right:6px">
                                            <button type="button" id="incomeExpBusiExpBroiler" name="search" style="padding: 4px;" class="btn btn-primary">
                                                Search
                                            </button>
                                            <button  style="padding: 4px;" onclick="window.history.back();" class="btn btn-danger" type="button">
                                                Cancel
                                            </button>
                                        </div>
                                    </fieldset>
                                    </div>
                            </div>
							<div id="incomeExpBroiler" style="min-width: auto; height:300px; margin: 0 auto;display:none"></div>
							<div id="businessExpBroiler" style="min-width: auto; height:300px; margin: 0 auto;display:none"></div>
							
						</div>
                        <!-- end widget div -->
						<?php } } ?>
                    </div>
                    <!-- end widget -->
                    

                </article>
                <script type="text/javascript">

                runAllForms();

                $(function () {

                    // Time 
                    $('#time').timepicker();

                    pageSetUp();


                });
            </script>

                <script type="text/javascript">
		$(document).ready(function() {
		
			//--- starts--- Income and expenses & business expenses calculation(Catfish)
			$('#incomeExpBusiExpCfish').click(function(){
				var
				pond_name1 = $('#pond_name').val(),
				start_date = $('#start-date').val(),
				end_date = $('#end-date').val();
				
				if(pond_name1 == null){
					pond_name = '';
				}else{
					pond_name = pond_name1;
				}
				
				if(pond_name != ''){
					incomeExpCfish(pond_name,start_date,end_date);
					businessExpCfish(pond_name,start_date,end_date);
				}else{
					jAlert('Please select pond and date filter.', 'Search error');
				}
				
			});
			
			function incomeExpCfish(pond_name,start_date,end_date){
				var options = {
					chart: {
						renderTo: 'incomeExpCatfish',
						type: 'column'
					},
					title: {
						text: 'Income and Expenses',
						x: -20 //center
					},
					subtitle: {
						text: '',
						x: -20
					},
					xAxis: {
						categories: []
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Requests'
						},
						labels: {
							overflow: 'justify'
						}
					},
					tooltip: {
						formatter: function() {
								return '<b>'+ this.series.name +'</b><br/>'+
								this.x +': '+ this.y;
						}
					},
						 credits: {
						  enabled: false
						},

					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 100,
						borderWidth: 0
					}, 
					plotOptions: {
						bar: {
							dataLabels: {
								enabled: true
							}
						}
					},
					series: []
				}
				
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo base_url() . 'user/incomeexpenses_barchart/barchart_view/'; ?>"+pond_name+'/'+start_date+'/'+end_date, //The url where the server req would we made.
					beforeSend: function(){
						
					},
					success: function(json) {
						if(json != ''){
							$('#incomeExpCatfish').fadeIn(0);
							options.xAxis.categories = json[0]['data'];
							options.series[0] = json[1];
							options.series[1] = json[2];
							//options.series[2] = json[3];
							chart = new Highcharts.Chart(options);
						}
					}, 
					complete: function(){
						
					},
					error: function(e){ // alert error if ajax fails
						if(e.responseText)
							alert(e.responseText);
					} 
				 }); //end AJAX
			}
			
			function businessExpCfish(pond_name,start_date,end_date){
				var options = {
					chart: {
						renderTo: 'businessExpCatfish',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Business Expenses'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2)  +' %';
								}
							}
						}
					},
						 credits: {
								enabled: false
						  },

					series: [{
						type: 'pie',
						name: 'Browser share',
						data: []
					}]
				}
				
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo base_url() . 'user/businessexpense_piechart/piechart_view/'; ?>"+pond_name+'/'+start_date+'/'+end_date, //The url where the server req would we made.
					beforeSend: function(){
						
					},
					success: function(json) {
						if(json != ''){
							$('#businessExpCatfish').fadeIn(0);
							options.series[0].data = json;
							chart = new Highcharts.Chart(options);
						}
					}, 
					complete: function(){
						
					},
					error: function(e){ // alert error if ajax fails
						if(e.responseText)
							alert(e.responseText);
					} 
				 }); //end AJAX
			}
			
			//--- ends--- Income and expenses & business expenses calculation(Catfish)
			
			//--- starts--- Income and expenses & business expenses calculation(Broiler)
			$('#incomeExpBusiExpBroiler').click(function(){
				var
				pen_name1 = $('#pen_name').val(),
				broiler_start_date = $('#broiler_start-date').val(),
				broiler_end_date = $('#broiler_end-date').val();
				
				if(pen_name1 == null){
					pen_name = '';
				}else{
					pen_name = pen_name1;
				}
				
				if(pen_name != ''){
					incomeExpBroiler(pen_name,broiler_start_date,broiler_end_date);
					businessExpBroiler(pen_name,broiler_start_date,broiler_end_date);
				}else{
					jAlert('Please select pen and date filter.', 'Search error');
				}
				
			});
			
			function incomeExpBroiler(pen_name,broiler_start_date,broiler_end_date){
				var options = {
					chart: {
						renderTo: 'incomeExpBroiler',
						type: 'column'
					},
					title: {
						text: 'Income and Expenses',
						x: -20 //center
					},
					subtitle: {
						text: '',
						x: -20
					},
					xAxis: {
						categories: []
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Requests'
						},
						labels: {
							overflow: 'justify'
						}
					},
					tooltip: {
						formatter: function() {
								return '<b>'+ this.series.name +'</b><br/>'+
								this.x +': '+ this.y;
						}
					},
						 credits: {
						  enabled: false
						},

					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 100,
						borderWidth: 0
					}, 
					plotOptions: {
						bar: {
							dataLabels: {
								enabled: true
							}
						}
					},
					series: []
				}
				
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo base_url() . 'user/incomeexpenses_barchart_broiler/barchart_view/'; ?>"+pen_name+'/'+broiler_start_date+'/'+broiler_end_date, //The url where the server req would we made.
					beforeSend: function(){
						
					},
					success: function(json) {
						if(json != ''){
							$('#incomeExpBroiler').fadeIn(0);
							options.xAxis.categories = json[0]['data'];
							options.series[0] = json[1];
							options.series[1] = json[2];
							//options.series[2] = json[3];
							chart = new Highcharts.Chart(options);
						}
					}, 
					complete: function(){
						
					},
					error: function(e){ // alert error if ajax fails
						if(e.responseText)
							alert(e.responseText);
					} 
				 }); //end AJAX
			}
			
			function businessExpBroiler(pen_name,broiler_start_date,broiler_end_date){
				var options = {
					chart: {
						renderTo: 'businessExpBroiler',
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					title: {
						text: 'Business Expenses'
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
						}
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								color: '#000000',
								connectorColor: '#000000',
								formatter: function() {
									return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
								}
							}
						}
					},
						 credits: {
								enabled: false
						  },

					series: [{
						type: 'pie',
						name: 'Browser share',
						data: []
					}]
				}
				
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo base_url() . 'user/businessexpensebroiler_piechart/piechart_view/'; ?>"+pen_name+'/'+broiler_start_date+'/'+broiler_end_date, //The url where the server req would we made.
					beforeSend: function(){
						
					},
					success: function(json) {
						if(json != ''){
							$('#businessExpBroiler').fadeIn(0);
							options.series[0].data = json;
							chart = new Highcharts.Chart(options);
						}
					}, 
					complete: function(){
						
					},
					error: function(e){ // alert error if ajax fails
						if(e.responseText)
							alert(e.responseText);
					} 
				 }); //end AJAX
			}
			
			//--- ends--- Income and expenses & business expenses calculation(Broiler)
		
		});
		</script>
                <script src="http://code.highcharts.com/highcharts.js"></script>
                <script src="http://code.highcharts.com/modules/exporting.js"></script>
                
                
                <?php
                $this->load->view('user/footer');
                ?>
            </div>
        </div>
    </body>
</html>