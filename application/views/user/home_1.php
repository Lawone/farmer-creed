<!DOCTYPE html>
<html lang="en-us">
    <head>
        <title> Farmerscreed </title>
        <?php echo $this->load->view('user/includes_view'); ?>
    </head>
    <style>
        .copy {

            text-align:center;
            margin: auto;
            color:#000;
            font-family: open sans;
        }
    </style>
    <body id="" class="animated fadeInDown" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
        <?php echo $this->load->view('user/header'); ?>
        <?php echo $this->load->view('user/sidebar_menu'); ?>
        <div id="main"  role="main">

            <!-- MAIN CONTENT -->
            <div id="content" >
                
                <article class="col-sm-12 col-md-12 col-lg-12" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
                    
                    <?php
                    if ($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp' . $this->session->flashdata('message') . '</div>';
                    endif;
                    ?>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
                        <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                
                                data-widget-colorbutton="false"	
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true" 
                                data-widget-sortable="false"
                                
                        -->
                        <header style="color:#fff;background:#3399ff; border-color:#3399ff;">
                            <h2>Dashboard</h2>				

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <form action="" id="smart-form-farmer" class="smart-form" method="post" >
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">Pond Name</label>
                                                <label class="select">
                                                    <select name="pond_name">
                                                        <option value="" selected="" disabled="">Please select Pond name</option>
                                                        <?php
                                                        if ($getpondname != false) {
                                                            foreach ($getpondname as $getpondnames) {
                                                                echo '<option ' . (($_POST['pond_name'] == $getpondnames['scid']) ? "selected" : "") . ' value="' . $getpondnames['scid'] . '">' . $getpondnames['pond_name'] . '</option>';
                                                            }
                                                        }
                                                        ?>
                                                    </select><i></i>
                                            </section>
                                            <section class="col col-8">

                                            </section>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    <label>Start Date</label>
                                                    <div class="input-group">
                                                        <input type="text" name="start-date" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="col col-6">
                                                <div class="form-group">
                                                    <label>End Date</label>
                                                    <div class="input-group">
                                                        <input type="text" name="end-date" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                        <div class="row pull-right" style="margin-right:6px">
                                            <button type="submit" name="search" style="padding: 4px;" class="btn btn-primary">
                                                Search
                                            </button>
                                            <button  style="padding: 4px;" onclick="window.history.back();" class="btn btn-danger" type="button">
                                                Cancel
                                            </button>
                                        </div>
                                    </fieldset>
                                    </form>
                            </div>
                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                    

                </article>

                <script type="text/javascript">
                                                        $(document).ready(function() {
                                                            runAllForms();

                                                            pageSetUp();

                                                            // Time 
                                                            $('#time').timepicker();

                                                            // Validation
                                                            $("#smart-form-farmer").validate({
                                                                // Rules for form validation
                                                                rules: {
                                                                    pond_name: {
                                                                        required: true
                                                                    },
                                                                },
                                                                // Messages for form validation
                                                                messages: {
                                                                    pond_name: {
                                                                        required: 'Please Select Pond Name'
                                                                    },
                                                                },
                                                                // Do not change code below
                                                                errorPlacement: function(error, element) {
                                                                    error.insertAfter(element.parent());
                                                                }
                                                            });

                                                            var options = {
                                                                chart: {
                                                                    renderTo: 'container',
                                                                    type: 'column'
                                                                },
                                                                title: {
                                                                    text: 'Project Requests',
                                                                    x: -20 //center
                                                                },
                                                                subtitle: {
                                                                    text: '',
                                                                    x: -20
                                                                },
                                                                xAxis: {
                                                                    categories: []
                                                                },
                                                                yAxis: {
                                                                    min: 0,
                                                                    title: {
                                                                        text: 'Requests'
                                                                    },
                                                                    labels: {
                                                                        overflow: 'justify'
                                                                    }
                                                                },
                                                                tooltip: {
                                                                    formatter: function() {
                                                                        return '<b>' + this.series.name + '</b><br/>' +
                                                                                this.x + ': ' + this.y;
                                                                    }
                                                                },
                                                                credits: {
                                                                    enabled: false
                                                                },
                                                                legend: {
                                                                    layout: 'vertical',
                                                                    align: 'right',
                                                                    verticalAlign: 'top',
                                                                    x: -10,
                                                                    y: 100,
                                                                    borderWidth: 0
                                                                },
                                                                plotOptions: {
                                                                    bar: {
                                                                        dataLabels: {
                                                                            enabled: true
                                                                        }
                                                                    }
                                                                },
                                                                series: []
                                                            };

                                                            $.getJSON("<?php echo base_url() . 'user/incomeexpenses_barchart/barchart_view/' . $_POST['pond_name'] . '/' . $_POST['start-date'] . '/' . $_POST['end-date']; ?>", function(json) {
                                                                options.xAxis.categories = json[0]['data'];
                                                                options.series[0] = json[1];
                                                                options.series[1] = json[2];
                                                                //options.series[2] = json[3];
                                                                chart = new Highcharts.Chart(options);
                                                            });
                                                        });
                </script>
                <script src="http://code.highcharts.com/highcharts.js"></script>
                <script src="http://code.highcharts.com/modules/exporting.js"></script>
                <div id="container" style="min-width: auto; height:300px; margin: 0 auto"></div>
                
                <?php
                $this->load->view('user/footer');
                ?>
            </div>
        </div>
    </body>
</html>