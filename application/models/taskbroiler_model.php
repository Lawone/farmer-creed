<?php
Class Taskbroiler_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		
	}
	
	public function litterchange_add() {
	if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$litterchange_add = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'user_type' => $this->session->userdata('user_type'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id
		);
		
		$this->db->insert('tbl_litterchange', $litterchange_add);
		$ihid = $this->db->insert_id();
}
	
	//update profile
	public function litterchange_edit($tlbid)  {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$waterfish_edit = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'user_type' => $this->session->userdata('user_type'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id
		);
			
		$this->db->where('tlbid', $tlbid);
		$this->db->update('tbl_litterchange',$waterfish_edit);
		
	}
	public function getlitterchangepond()
	{	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('uid' => $this->session->userdata('parent_uid')));
		}
		$this->db->select('sbid,pen_name')->from('tbl_structure_broiler');
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	//change profile
	public function gettasklitterchange($tlbid)
	{
		$this->db->select('*')->from('tbl_litterchange')->where('tlbid', $tlbid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	public function litterchange_delete($tlbid){
		
		$where = array('tlbid' => $tlbid);
		$this->db->where($where);
		$this->db->delete('tbl_litterchange');
		
	}
	
	
	
	/*============Broiler Feeding===================*/

	public function broilerfeed_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilerfeed_add = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'user_type' => $this->session->userdata('user_type'),
			'feedtype' => $this->input->post('feedtype'),
			'feedweight' => $this->input->post('feedweight'),
                        'bfpid' => $this->input->post('feedid'),
			'feedbrand' => $this->input->post('feedbrand'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id,
			//'time' => $this->input->post('time')
		);
		$this->db->insert('tbl_broilerfeed', $broilerfeed_add);
		$ihid = $this->db->insert_id();
		
                
                
                $feedpurchased = array("avl_weight" => ( $this->input->post('feedaw') - $this->input->post('feedweight') ) );
                $this->db->where('bfpid', $this->input->post('feedid'));
		$this->db->update('tbl_broilerfeedpurchased',$feedpurchased);
	}
	
	//update profile
	public function broilerfeed_edit($bfid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilerfeed_edit = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'user_type' => $this->session->userdata('user_type'),
			'feedtype' => $this->input->post('feedtype'),
			'feedweight' => $this->input->post('feedweight'),
			'feedbrand' => $this->input->post('feedbrand'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id,
			//'time' => $this->input->post('time')
		);
			
		$this->db->where('bfid', $bfid);
		$this->db->update('tbl_broilerfeed',$broilerfeed_edit);
	}
	public function getBroilerfeedpond()
	{	
		$this->db->select('lsb.sbid,sb.pen_name')->from('tbl_livestock_broiler lsb');
		$this->db->join('tbl_structure_broiler sb','lsb.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('lsb.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('lsb.uid' => $this->session->userdata('parent_uid')));
		}
		
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	public function getBroilerfeedbrand()
	{	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('uid' => $this->session->userdata('parent_uid')));
		}
                $this->db->where(array('avl_weight !='=>0));
		$this->db->select('bfpid,brand,type,avl_weight')->from('tbl_broilerfeedpurchased');
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	//change profile
	public function getBroilerfeeding($bfid)
	{	
		$this->db->select('*')->from('tbl_broilerfeed')->where('bfid', $bfid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function broilerfeed_delete($bfid){
	
                $this->db->select('feedweight,bfpid')->from('tbl_broilerfeed')->where(array('bfid' => $bfid));
		$catfishfeeding = $this->db->get();
		$catfishfeeding_arr = $catfishfeeding->row_array();
            
                //echo "<pre>"; print_r($catfishfeeding_arr); echo "</pre>"; 
                
                $this->db->where('bfpid', $catfishfeeding_arr['bfpid']);
                $this->db->set('avl_weight', 'avl_weight+'.$catfishfeeding_arr['feedweight'], FALSE);
                $this->db->update('tbl_broilerfeedpurchased');
                
            
            
		$where = array('bfid' => $bfid);
		$this->db->where($where);
		$this->db->delete('tbl_broilerfeed');
		
	}
	
	/*============Broiler Weight===================*/

	public function broilerweight_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilerweight_add = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'avg_weight' => $this->input->post('avg_weight'),
			'user_type' => $this->input->post('user_type'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id,
			);
		$this->db->insert('tbl_broilerweight', $broilerweight_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function broilerweight_edit($uid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilerweight_edit = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'avg_weight' => $this->input->post('avg_weight'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id,
		);
			
		$this->db->where('bwid', $uid);
		$this->db->update('tbl_broilerweight',$broilerweight_edit);
}
                  public function getBroilerweightpond()
	{	
		$this->db->select('lsb.sbid,sb.pen_name')->from('tbl_livestock_broiler lsb');
		$this->db->join('tbl_structure_broiler sb','lsb.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('lsb.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('lsb.uid' => $this->session->userdata('parent_uid')));
		}
		
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	//change profile
	public function getBroilerweight($bwid)
	{	
		$this->db->select('*')->from('tbl_broilerweight')->where('bwid', $bwid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	public function broilerweight_delete($bwid)
	{
		$where = array('bwid' => $bwid);
		$this->db->where($where);
		$this->db->delete('tbl_broilerweight');
	}
	
	/*============Broiler Mortality===================*/

	public function broilermortality_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilermortality_add = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'mortality' => $this->input->post('mortality'),
			'user_type' => $this->input->post('user_type'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id,
			);
		$this->db->insert('tbl_broiler_mortality', $broilermortality_add);
		$ihid = $this->db->insert_id();
		
		//mortality pond Add
		$this->db->select('mortality_addition')->from('tbl_livestock_broiler')->where(array('uid' => $uid, 'sbid' => $this->input->post('sbid')));
		$mortalitypond = $this->db->get();
		$mortalityData = $mortalitypond->row_array();
		
		$newMortalityAdd1 = $mortalityData['mortality_addition'] + $this->input->post('mortality');
		$newMortalityAdd = abs($newMortalityAdd1);

		$this->db->where(array('uid' => $uid, 'sbid' => $this->input->post('sbid')));
		$this->db->update('tbl_livestock_broiler',array('mortality_addition' => $newMortalityAdd));
		
	}
	
	//update profile
	public function broilermortality_edit($brmid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilermortality_edit = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'mortality' => $this->input->post('mortality'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id,
		);
			
		$this->db->where('bmid', $brmid);
		$this->db->update('tbl_broiler_mortality',$broilermortality_edit);

		//mortality pond Add
		$this->db->select('mortality_addition')->from('tbl_livestock_broiler')->where(array('uid' => $uid, 'sbid' => $this->input->post('sbid')));
		$mortalitypond = $this->db->get();
		$mortalityData = $mortalitypond->row_array();
		
		if($this->input->post('hidden_mortality') == $this->input->post('mortality'))
		{
			$newMortalityAdd1 = $this->input->post('mortality');
			$newMortalityAdd = abs($newMortalityAdd1);
		}
		elseif($this->input->post('hidden_mortality') < $this->input->post('mortality'))
		{
			$newMortalityUpt = $this->input->post('mortality') - $this->input->post('hidden_mortality');
			$newMortalityAdd1 = $mortalityData['mortality_addition'] + $newMortalityUpt;
			$newMortalityAdd = abs($newMortalityAdd1);
		}
		elseif($this->input->post('hidden_mortality') > $this->input->post('mortality'))
		{
			$newMortalityUpt = $this->input->post('hidden_mortality') - $this->input->post('mortality');
			$newMortalityAdd1 = $mortalityData['mortality_addition'] - $newMortalityUpt;
			$newMortalityAdd = abs($newMortalityAdd1);
		}
		$this->db->where(array('uid' => $uid, 'sbid' => $this->input->post('sbid')));
		$this->db->update('tbl_livestock_broiler',array('mortality_addition' => $newMortalityAdd));
	}
	//change profile
	public function getBroilerMortality($brmid)
	{	
		$this->db->select('*')->from('tbl_broiler_mortality')->where('bmid', $brmid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	public function broilermortality_delete($brmid)
	{
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$this->db->select('sbid,mortality')->from('tbl_broiler_mortality')->where(array('uid' => $uid, 'bmid' => $brmid));
		$Mortality_Data = $this->db->get();
		$Mortality_Delete = $Mortality_Data->row_array();
		
		//Mortality Delete	
		$this->db->select('mortality_addition')->from('tbl_livestock_broiler')->where(array('uid' => $uid, 'sbid' =>$Mortality_Delete['sbid']));
		$Mortality = $this->db->get();
		$MortalityData = $Mortality->row_array();
		
		$MortalityAdd1 = $MortalityData['mortality_addition'] - $Mortality_Delete['mortality'];
		$MortalityAdd = abs($MortalityAdd1);

		$this->db->where(array('uid' => $uid, 'sbid' => $Mortality_Delete['sbid']));
		$this->db->update('tbl_livestock_broiler',array('mortality_addition' => $MortalityAdd));
		
		$where = array('bmid' => $brmid);
		$this->db->where($where);
		$this->db->delete('tbl_broiler_mortality');
	}
	public function getMortalitypond()
	{	
		$this->db->select('lsb.sbid,sb.pen_name')->from('tbl_livestock_broiler lsb');
		$this->db->join('tbl_structure_broiler sb','lsb.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('lsb.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('lsb.uid' => $this->session->userdata('parent_uid')));
		}
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	public function getStaffname($staff_id){
	
		$this->db->select('id,firstname')->from('tbl_users')->where('id', $staff_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	} 
	/*============Broiler Treatment===================*/

	public function broilertreatment_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilertreatment_add = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'user_type' => $this->input->post('user_type'),
			'vaccine' => $this->input->post('vaccine'),
			'treatment' => $this->input->post('treatment'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id,
			);
		$this->db->insert('tbl_broilertreatment', $broilertreatment_add);
		$ihid = $this->db->insert_id();
	}
//update profile
	public function broilertreatment_edit($btid)  {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilertreatment_edit = array(
			'uid' => $uid,
			'sbid' => $this->input->post('sbid'),
			'user_type' => $this->session->userdata('user_type'),
			'vaccine' => $this->input->post('vaccine'),
			'treatment' => $this->input->post('treatment'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id
		);
			
		$this->db->where('btid', $btid);
		$this->db->update('tbl_broilertreatment',$broilertreatment_edit);
		
	}
	public function getTreatmentpond()
	{	
		$this->db->select('lsb.sbid,sb.pen_name')->from('tbl_livestock_broiler lsb');
		$this->db->join('tbl_structure_broiler sb','lsb.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('lsb.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('lsb.uid' => $this->session->userdata('parent_uid')));
		}
		
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	//change profile
	public function getBroilerTreatment($btid)
	{
		$this->db->select('*')->from('tbl_broilertreatment')->where('btid', $btid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	public function broilertreatment_delete($btid){
		
		$where = array('btid' => $btid);
		$this->db->where($where);
		$this->db->delete('tbl_broilertreatment');
		
	}	
	
	
}
