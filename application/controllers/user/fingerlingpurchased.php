<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fingerlingpurchased extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('fingerlingpurchased_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function fingerlingpurchased_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pond_name', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_number', 'Total Number', 'trim|required|xss_clean');
                $this->form_validation->set_rules('total_cost', 'Total cost', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg.Weight', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->fingerlingpurchased_model->fingerlingpurchased_add();
			$this->session->set_flashdata('message', 'Catfish Fingerling Purchased has been added successfully.');
			redirect(base_url().'user/fingerlingpurchased/fingerlingpurchased_add', 'refresh');
		}
		$data['getpondname']=$this->fingerlingpurchased_model->getFingerlingpurchasedpond();
		$this->load->view('user/fingerlingpurchased_add',$data);
	}
	
	//user edit for notification
	public function fingerlingpurchased_edit($flpid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pond_name', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_number', 'Total Number', 'trim|required|xss_clean');
                $this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg.Weight', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->fingerlingpurchased_model->fingerlingpurchased_edit($flpid);
			$this->session->set_flashdata('message', 'Catfish Fingerling Purchased has been changed successfully.');
			redirect(base_url().'user/fingerlingpurchased/fingerlingpurchased_view', 'refresh');
		}
		$data['fingerlingpurchased_e']=$this->fingerlingpurchased_model->getFingerpurchased($flpid);
		$data['getpondname']=$this->fingerlingpurchased_model->getFingerlingpurchasedPond();
		$this->load->view('user/fingerlingpurchased_edit',$data);
	}
	
	// domain view method
	public function fingerlingpurchased_view() {
		$this->db->select('flp.flpid,flp.date,flp.total_number,flp.total_cost,flp.avg_weight,flp.user_type,flp.staff_id,sc.pond_name')->from('tbl_fingerlingpurchased flp');
		$this->db->join('tbl_structure_catfish sc','flp.pond_name = sc.scid');
	
	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('flp.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['fingerlingpurchased_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/fingerlingpurchased_view', $data);
	}
	
	//user delete method
	public function fingerlingpurchased_delete($flpid){
		
		$this->fingerlingpurchased_model->fingerlingpurchased_delete($flpid);
		$this->session->set_flashdata('message', 'Catfish Fingerling Purchased has been deleted successfully.');
		redirect(base_url().'user/fingerlingpurchased/fingerlingpurchased_view', 'refresh');
	}
}
