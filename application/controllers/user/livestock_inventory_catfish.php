<?php 
error_reporting(0);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class livestock_inventory_catfish extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('catfishsales_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function catfishincome_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pond_name', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('sales', 'Sales', 'trim|required|xss_clean');
				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->catfishsales_model->catfishincome_add();
			$this->session->set_flashdata('message', 'Catfish Income has been added successfully.');
			redirect(base_url().'user/catfishincome/catfishincome_add', 'refresh');
		}
		$data['getpondname']=$this->catfishincome_model->getCatfishincomepond();
		$this->load->view('user/catfishincome_add',$data);
	}
	
	//user edit for notification
	
	// domain view method
	public function catfishincome_view() {
            
	    if(isset($_REQUEST['search'])){
			$f_arr = array();
                 $date_arr = array();
                 $sql_query = "SELECT SUM( `mortality` ) as  mortality,date(created_date) as created_date FROM `tbl_catfish_mortality` WHERE `uid` = '".$this->session->userdata('uid')."' AND ";
                 $sql_query1 = "SELECT ( `no_of_fish` ) as no_of_fish ,from_scid,to_scid,date(created_date) as created_date FROM `tbl_sorting` WHERE  `uid` = '".$this->session->userdata('uid')."' AND ";
		 
                 $sales = "SELECT sum(no_of_fish) as total_cost,date(created_date) as created_date FROM `tbl_catfishsales` WHERE  `uid` = '".$this->session->userdata('uid')."' AND ";
                 $purchase="SELECT sum(total_number) as total_purchase,date(created_date) as created_date FROM `tbl_fingerlingpurchased` WHERE  `uid` = '".$this->session->userdata('uid')."' AND ";
                 
                 if((isset($_POST['pond_name']))  &&  (empty($_POST['start-date']))  &&  (empty($_POST['end-date'])) ){
                    $sql_query .=  "  `scid` = '".$_POST['pond_name']."' ";
                    $sql_query1 .=  "  (`from_scid` = '".$_POST['pond_name']."' OR  `to_scid` = '".$_POST['pond_name']."') ";
                   
                    $sales .= " pond_name='".$_POST['pond_name']."' ";
                    //$totalstocked="SELECT (`total_stocked`+`stock_addition`)- (`stock_deletion`+`mortality_addition`) as total_stocked FROM `tbl_livestock_catfish` WHERE scid='".$_POST['pond_name']."'";
                    
   		    $purchase .=" pond_name='".$_POST['pond_name']."'";
                    
                 }else if((!empty($_POST['pond_name'])) &&  (!empty($_POST['start-date']))  &&  (!empty($_POST['end-date']))) {
          
                    $sql_query .=  "   (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
                    $sql_query1 .=  "   (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
                    
                    $sales .=" pond_name='".$_POST['pond_name']."' AND (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
   		    $purchase .=" pond_name='".$_POST['pond_name']."' AND (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."') ";
                 }
                 
                    $sql_query  .= " GROUP BY date( `created_date` )";
                    $sales  .= " GROUP BY date( `created_date` )";
                    $purchase  .= " GROUP BY date( `created_date` )";
                    //echo "<br />";
                    $query = $this->db->query($sql_query);
                    $result = $query->result_array();
                    
                    //$totalstockedquery= $this->db->query($totalstocked);
                    //$totalstockedresult = $totalstockedquery->result_array();
		    //$data['totalstockedresult_v'] = $totalstockedresult;
                    
                    $mortality_present = 0;
                    $in_present = 0;
                    $out_present = 0;
                    $total_purchase_present = 0;
                    $total_cost_present = 0;
                    
                    
                    
                    foreach ($result as $key => $value) {
                        $now_count = count($f_arr);
                        $date_arr[$now_count] = $value['created_date'];
                        $f_arr[$now_count]['date'] = $value['created_date'];
                        $mortality_present += $f_arr[$now_count]['mortality'] = $value['mortality'];
                        $f_arr[$now_count]['in'] = 0;
                        $f_arr[$now_count]['out'] = 0;
			$f_arr[$now_count]['total_purchase'] = 0;
			$f_arr[$now_count]['total_cost'] = 0;
                    }
              

                    //echo $sql_query1 .= " GROUP BY date( `created_date` )";

                    $query1 = $this->db->query($sql_query1);
                    $result1 = $query1->result_array();
                    foreach ($result1 as $key => $value) {
                        
                        if(in_array($value['created_date'], $date_arr)){
                            $now_count = array_search ($value['created_date'], $date_arr);
                        }  else {
                            $now_count = count($f_arr);
                            $date_arr[$now_count] = $value['created_date'];
                            $f_arr[$now_count]['mortality'] = 0;
				$f_arr[$now_count]['total_purchase'] = 0;
				$f_arr[$now_count]['total_cost'] = 0;
                        }
                        
                        $f_arr[$now_count]['date'] = $value['created_date'];
                        
                        
                        if($value['from_scid'] == $_POST['pond_name']){
                            $f_arr[$now_count]['in'] += 0;
                            $out_present += $f_arr[$now_count]['out'] += $value['no_of_fish'];
                        }  else {
                            $in_present += $f_arr[$now_count]['in'] += $value['no_of_fish'];
                            $f_arr[$now_count]['out'] += 0;
                        }
                        
                    }
                    

		// sales result 
                    $sales_query = $this->db->query($sales);
                    $salesresult = $sales_query->result_array();
                   // print_r($incomeresult);
                    	
                    foreach ($salesresult as $key => $value) {
                        

			 if(in_array($value['created_date'], $date_arr)){
                            $now_count = array_search ($value['created_date'], $date_arr);
                        }  else {
                            $now_count = count($f_arr);
                            $date_arr[$now_count] = $value['created_date'];
                            $f_arr[$now_count]['mortality'] = 0;
                            $f_arr[$now_count]['out'] = 0;
                            $f_arr[$now_count]['total_purchase'] = 0;
                            $f_arr[$now_count]['in'] = 0;
                        }

                        
                        $f_arr[$now_count]['date'] = $value['created_date'];
                        $total_cost_present += $f_arr[$now_count]['total_cost'] = $value['total_cost'];


                        
 			
                        
                    }
                    // purchase result 
                   
                    $purchase_query = $this->db->query($purchase);
                    $purchaseresult = $purchase_query->result_array();
                   // print_r($incomeresult);
                    	
                    foreach ($purchaseresult as $key => $value) {
                        
                        //$now_count = count($f_arr);
                        //$date_arr[$now_count] = $value['created_date'];
			
			
			 if(in_array($value['created_date'], $date_arr)){
                            $now_count = array_search ($value['created_date'], $date_arr);
                        }  else {
                            $now_count = count($f_arr);
                            $date_arr[$now_count] = $value['created_date'];
                            
                            $f_arr[$now_count]['mortality'] = 0;
                            $f_arr[$now_count]['out'] = 0;
                            $f_arr[$now_count]['total_cost'] = 0;
                            $f_arr[$now_count]['in'] = 0;
                        }
                        $f_arr[$now_count]['date'] = $value['created_date'];
                        $total_purchase_present += $f_arr[$now_count]['total_purchase'] = $value['total_purchase'];


                        
                    }
                    
                    $totalstocked="SELECT `total_stocked` FROM `tbl_livestock_catfish` WHERE  `uid` = '".$this->session->userdata('uid')."' AND scid='".$_POST['pond_name']."'";
                    $totalstockedquery= $this->db->query($totalstocked);
                    $totalstockedresult = $totalstockedquery->result_array();
		    $data['totalstockedresult_v'] = $totalstockedresult;

                    $present_stock = $totalstockedresult[0]['total_stocked'];
                    
                    $present_stock -= $mortality_present;
                    $present_stock += $in_present;
                    $present_stock -= $out_present;
                    $present_stock += $total_purchase_present;
                    $present_stock -= $total_cost_present;
                    
                    //echo $totalstockedresult[0]['total_stocked']." - ".$mortality_present." - ".$in_present." - ".$out_present." - ".$total_purchase_present." - ".$total_cost_present." - ".$present_stock;
                    //echo "<br />";
                    $data['totalstockedresult_v'] = $present_stock;
                    

                    //echo "dfg";
                    //echo "<pre>"; print_r($f_arr);   echo "</pre>";
                    //exit();
                    $data['catfishincome_v'] = $f_arr;
                    
		}
		
        $data['getpondname']=$this->catfishsales_model->getcatfishsalespond();
		$this->load->view('user/livestock_inventory_catfish', $data);
	}
	
	
}
