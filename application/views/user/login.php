<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
<style>
@media (min-width:320px) and (max-width:640px)
{
.copy {
			color:#fff !important;
			text-align:center;
			margin-top: -21px;
			font-family: open sans;
			background:#000;
			}
}
.copy {
			color:white;
			text-align:center;
			   margin-top: -21px;
			
			font-family: open sans;
		}

#login-header-space {
    display: block;
    float: right;
    margin-top: 15px !important;
    text-align: right;
    vertical-align: middle;
}

}
</style>
</head>
<body id="login" class="register desktop-detected pace-done" style="max-width: 1370px; margin: auto;">
<!-- <body id="login" class="animated fadeInDown"> -->
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<header>
			<!--<span id="logo"></span>-->
<div style="margin-top:1em; float:right; margin-right:27px;">
<span id="login-header-space"> <!--<span class="hidden-mobile">Need an account?</span>--> <a href="<?php echo base_url().'user/dashboard/register';?>" class="btn btn-login">Register</a> </span>
</div>

		</header>
		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
						<!--<h1 class="txt-color-red login-header-big">SmartAdmin</h1>
						<div class="hero">

							<div class="pull-left login-desc-box-l">
								<h4 class="paragraph-header">It's Okay to be Smart. Experience the simplicity of SmartAdmin, everywhere you go!</h4>
								<div class="login-app-icons">
									<a href="javascript:void(0);" class="btn btn-danger btn-sm">Frontend Template</a>
									<a href="javascript:void(0);" class="btn btn-danger btn-sm">Find out more</a>
								</div>
							</div>
							
							<img src="<?php echo base_url();?>img/demo/iphoneview.png" class="pull-right display-image" alt="" style="width:210px">

						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h5 class="about-heading">About SmartAdmin - Are you up to date?</h5>
								<p>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.
								</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h5 class="about-heading">Not just your average template!</h5>
								<p>
									Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi voluptatem accusantium!
								</p>
							</div>
						</div>-->

					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-3" style="float:none; margin:auto;">
						
						<div class="no-padding" style="margin-top:6.8em;">
						<div style="text-align:center">
						<span id="logo"> <img src="<?php echo base_url();?>img/logo.png" alt="SmartAdmin"> </span>
						</div><br><br>
						<?php if($this->session->flashdata('user_login_error')) echo $this->session->flashdata('user_login_error');
						if($this->session->flashdata('success_message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
							×
						</button>
						<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('success_message').'</div>'; endif;
						if($this->session->flashdata('invalidCode')): echo '<div class="alert alert-danger"><button class="close" data-dismiss="alert">
							×
						</button>
						<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('invalidCode').'</div>'; endif;
						?>
							<form action="" id="login-form" class="smart-form client-form" method="post">
								<fieldset>
									
									<section>
										
										<label class="input"> <i class="icon-append fa fa-user"></i>
										<input type="text" name="username" style="padding-left: 3em;" placeholder="username">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter username</b></label>
											<?php echo form_error('username'); ?>
									</section>

									<section>
																				<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password" placeholder="password" style="padding-left: 3em;">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Please enter password</b> </label>
											<?php echo form_error('password'); ?>
										<div class="note">
											<a href="<?php echo base_url(); ?>user/dashboard/forgot_password"><span style="color:white; margin-top:1em;">FORGOT PASSWORD?</span></a>
										</div>
									</section>

									
								</fieldset>
								
									<button type="submit" name="login" class="btn btn-primary" style="float:right; border-color:#2c699d; padding:3.5px 15px; margin-right:1em; background-color: #3276b1; color:#fff;">
										LOGIN
									</button>
								
							</form>

						</div>
						
						<!--<h5 class="text-center"> - Or sign in using -</h5>
															
										<ul class="list-inline text-center">
											<li>
												<a href="javascript:void(0);" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
											</li>
											<li>
												<a href="javascript:void(0);" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
											</li>
											<li>
												<a href="javascript:void(0);" class="btn btn-warning btn-circle"><i class="fa fa-linkedin"></i></a>
											</li>
										</ul>-->
						
					</div>
				</div>
					</div>
		</div>
		<?php
			$this->load->view('user/footer');
		?>
		
		<script type="text/javascript">
			runAllForms();

			$(function() {
				// Validation
				$("#login-form").validate({
					// Rules for form validation
					rules : {
						username : {
							required : true
						},
						password : {
							required : true,
							minlength : 3,
							maxlength : 20
						}
					},

					// Messages for form validation
					messages : {
						username : {
							required : 'Please enter your username'
						},
						password : {
							required : 'Please enter your password'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
