<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chickpurchased extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('chickpurchased_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function chickpurchased_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pen_name', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_number', 'Total Number', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');

		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->chickpurchased_model->chickpurchased_add();
			$this->session->set_flashdata('message', 'Chick purchased has been added successfully.');
			redirect(base_url().'user/chickpurchased/chickpurchased_add', 'refresh');
		}
		$data['getpenname']=$this->chickpurchased_model->getChickpurchasedPen();
		$this->load->view('user/chickpurchased_add',$data);
	}
	
	//user edit for notification
	public function chickpurchased_edit($ckpid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pen_name', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_number', 'Total Number', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->chickpurchased_model->chickpurchased_edit($ckpid);
			$this->session->set_flashdata('message', 'Chick purchased has been changed successfully.');
			redirect(base_url().'user/chickpurchased/chickpurchased_view', 'refresh');
		}
		$data['chickpurchased_e']=$this->chickpurchased_model->getChickpurchased($ckpid);
		$data['getpenname']=$this->chickpurchased_model->getChickpurchasedPen();
		$this->load->view('user/chickpurchased_edit',$data);
	}
	
	// domain view method
	public function chickpurchased_view() {
		$this->db->select('ckp.ckpid,ckp.date,ckp.total_number,ckp.total_cost,ckp.avg_weight,ckp.user_type,ckp.staff_id,sb.pen_name')->from('tbl_chickpurchased ckp');
		$this->db->join('tbl_structure_broiler sb','ckp.pen_name = sb.sbid');
		
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('ckp.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('ckp.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['chickpurchased_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/chickpurchased_view', $data);
	}
	
	//user delete method
	public function chickpurchased_delete($ckpid){
		
		$this->chickpurchased_model->chickpurchased_delete($ckpid);
		$this->session->set_flashdata('message', 'Chick purchased has been deleted successfully.');
		redirect(base_url().'user/chickpurchased/chickpurchased_view', 'refresh');
	}
}
