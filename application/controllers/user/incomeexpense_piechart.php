
<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class incomeexpense_piechart extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->library(array('email','encrypt'));  // load library classes
		$this->load->model(array('user_model'));
	}
	public function index()
	{     
echo "subbu"; 
     
        
	  $this->load->model(array('catfishsales_model'));
            $this->load->view('user/income_expenses_reports'); 
}     
   public function piechart() {

$uid=$this->session->userdata('uid');

     $other_expense = "SELECT MONTHNAME(created_date) as month,sum(amount) as amount FROM `tbl_catfishgenpurchased` where uid='$uid' group by month";     $f_arr = array();
         $otherexpquery = $this->db->query($other_expense);
         $resultotherexp = $otherexpquery->result_array();
         foreach ($resultotherexp as $key => $value) {
          $now_count = count($f_arr);
          $f_arr[$value['month']]['amount'] = $value['amount'];
          //echo $otherexp['amount'];
         }
    $fingerling_query= "SELECT MONTHNAME(created_date) as month,sum(total_cost)as fingerling_cost from tbl_fingerlingpurchased where uid='$uid' group by month";
        $fingerlingquery = $this->db->query($fingerling_query);
         $resultfingerlingexp = $fingerlingquery->result_array();
         foreach ($resultfingerlingexp as $key => $value) {
          //$now_count = count($f_arr);
	
          $f_arr[$value['month']]['fingerling_cost'] = $value['fingerling_cost'];
         }  
    $salary = "SELECT MONTHNAME(created_date) as month, sum(amount) as amount1,created_date FROM `tbl_salaries` where uid='$uid' group by month";
  
         $salaryquery = $this->db->query($salary);
         $resultsalary = $salaryquery->result_array();
         foreach ($resultsalary as $key => $value) {
         //echo "<pre>"; print_r($value); echo "</pre>";
         //echo $now_count = count($f_arr);
         $f_arr[$value['month']]['amount1'] = $value['amount1'];
        
         }  
             

       $income = "SELECT MONTHNAME(created_date) as month, sum(total_cost) as total_cost1,created_date from tbl_catfishincome where uid='$uid' group by month ";
  
         $incomequery = $this->db->query($income);
         $resultincome = $incomequery->result_array();

foreach ($resultincome as $key => $value) {
//echo "<pre>"; print_r($value); echo "</pre>";
         //echo $now_count = count($f_arr);
         $f_arr[$value['month']]['total_cost1'] = $value['total_cost1'];
         $f_arr[$value['month']]['month'] = $value['month'];
         } 
  $feed = "SELECT MONTHNAME(created_date) as month, sum(total_cost) as total_cost2,created_date from tbl_feedpurchased where uid='$uid' group by month ";
  
         $feedquery = $this->db->query($feed);
         $resultfeed = $feedquery->result_array();

foreach ($resultfeed as $key => $value) {
//echo "<pre>"; print_r($value); echo "</pre>";
         //echo $now_count = count($f_arr);
         $f_arr[$value['month']]['total_cost2'] = $value['total_cost2'];
        // $f_arr[$value['month']]['month'] = $value['month'];
         } 
//echo "<pre>"; print_r($f_arr); echo "</pre>";
//print_r($result); 
$data['piechart_v'] = $f_arr;   

$category = array();
$category['name'] = 'createddate';

$series1 = array();
$series1['name'] = 'Expenses';

$series2 = array();
$series2['name'] = 'Income';

$series3 = array();
$series3['name'] = 'Profit';

$series_summa = array('createddate','Pond Name','Number of Fish','Income');

              
                $rows = array();
 $rows1 = array();
                //echo "<pre>";                print_r($result); echo "<pre>";  
$co_summa = 0;
                foreach ($f_arr as $key => $value1) {
		/*	$rows1[$co_summa]['name']  =  $series_summa[$co_summa];
			$rows1[$co_summa]['data'][]  =  (isset($value1['amount1']))?$value1['amount1']:0;
			$rows1[$co_summa]['data'][]  =  (isset($value1['amount']))?$value1['amount']:0;
			$rows1[$co_summa]['data'][]  =  (isset($value1['total_cost']))?$value1['total_cost']:0;*/
                        $sal_expenses=(isset($value1['amount1']))?$value1['amount1']:0;
                        $other_expenses=(isset($value1['amount']))?$value1['amount']:0;
                        $fingerling_expenses=(isset($value1['total_cost']))?$value1['total_cost']:0;
                        $feed_expenses=(isset($value1['total_cost2']))?$value1['total_cost2']:0;
                        $total_expenses=$sal_expenses+$other_expenses+$fingerling_expenses;
			$total_income=(isset($value1['total_cost1']))?$value1['total_cost1']:0;
                        $total_profit=$total_income-$total_expenses;
                    	$category['data'][]  = (isset($value1['month']))?$value1['month']:0;
                    	$series1['data'][]  =$total_income;
			$series2['data'][]  =$total_expenses;
			$series3['data'][]  =$total_profit;

		}
                                                array_push($rows,$category);
							array_push($rows,$series1);
						    array_push($rows,$series2);
						    array_push($rows,$series3);
                
           
                 print json_encode($rows, JSON_NUMERIC_CHECK);
       // $this->load->view('user/business_expenses_reports');         
            }

}
/* End of file login.php */
/* Location: ./application/controllers/login.php */

