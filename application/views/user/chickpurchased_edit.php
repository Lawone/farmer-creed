<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:62%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-7" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2>Edit Chick Purchased</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">

						<form action="" id="smart-form-farmer" class="smart-form" method="post" >

							<fieldset>
								<div class="row">
									<section class="col col-10">
										<div class="form-group">
											<label>Date</label>
											   <div class="input-group">
												<input type="text" name="date" readonly class="form-control datepicker" data-dateformat="dd/mm/yy" value="<?php echo set_value('date', isset($chickpurchased_e['date']) ? $chickpurchased_e['date'] : ''); ?>">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
									<?php echo form_error('date'); ?>
										</div>
									</section>
									<section class="col col-10">
									<label class="label">Pen Name</label>
									<label class="select">
										<select name="pen_name">
											<option value="" selected="" disabled="">Please Select Pen Name</option>
											<?php 
											 if ($getpenname != false) {
												 foreach ($getpenname as $getpennames) {
													if($getpennames['sbid'] == $chickpurchased_e['pen_name'])
													echo '<option value="'.$getpennames['sbid'].'" selected>'.$getpennames['pen_name'].'</option>';
													else
														echo '<option value="'.$getpennames['sbid'].'">'.$getpennames['pen_name'].'</option>';
					
												}
											}
											?>
										</select> <i></i>
									</label>
									<?php echo form_error('from_scid'); ?>
									</section>
									<section class="col col-10">
									<label class="label">Total Number</label>
										<label class="input">
										<input id="total_number" type="text" name="total_number" style="text-align: right" value="<?php echo set_value('total_number', isset($chickpurchased_e['total_number']) ? $chickpurchased_e['total_number'] : ''); ?>">
										</label>
										<?php echo form_error('total_number'); ?>
										</label>
									</section>
<section class="col col-10">
									<label class="label">Total Cost</label>
										<label class="input">
										<input id="total_cost" type="text" name="total_cost" style="text-align: right" value="<?php echo set_value('total_cost', isset($chickpurchased_e['total_cost']) ? $chickpurchased_e['total_cost'] : ''); ?>">
										</label>
										<?php echo form_error('total_cost'); ?>
										</label>
									</section>
									<section class="col col-10">
									<label class="label">Avg.Weight</label>
										<label class="input">
										<input id="avg_weight" type="text" name="avg_weight" style="text-align: right" value="<?php echo set_value('avg_weight', isset($chickpurchased_e['avg_weight']) ? $chickpurchased_e['avg_weight'] : ''); ?>">
										</label>
										<?php echo form_error('avg_weight'); ?>
										</label>
									</section>
								</div>
							<footer>
								<button type="submit" class="btn btn-primary">
									Save
								</button>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" onclick="window.history.back();" class="btn btn-default" type="button">
													Back
												</button>
							</footer>
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>
			</div>

		</div>
		<script type="text/javascript">
	
			runAllForms();

			$(function() {
				// Time 					
				$('#time').timepicker();
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						date : {
							required : true
						},
						pen_name : {
							required : true
						},
						total_number : {
							required : true,
							number : true
						},
						total_cost : {
							required : true,
							number : true
						},
						avg_weight : {
							required : true,
							number : true
						},
					},

					// Messages for form validation
					messages : {
						date : {
							required : 'Please Select Date'
						},
						pen_name : {
							required : 'Please Select Pen Name'
						},
						total_number : {
							required : 'Please Enter Total Number',
							number : 'Please Enter only valid number'
						},
						total_cost : {
							required : 'Please Enter Total Cost',
							number : 'Please Enter only valid number'
						},
						avg_weight : {
							required : 'Please Enter Avg Weight',
							number : 'Please Enter only valid number'
						},
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
		
