<!DOCTYPE html>
<html lang="en-us">
    <head>
        <title> Farmerscreed </title>
        <?php echo $this->load->view('user/includes_view'); ?>
    </head>
    <style>
        #login #main {
            background: none !important;
            margin: -11px 0 0;
            min-height: 676px;
            padding-top: 98px;
        }
        .textAlignRight {
            text-align:right;
        }
    </style>
    <body id="" class="animated fadeInDown">
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
        <?php echo $this->load->view('user/header'); ?>
        <?php echo $this->load->view('user/sidebar_menu'); ?>
        <div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

            <!-- MAIN CONTENT -->
            <div id="content" class="container" style="width:75%" >

                <div class="row" >
                    <!-- NEW COL START -->

                    <center>
                        <article class="col-sm-12 col-md-12 col-lg-10" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
                            <?php if ($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp' . $this->session->flashdata('message') . '</div>';
                            endif;
                            ?>
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
                                <!-- widget options:
                                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                        
                                        data-widget-colorbutton="false"	
                                        data-widget-editbutton="false"
                                        data-widget-togglebutton="false"
                                        data-widget-deletebutton="false"
                                        data-widget-fullscreenbutton="false"
                                        data-widget-custombutton="false"
                                        data-widget-collapsed="true" 
                                        data-widget-sortable="false"
                                        
                                -->
                                <header style="color:#fff;background:#3399ff; border-color:#3399ff;">
                                    <h2>Livestock Inventory Broiler</h2>				

                                </header>

                                <!-- widget div-->
                                <div>

                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body no-padding">

                                        <form action="" id="smart-form-farmer" class="smart-form" method="post" >
                                            <fieldset>
                                                <div class="row">
                                                    <section class="col col-6">
                                                        <label class="label">Pen Name</label>
                                                        <label class="select">
                                                            <select name="pen_name">
                                                                <option value="" selected="" disabled="">Please select Pen name</option>
                                                                <?php
                                                                if ($getpondname != false) {
                                                                    foreach ($getpondname as $getpondnames) {
                                                                        echo '<option '.(($_POST['pen_name']==$getpondnames['sbid'])?"selected":"").' value="' . $getpondnames['sbid'] . '">' . $getpondnames['pen_name'] . '</option>';
                                                                    }
                                                                }
                                                                ?>
                                                            </select><i></i>

                                                    </section>

                                                    <section class="col col-6">
                                                        <div class="form-group" style="margin-top:5px">
                                                            <label>Start Date</label>
                                                            <div class="input-group">
                                                                <input type="text" name="start-date" value="<?php echo $_POST['start-date'];?>" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>

                                                <div class="row">
                                                    <section class="col col-6">
                                                        <div class="form-group">
                                                            <label>End Date</label>
                                                            <div class="input-group">
                                                                <input type="text" name="end-date" value="<?php echo $_POST['end-date'];?>" class="form-control datepicker" data-dateformat="yy-mm-dd">
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>

                                                        </div>
                                                    </section>
                                                    <section class="col col-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <button class="btn btn-primary" name="search" style="margin:23px 5px 4px 2px; padding: 4px; background-color: #3399ff" type="submit"> Search </button>
                                                            </div>	   
                                                        </div>
                                                    </section>



                                                </div>
                                            </fieldset></div>
                                    <div> <?php
                                                    // Listing domains
                                                    if(isset($totalstockedresult_v)){
                                                        ?>
<br><br><div style="font-weight:bold;"> Present Stocks: <?php echo $totalstockedresult_v; ?></div><?php }?>
                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->
                                        </div>	
                                        <p>&#160;</p>
                                        <p>&#160;</p>

                                        <!-- end widget edit box -->
                                       
                                        <!-- widget content -->
                                        <div class="row">
                                            <table id="dt_basic" class="table table-striped table-bordered table-hover" style="margin-top:20px;">
                                                <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Mortality</th>
														<th>Sales</th>
                                                        <th>Purchases</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    // Listing domains
                                                    foreach ($broilerincome_v as $broilerincome) {
                                                        ?>
                                                        <tr>

                                                            <td><?php echo $broilerincome['date']; ?></td>
                                                            <td><?php echo $broilerincome['mortality']; ?></td>
							    <td><?php echo $broilerincome['total_cost']; ?></td>
							    <td><?php echo $broilerincome['total_purchase']; ?></td>






                                                        </tr>
<?php } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                        <!-- end widget content -->

                                    </div>


                                    </form>						

                                </div>
                                <!-- end widget content -->

                            </div>
                            <!-- end widget div -->




                        </article>
                    </center>
                    <!-- END COL -->
                </div>



            </div>
            <script type="text/javascript">

                runAllForms();

                $(function () {

                    // Time 
                    $('#time').timepicker();

                    // Validation
                    $("#smart-form-farmer").validate({
                        // Rules for form validation
                        rules: {
                            pond_name: {
                                required: true
                            },
                            feed: {
                                required: true,
                                number: true
                            },
                            salaries: {
                                required: true,
                                number: true
                            },
                            fingerling: {
                                required: true,
                                number: true
                            },
                            other_exp: {
                                required: true,
                                number: true
                            },
                        },
                        // Messages for form validation
                        messages: {
                            pond_name: {
                                required: 'Please Select Pond Name'
                            },
                            feed: {
                                required: 'Please Enter Feed',
                                number: 'Please Enter only valid number'
                            },
                            salaries: {
                                required: 'Please Enter Feed',
                                number: 'Please Enter only valid number'
                            },
                            fingerling: {
                                required: 'Please Enter Fingerling',
                                number: 'Please Enter only valid number'
                            },
                            other_exp: {
                                required: 'Please Enter Other Exp',
                                number: 'Please Enter only valid number'
                            }
                        },
                        // Do not change code below
                        errorPlacement: function (error, element) {
                            error.insertAfter(element.parent());
                        }
                    });


                    $(document).ready(function () {

                        $(".headerDiv").fadeOut();
                        $(".searchMe").fadeOut();

                        /*
                         * BASIC
                         */
                        var aoColumns = [
                            {"bSortable": false},
                            null,
                            null,
                            null,
			    null,
			    null,
                            {"bSortable": false}
                        ];
                        $('#dt_basic').dataTable({
                            "sPaginationType": "bootstrap_full",
                            "aaSorting": [[0, 'desc']]
                        });

                        /* END BASIC */

                        pageSetUp();
                        /* END TABLE TOOLS */

                    });


                });
            </script>




