<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salaries extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('salaries_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function salaries_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('staff', 'Staff', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');

		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->salaries_model->salaries_add();
			$this->session->set_flashdata('message', 'Salaries has been added successfully.');
			redirect(base_url().'user/salaries/salaries_add', 'refresh');
		}
		$data['getstaffname']=$this->salaries_model->getSalariesStaff();
		$this->load->view('user/salaries_add',$data);
	}
	
	//user edit for notification
	public function salaries_edit($ssid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('staff', 'Staff', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->salaries_model->salaries_edit($ssid);
			$this->session->set_flashdata('message', 'Salaries has been changed successfully.');
			redirect(base_url().'user/salaries/salaries_view', 'refresh');
		}
		$data['salaries_e']=$this->salaries_model->getSalaries($ssid);
		$data['getstaffname']=$this->salaries_model->getSalariesStaff();
		$this->load->view('user/salaries_edit',$data);
	}
	
	// domain view method
	public function salaries_view() {
		$this->db->select('ss.ssid,ss.date,ss.amount,ss.user_type,ss.staff_id,usr.firstname')->from('tbl_salaries ss');
		$this->db->join('tbl_users usr', 'ss.staff = usr.id');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('ss.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('ss.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['salaries_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/salaries_view', $data);
	}
	
	//user delete method
	public function salaries_delete($ssid){
		
		$this->salaries_model->salaries_delete($ssid);
		$this->session->set_flashdata('message', 'Salaries has been deleted successfully.');
		redirect(base_url().'user/salaries/salaries_view', 'refresh');
	}
}
