
<?php

error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class incomeexpenses_barchart_broiler extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->app_access->user(); // check access permission for user
        $this->load->model(array('broilersales_model'));
    }

    // Registration
    public function index() {



        $this->load->view('user/income_expenses_reports_broiler');
    }

    // domain view method
    public function barchart_view($pen_name, $start_date, $end_date) {
		$f_arr = array();
		$date_arr = array();
		$incomeQuery = "SELECT sum(`total_cost`) as `total_cost`, date_format(created_date,'%M') as month FROM `tbl_broilerincome` WHERE `uid` = '".$this->session->userdata('uid')."' and ";
		$feedQuery = "SELECT con.bfpid,sum(con.feedweight) as feedweight,date_format(con.created_date,'%M') as month,pur.weight,pur.total_cost,pur.no_of_bags FROM tbl_broilerfeed con, tbl_broilerfeedpurchased pur WHERE";
		$salaryQuery = "SELECT sum(amount) as amount,date_format(date,'%M') as month FROM tbl_salaries WHERE";
		$totalStockQuery = "SELECT sum(total_stocked) as total_stock FROM tbl_livestock_broiler WHERE";
		$totalStockPondQuery = "SELECT sum(total_stocked) as total_pondstock FROM tbl_livestock_broiler WHERE";
		$chickQuery = "SELECT date_format(date,'%M') as month,sum(total_cost) as chickAmt FROM tbl_chickpurchased WHERE";
		$otherExpQuery = "SELECT date_format(date,'%M') as month,sum(amount) as othExpAmt FROM tbl_broilergenpurchased WHERE";
		
		if ($pen_name != '' && $start_date != '' && $end_date != '') {
			$incomeQuery .=  "(date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') and pen_name = '".$pen_name."' ";
			$feedQuery .=  "  `sbid` = '".$pen_name."' and con.uid = '".$this->session->userdata('uid')."' AND (date(con.created_date) BETWEEN '".$start_date."' AND '".$end_date."') and con.bfpid = pur.bfpid and con.uid = pur.uid ";
			$salaryQuery .=  "  uid = '".$this->session->userdata('uid')."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			$totalStockQuery .=  "  uid = '".$this->session->userdata('uid')."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			$totalStockPondQuery .=  "  uid = '".$this->session->userdata('uid')."' and sbid = '".$pen_name."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			$chickQuery .=  "  `pen_name` = '".$pen_name."' and uid = '".$this->session->userdata('uid')."' AND (date(date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			$otherExpQuery .=  "  `uid` = '".$this->session->userdata('uid')."' AND (date(date) BETWEEN '".$start_date."' AND '".$end_date."') ";
		}
		
		$incomeQuery .= "group by date_format(created_date,'%M')";
		$feedQuery .= "group by con.bfpid,date_format(con.created_date,'%M')";
		$salaryQuery .= "group by date_format(date,'%M')";
		$chickQuery .= "group by date_format(date,'%M')";
		$otherExpQuery .= "group by date_format(date,'%M')";
		
		//echo $incomeQuery.'<br/>'.$feedQuery.'<br/>'.$salaryQuery.'<br/>'.$salaryCountQuery.'<br/>'.$totalStockQuery.'<br/>'.$totalStockPondQuery.'<br/>'.$fingerlingQuery.'<br/>'.$otherExpQuery;
				
		$incomeData = $this->db->query($incomeQuery);
		$incomeResult = $incomeData->result_array();
		
		$totalExp = 0;
		foreach ($incomeResult as $key => $value) {
			if(in_array($value['month'], $date_arr)){
					$now_count = array_search ($value['month'], $date_arr);
				}  else {
					$now_count = count($f_arr);
					$date_arr[$now_count] = $value['month'];
					$f_arr[$now_count]['expAmt'] = 0;
				}
			//echo $value['feedweight'].'---'.$value['weight'].'----'.$value['total_cost'];
			$f_arr[$now_count]['month'] = $value['month'];
			$f_arr[$now_count]['incAmt'] = number_format($value['total_cost'],2,'.','');				
		}
		
		$query = $this->db->query($feedQuery);
		$result = $query->result_array();
		
		$totalExp = 0;
		foreach ($result as $key => $value) {
			$feedCost = ($value['feedweight'] / ($value['no_of_bags'] * $value['weight'])) * $value['total_cost'];
			if(in_array($value['month'], $date_arr)){
					$now_count = array_search ($value['month'], $date_arr);
					$f_arr[$now_count]['expAmt'] += number_format($feedCost,2,'.','');
				}  else {
					$now_count = count($f_arr);
					$date_arr[$now_count] = $value['month'];
					$f_arr[$now_count]['incAmt'] = 0;
					$f_arr[$now_count]['expAmt'] = number_format($feedCost,2,'.','');
				}
			//echo $value['feedweight'].'---'.$value['weight'].'----'.$value['total_cost'];
			
			$f_arr[$now_count]['month'] = $value['month'];
			
			
			
		}
  

		//echo $sql_query1 .= " GROUP BY date( `created_date` )";

		//Salary expense calculation
		$query1 = $this->db->query($salaryQuery);
		$result1 = $query1->result_array();
					
		//get user total stock
		$stockTotQuery = $this->db->query($totalStockQuery);
		$stockTotData = $stockTotQuery->row_array();
		$totStock = ($stockTotQuery -> num_rows() > 0) ? $stockTotData['total_stock'] : 0;
		
		//get user with pond total stock
		$stockPondTotQuery = $this->db->query($totalStockPondQuery);
		$stockPondTotData = $stockPondTotQuery->row_array();
		$totPondStock = ($stockPondTotQuery -> num_rows() > 0) ? $stockPondTotData['total_pondstock'] : 0;
		if($totStock > 0 && $totPondStock > 0){
			foreach ($result1 as $key => $value) {
				$salCost1 = ($value['amount']) / $totStock;
				$salCost = number_format(($salCost1 * $totPondStock),2,'.','');
				//echo $value['amount'].'---'.$salCount.'----'.$totStock.'----'.$totPondStock.'<br/>';
				if(in_array($value['month'], $date_arr)){
					$now_count = array_search ($value['month'], $date_arr);
					$f_arr[$now_count]['expAmt']+=$salCost;
				}  else {
					$now_count = count($f_arr);
					$date_arr[$now_count] = $value['month'];
					$f_arr[$now_count]['incAmt'] = 0;
					$f_arr[$now_count]['expAmt'] = $salCost;
				}
				
				$f_arr[$now_count]['month'] = $value['month'];
				
				
									   
			}
		}
		
		$query3 = $this->db->query($chickQuery);
		$result3 = $query3->result_array();
		if($totStock > 0 && $totPondStock > 0){
			foreach ($result3 as $key => $value) {
				if(in_array($value['month'], $date_arr)){
					$now_count = array_search ($value['month'], $date_arr);
					$f_arr[$now_count]['expAmt']+=number_format($value['chickAmt'],2,'.','');
				}  else {
					$now_count = count($f_arr);
					$date_arr[$now_count] = $value['month'];
					$f_arr[$now_count]['incAmt'] = 0;
					$f_arr[$now_count]['expAmt'] = number_format($value['chickAmt'],2,'.','');
				}
				
				$f_arr[$now_count]['month'] = $value['month'];
				
				
									   
			}
		}
		
		$query4 = $this->db->query($otherExpQuery);
		$result4 = $query4->result_array();
		foreach ($result4 as $key => $value) {
			$othCost = ($value['othExpAmt'] / $totStock) * $totPondStock;
			//echo $value['othExpAmt'].'---'.$totStock.'----'.$totPondStock;
			if(in_array($value['month'], $date_arr)){
				$now_count = array_search ($value['month'], $date_arr);
				$f_arr[$now_count]['expAmt']+=number_format($othCost,2,'.','');
			}  else {
				$now_count = count($f_arr);
				$date_arr[$now_count] = $value['month'];
				$f_arr[$now_count]['incAmt'] = 0;
				$f_arr[$now_count]['expAmt'] = number_format($othCost,2,'.','');
			}
			
			$f_arr[$now_count]['month'] = $value['month'];
			
			
								   
		}
		
		$series2 = array();
		$series2['name'] = 'Expenses';

		$series1 = array();
		$series1['name'] = 'Income';
		$rows = array();
		
		foreach ($f_arr as $f_result) {
			$category['data'][] = $f_result['month'];
			$series1['data'][] = $f_result['incAmt'];
			$series2['data'][] = $f_result['expAmt'];
		}
		array_push($rows, $category);
		array_push($rows, $series1);
		array_push($rows, $series2);

            /* if ($pen_name != '' && $start_date != '' && $end_date != '') {
                $uid = $this->session->userdata('uid');
                $f_arr = array();
                $nooflivestock = "SELECT MONTHNAME(created_date) as month, total_stocked FROM `tbl_livestock_broiler` where sbid='" . $pen_name . "' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "' group by month";
                $nooflivestockquery = $this->db->query($nooflivestock);
                $resultlivestock = $nooflivestockquery->result_array();
                foreach ($resultlivestock as $key => $value) {
                    $now_count = count($f_arr);
                    $f_arr[$value['month']]['total_stocked'] = $value['total_stocked'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }
                $totnooflivestock = "SELECT MONTHNAME(created_date) as month, total_stocked,avg_weight,sum(total_stocked) as total_stock FROM `tbl_livestock_broiler`where uid='$uid' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "' group by month";
                $totnooflivestockquery = $this->db->query($totnooflivestock);
                $resulttotlivestock = $totnooflivestockquery->result_array();
                foreach ($resulttotlivestock as $key => $value) {
                    $f_arr[$value['month']]['total_stock'] = $value['total_stock'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }
                $other_expense = "SELECT MONTHNAME(created_date) as month,sum(amount) as amount FROM `tbl_broilergenpurchased` where uid='$uid' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "' group by month";
                $otherexpquery = $this->db->query($other_expense);
                $resultotherexp = $otherexpquery->result_array();
                foreach ($resultotherexp as $key => $value) {
                    $f_arr[$value['month']]['amount'] = $value['amount'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                    //echo $otherexp['amount'];
                }
                $chick = "SELECT MONTHNAME(created_date) as month,sum(total_cost) as chick_cost from tbl_chickpurchased where uid='$uid' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "' group by month";

                $chickquery = $this->db->query($chick);
                $resultchickexp = $chickquery->result_array();
                foreach ($resultchickexp as $key => $value) {
                    $f_arr[$value['month']]['chick_cost'] = $value['chick_cost'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }
                $salaries = "SELECT MONTHNAME(created_date) as month, sum(amount) as amount2,created_date FROM `tbl_salaries` where uid='" . $uid . "' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "' group by month";
                $salexpquery = $this->db->query($salaries);
                $resultsalexp = $salexpquery->result_array();
                foreach ($resultsalexp as $key => $value) {
                    $f_arr[$value['month']]['amount2'] = $value['amount2'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }


                $income = "SELECT ci.pen_name, SUM(ci.poultry) as poultry ,SUM(ci.total_cost) as total_cost1, MONTHNAME(ci.created_date) as month, cat.pen_name FROM `tbl_structure_broiler` cat JOIN tbl_broilerincome ci WHERE cat.sbid = ci.pen_name AND ci.pen_name='" . $pen_name . "' group by month";

                $incomequery = $this->db->query($income);
                $resultincome = $incomequery->result_array();

                foreach ($resultincome as $key => $value) {
                    //echo "<pre>"; print_r($value); echo "</pre>";
                    //echo $now_count = count($f_arr);
                    $now_count = count($f_arr);
                    $f_arr[$value['month']]['total_cost1'] = $value['total_cost1'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }

                $feed = "SELECT MONTHNAME(feed.created_date) as month,feed.total_cost,bro.feedweight,(bro.feedweight*(feed.total_cost/feed.weight)) as daily_feed FROM    `tbl_broilerfeedpurchased` feed join tbl_broilerfeed bro where bro.feedbrand=feed.brand and bro.sbid='" . $pen_name . "' group by month";

                $feedquery = $this->db->query($feed);
                $resultfeed = $feedquery->result_array();

                foreach ($resultfeed as $key => $value) {
//echo "<pre>"; print_r($value); echo "</pre>";
                    //echo $now_count = count($f_arr);
                    $f_arr[$value['month']]['daily_feed'] = $value['daily_feed'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                    // $f_arr[$value['month']]['month'] = $value['month'];
                }
            }else if ($pen_name != '') {

                $uid = $this->session->userdata('uid');
                $f_arr = array();
                $nooflivestock = "SELECT MONTHNAME(created_date) as month, total_stocked FROM `tbl_livestock_broiler` where sbid='" . $pen_name . "' group by month";
                $nooflivestockquery = $this->db->query($nooflivestock);
                $resultlivestock = $nooflivestockquery->result_array();
                foreach ($resultlivestock as $key => $value) {
                    $now_count = count($f_arr);
                    $f_arr[$value['month']]['total_stocked'] = $value['total_stocked'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }
                $totnooflivestock = "SELECT MONTHNAME(created_date) as month, total_stocked,avg_weight,sum(total_stocked) as total_stock FROM `tbl_livestock_broiler`where uid='$uid' group by month";
                $totnooflivestockquery = $this->db->query($totnooflivestock);
                $resulttotlivestock = $totnooflivestockquery->result_array();
                foreach ($resulttotlivestock as $key => $value) {
                    $f_arr[$value['month']]['total_stock'] = $value['total_stock'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }
                $other_expense = "SELECT MONTHNAME(created_date) as month,sum(amount) as amount FROM `tbl_broilergenpurchased` where uid='$uid' group by month";
                $otherexpquery = $this->db->query($other_expense);
                $resultotherexp = $otherexpquery->result_array();
                foreach ($resultotherexp as $key => $value) {
                    $f_arr[$value['month']]['amount'] = $value['amount'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                    //echo $otherexp['amount'];
                }
                $chick = "SELECT MONTHNAME(created_date) as month,sum(total_cost) as chick_cost from tbl_chickpurchased where uid='$uid' group by month";

                $chickquery = $this->db->query($chick);
                $resultchickexp = $chickquery->result_array();
                foreach ($resultchickexp as $key => $value) {
                    $f_arr[$value['month']]['chick_cost'] = $value['chick_cost'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }
                $salaries = "SELECT MONTHNAME(created_date) as month, sum(amount) as amount2,created_date FROM `tbl_salaries` where uid='" . $uid . "' group by month";
                $salexpquery = $this->db->query($salaries);
                $resultsalexp = $salexpquery->result_array();
                foreach ($resultsalexp as $key => $value) {
                    $f_arr[$value['month']]['amount2'] = $value['amount2'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }


                $income = "SELECT ci.pen_name, SUM(ci.poultry) as poultry ,SUM(ci.total_cost) as total_cost1, MONTHNAME(ci.created_date) as month, cat.pen_name FROM `tbl_structure_broiler` cat JOIN tbl_broilerincome ci WHERE cat.sbid = ci.pen_name AND ci.pen_name='" . $pen_name . "' group by month";

                $incomequery = $this->db->query($income);
                $resultincome = $incomequery->result_array();

                foreach ($resultincome as $key => $value) {
                    //echo "<pre>"; print_r($value); echo "</pre>";
                    //echo $now_count = count($f_arr);
                    $now_count = count($f_arr);
                    $f_arr[$value['month']]['total_cost1'] = $value['total_cost1'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                }

                $feed = "SELECT MONTHNAME(feed.created_date) as month,feed.total_cost,bro.feedweight,(bro.feedweight*(feed.total_cost/feed.weight)) as daily_feed FROM    `tbl_broilerfeedpurchased` feed join tbl_broilerfeed bro where bro.feedbrand=feed.brand and bro.sbid='" . $pen_name . "' group by month";

                $feedquery = $this->db->query($feed);
                $resultfeed = $feedquery->result_array();

                foreach ($resultfeed as $key => $value) {
//echo "<pre>"; print_r($value); echo "</pre>";
                    //echo $now_count = count($f_arr);
                    $f_arr[$value['month']]['daily_feed'] = $value['daily_feed'];
                    $f_arr[$value['month']]['month'] = $value['month'];
                    // $f_arr[$value['month']]['month'] = $value['month'];
                }
            }
        $series2 = array();
        $series2['name'] = 'Expenses';

        $series1 = array();
        $series1['name'] = 'Income';

        $series3 = array();
        $series3['name'] = 'Profit';
        $rows = array();

        foreach ($f_arr as $key => $value1) {
            $otherexp1 = ($value1['amount'] / $value1['total_stock']);
            $other_expenses = $otherexp1 * $value1['total_stocked'];
            $salcalc = ($value1['amount2'] / 30) / $value1['total_stock'];
            $salary_expenses = $salcalc * $value1['total_stocked'];
            $chick_expenses = $value1['chick_cost'];
            $feed_expenses = $value1['daily_feed'];
            $total_expenses = $other_expenses + $salary_expenses + $chick_expenses + $feed_expenses;
            $total_income = (isset($value1['total_cost1'])) ? $value1['total_cost1'] : 0;
            $total_profit = $total_income - $total_expenses;
            $category['data'][] = $value1['month'];
            $series1['data'][] = $total_expenses;
            $series2['data'][] = $total_income;
            $series3['data'][] = $total_profit;
        }
        array_push($rows, $category);
        array_push($rows, $series1);
        array_push($rows, $series2);
        array_push($rows, $series3); */

        //echo "<pre>";                print_r($data); echo "<pre>";  

        print json_encode($rows, JSON_NUMERIC_CHECK);
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
