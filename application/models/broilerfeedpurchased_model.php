<?php
Class Broilerfeedpurchased_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function broilerfeedpurchased_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$avlWeight = $this->input->post('no_of_bags') * $this->input->post('weight');
		$broilerfeedpurchased_add = array(
			'uid' => $uid,
			'no_of_bags' => $this->input->post('no_of_bags'),
			'type' => $this->input->post('type'),
			'weight' => $this->input->post('weight'),
                        'avl_weight' => $avlWeight,
                    
			'brand' => $this->input->post('brand'),
			'total_cost' => $this->input->post('total_cost'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id
		);
		
		$this->db->insert('tbl_broilerfeedpurchased', $broilerfeedpurchased_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function broilerfeedpurchased_edit($bfpid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$avlWeight = $this->input->post('no_of_bags') * $this->input->post('weight');
		$broilerfeedpurchased_edit = array(
			'uid' => $uid,
			'no_of_bags' => $this->input->post('no_of_bags'),
			'type' => $this->input->post('type'),
			'weight' => $this->input->post('weight'),
			'avl_weight' => $avlWeight,
			'brand' => $this->input->post('brand'),
			'total_cost' => $this->input->post('total_cost'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id
		);
			
		$this->db->where('bfpid', $bfpid);
		$this->db->update('tbl_broilerfeedpurchased',$broilerfeedpurchased_edit);
		
	}
	
	public function getBroilerfeedpurchased($bfpid)
	{	
		$this->db->select('*')->from('tbl_broilerfeedpurchased')->where('bfpid', $bfpid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}	
	
	public function broilerfeedpurchased_delete($bfpid){
		
		$where = array('bfpid' => $bfpid);
		$this->db->where($where);
		$this->db->delete('tbl_broilerfeedpurchased');
		
	}
}
