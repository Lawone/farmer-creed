<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Structcatfish extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('structcatfish_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('pond_name','Name','trim|required|callback_alreadyExistPondName|xss_clean');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('size', 'Size', 'trim|required|xss_clean');
				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->structcatfish_model->add();
			$this->session->set_flashdata('message', 'Catfish has been added successfully.');
			redirect(base_url().'user/structcatfish/add', 'refresh');
		}
		
		$this->load->view('user/structcatfish_add');
	}
	
	//user edit for notification
	public function edit($uid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('pond_name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('size', 'Size', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->structcatfish_model->edit($uid);
			$this->session->set_flashdata('message', 'Catfish has been changed successfully.');
			redirect(base_url().'user/structcatfish/view', 'refresh');
		}
		$data['structcatfish_e']=$this->structcatfish_model->getStructcatfish($uid);
		$this->load->view('user/structcatfish_edit',$data);
	}
	
	// domain view method
	public function view() {
		$this->db->select('*')->from('tbl_structure_catfish')->where('uid', $this->session->userdata('uid'));
		
		// data subset
		$query = $this->db->get();
		$data['structcatfish_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/structcatfish_view', $data);
	}
	
	//user delete method
	public function delete($scid){
		
		$this->structcatfish_model->delete($scid);
		$this->session->set_flashdata('message', 'Catfish has been deleted successfully.');
		redirect(base_url().'user/structcatfish/view', 'refresh');
	}
	
	public function alreadyExistPondName()
	{
		$this->db->select('pond_name')->from('tbl_structure_catfish')->where(array('pond_name' => $this->input->post('pond_name'),'uid' => $this->session->userdata('uid')));
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$this->form_validation->set_message('alreadyExistPondName', 'Pond Name already exists.');
			return false;
		}else{
			return true;
		}
	}
}
