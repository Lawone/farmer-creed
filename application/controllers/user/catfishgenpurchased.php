<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catfishgenpurchased extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('catfishgenpurchased_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function catfishgenpurchased_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->catfishgenpurchased_model->catfishgenpurchased_add();
			$this->session->set_flashdata('message', 'Catfish general purchased has been added successfully.');
			redirect(base_url().'user/catfishgenpurchased/catfishgenpurchased_add', 'refresh');
		}
		$this->load->view('user/catfishgenpurchased_add');
	}
	
	//user edit for notification
	public function catfishgenpurchased_edit($cgpid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->catfishgenpurchased_model->catfishgenpurchased_edit($cgpid);
			$this->session->set_flashdata('message', 'Catfish general purchased has been changed successfully.');
			redirect(base_url().'user/catfishgenpurchased/catfishgenpurchased_view', 'refresh');
		}
		$data['catfishgenpurchased_e']=$this->catfishgenpurchased_model->getCatfishgenpurchased($cgpid);
		$this->load->view('user/catfishgenpurchased_edit',$data);
	}
	
	// domain view method
	public function catfishgenpurchased_view() {
		$this->db->select('cgpid,date,description,amount,user_type,staff_id')->from('tbl_catfishgenpurchased');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['catfishgenpurchased_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/catfishgenpurchased_view', $data);
	}
	
	//user delete method
	public function catfishgenpurchased_delete($cgpid){
		
		$this->catfishgenpurchased_model->catfishgenpurchased_delete($cgpid);
		$this->session->set_flashdata('message', 'Catfish general purchased has been deleted successfully.');
		redirect(base_url().'user/catfishgenpurchased/catfishgenpurchased_view', 'refresh');
	}
}
