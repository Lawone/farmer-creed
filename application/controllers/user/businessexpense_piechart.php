
<?php

error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class businessexpense_piechart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->app_access->user(); // check access permission for user
        $this->load->model(array('catfishsales_model'));
    }

    // Registration
    public function index() {



        $this->load->view('user/business_expenses_reports');
    }

    // domain view method
    public function piechart_view($pond_name, $start_date, $end_date) {
	
		$f_arr = array();
		$date_arr = array();
		$feedQuery = "SELECT con.fpid,sum(con.feedweight) as feedweight,date_format(con.created_date,'%M') as month,pur.weight,pur.total_cost,pur.no_of_bags FROM tbl_catfishfeed con, tbl_feedpurchased pur WHERE";
		$salaryQuery = "SELECT sum(amount) as amount,date_format(date,'%M') as month FROM tbl_salaries WHERE";
		$totalStockQuery = "SELECT sum(total_stocked) as total_stock FROM tbl_livestock_catfish WHERE";
		$totalStockPondQuery = "SELECT sum(total_stocked) as total_pondstock FROM tbl_livestock_catfish WHERE";
		$fingerlingQuery = "SELECT date_format(date,'%M') as month,sum(total_cost) as fingerlingAmt FROM tbl_fingerlingpurchased WHERE";
		$otherExpQuery = "SELECT date_format(date,'%M') as month,sum(amount) as othExpAmt FROM tbl_catfishgenpurchased WHERE";
		
		if ($pond_name != '' && $start_date != '' && $end_date != '') {
			$feedQuery .=  "  `scid` = '".$pond_name."' and con.uid = '".$this->session->userdata('uid')."' AND (date(con.created_date) BETWEEN '".$start_date."' AND '".$end_date."') and con.fpid = pur.fpid and con.uid = pur.uid ";
			$salaryQuery .=  "  uid = '".$this->session->userdata('uid')."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			$totalStockQuery .=  "  uid = '".$this->session->userdata('uid')."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			$totalStockPondQuery .=  "  uid = '".$this->session->userdata('uid')."' and scid = '".$pond_name."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			$fingerlingQuery .=  "  `pond_name` = '".$pond_name."' and uid = '".$this->session->userdata('uid')."' AND (date(date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			$otherExpQuery .=  "  `uid` = '".$this->session->userdata('uid')."' AND (date(date) BETWEEN '".$start_date."' AND '".$end_date."') ";
		}
		
		$feedQuery .= "group by con.fpid,date_format(con.created_date,'%M')";
		$salaryQuery .= "group by date_format(date,'%M')";
		$fingerlingQuery .= "group by date_format(date,'%M')";
		$otherExpQuery .= "group by date_format(date,'%M')";
		
		//echo $incomeQuery.'<br/>'.$feedQuery.'<br/>'.$salaryQuery.'<br/>'.$salaryCountQuery.'<br/>'.$totalStockQuery.'<br/>'.$totalStockPondQuery.'<br/>'.$fingerlingQuery.'<br/>'.$otherExpQuery;
				
		$query = $this->db->query($feedQuery);
		$result = $query->result_array();
		
		$totalFeedExp = 0;
		$totalSalExp = 0;
		$totalFingExp = 0;
		$totalOthExp = 0;
		foreach ($result as $key => $value) {
			$feedCost = ($value['feedweight'] / ($value['no_of_bags'] * $value['weight'])) * $value['total_cost'];
			if(in_array($value['month'], $date_arr)){
					$now_count = array_search ($value['month'], $date_arr);
				}  else {
					$now_count = count($f_arr);
					$date_arr[$now_count] = $value['month'];					
				}
			//echo $value['feedweight'].'---'.$value['weight'].'----'.$value['total_cost'];
			
			$f_arr[$now_count]['month'] = $value['month'];
			$totalFeedExp += $f_arr[$now_count]['expAmt'] = number_format($feedCost,2,'.','');
			
			
		}
  

		//echo $sql_query1 .= " GROUP BY date( `created_date` )";

		//Salary expense calculation
		$query1 = $this->db->query($salaryQuery);
		$result1 = $query1->result_array();
					
		//get user total stock
		$stockTotQuery = $this->db->query($totalStockQuery);
		$stockTotData = $stockTotQuery->row_array();
		$totStock = ($stockTotQuery -> num_rows() > 0) ? $stockTotData['total_stock'] : 0;
		
		//get user with pond total stock
		$stockPondTotQuery = $this->db->query($totalStockPondQuery);
		$stockPondTotData = $stockPondTotQuery->row_array();
		$totPondStock = ($stockPondTotQuery -> num_rows() > 0) ? $stockPondTotData['total_pondstock'] : 0;
		if($totStock > 0 && $totPondStock > 0){
			foreach ($result1 as $key => $value) {
				$salCost1 = ($value['amount']) / $totStock;
				$salCost = number_format(($salCost1 * $totPondStock),2,'.','');
				//echo $value['amount'].'---'.$salCount.'----'.$totStock.'----'.$totPondStock.'<br/>';
				if(in_array($value['month'], $date_arr)){
					$now_count = array_search ($value['month'], $date_arr);
				}  else {
					$now_count = count($f_arr);
					$date_arr[$now_count] = $value['month'];
				}
				
				$f_arr[$now_count]['month'] = $value['month'];
				$totalSalExp += $f_arr[$now_count]['expAmt'] = $salCost;
				
									   
			}
		}
		
		$query3 = $this->db->query($fingerlingQuery);
		$result3 = $query3->result_array();
		if($totStock > 0 && $totPondStock > 0){
			foreach ($result3 as $key => $value) {
				if(in_array($value['month'], $date_arr)){
					$now_count = array_search ($value['month'], $date_arr);
				}  else {
					$now_count = count($f_arr);
					$date_arr[$now_count] = $value['month'];
					$f_arr[$now_count]['incAmt'] = 0;
				}
				
				$f_arr[$now_count]['month'] = $value['month'];
				$totalFingExp += $f_arr[$now_count]['expAmt'] = number_format($value['fingerlingAmt'],2,'.','');
				
									   
			}
		}
		
		$query4 = $this->db->query($otherExpQuery);
		$result4 = $query4->result_array();
		foreach ($result4 as $key => $value) {
			$othCost = ($value['othExpAmt'] / $totStock) * $totPondStock;
			//echo $value['othExpAmt'].'---'.$totStock.'----'.$totPondStock;
			if(in_array($value['month'], $date_arr)){
				$now_count = array_search ($value['month'], $date_arr);
			}  else {
				$now_count = count($f_arr);
				$date_arr[$now_count] = $value['month'];
				
			}
			
			$f_arr[$now_count]['month'] = $value['month'];
			$totalOthExp += $f_arr[$now_count]['expAmt'] = number_format($othCost,2,'.','');
			
								   
		}
		
		$totalExpense = $totalOthExp + $totalFingExp + $totalSalExp + $totalFeedExp;
		$othExpPerc = number_format((($totalOthExp / $totalExpense) * 100),2,'.','');
		$fingExpPerc = number_format((($totalFingExp / $totalExpense) * 100),2,'.','');
		$salExpPerc = number_format((($totalSalExp / $totalExpense) * 100),2,'.','');
		$feedExpPerc = number_format((($totalFeedExp / $totalExpense) * 100),2,'.','');
		
		$data[0][] = 'other_expenses';
        $data[0][] = number_format($othExpPerc, 2, '.', '');
        $data[1][] = 'feed_expenses';
        $data[1][] = number_format($feedExpPerc, 2, '.', '');
        $data[2][] = 'salary_expenses';
        $data[2][] = number_format($salExpPerc, 2, '.', '');
        $data[3][] = 'fingerling_expenses';
        $data[3][] = number_format($fingExpPerc, 2, '.', '');
        
        /* if ($pond_name != '' && $start_date != '' && $end_date != '') {
                $uid = $this->session->userdata('uid');

                $nooflivestock = "SELECT total_stocked FROM `tbl_livestock_catfish`where scid='" . $pond_name . "' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
                $nooflivestockquery = $this->db->query($nooflivestock);
                $resultlivestock = $nooflivestockquery->result_array();
                foreach ($resultlivestock as $key => $totlivestock) {
                    $totlivestock['total_stocked'];
                }
                $totnooflivestock = "SELECT total_stocked,avg_weight,sum(total_stocked) as total_stock FROM `tbl_livestock_catfish`where uid='$uid' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
                $totnooflivestockquery = $this->db->query($totnooflivestock);
                $resulttotlivestock = $totnooflivestockquery->result_array();
                foreach ($resulttotlivestock as $key => $restotlivestock) {
                    $restotlivestock['total_stock'];
                }
                $other_expense = "SELECT sum(amount) as amount FROM `tbl_catfishgenpurchased` where uid='$uid' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
                $otherexpquery = $this->db->query($other_expense);
                $resultotherexp = $otherexpquery->result_array();
                foreach ($resultotherexp as $key => $otherexp) {
                    $otherexp['amount'];
                }
                $otherexp1 = ($otherexp['amount'] / $restotlivestock['total_stock']);
                $other_expenses = $otherexp1 * $totlivestock['total_stocked'];
                $salaries = "SELECT sum(amount) as amount,created_date FROM `tbl_salaries` where uid='" . $uid . "' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "'";

                $salexpquery = $this->db->query($salaries);
                $resultsalexp = $salexpquery->result_array();
                foreach ($resultsalexp as $key => $resultsalexpenses) {
                    $date = 30;
                }
//echo "<pre>"; print_r($resultsalexp); echo "</pre>";
///exit();
                $salcalc = ($resultsalexpenses['amount'] / $date) / $restotlivestock['total_stock'];
                $salary_expenses = $salcalc * $totlivestock['total_stocked'];
                $feeding_query = "SELECT feed.size,feed.total_cost,cat.feedweight,(cat.feedweight*(feed.total_cost/feed.size)) as daily_feed FROM    `tbl_feedpurchased` feed join tbl_catfishfeed cat where cat.feedbrand=feed.brand and cat.scid='" . $pond_name . "' AND date(feed.created_date) BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
                //print_r($feeding_query);
                $feedingdata = $this->db->query($feeding_query);
                $resultfeed = $feedingdata->result_array();
                foreach ($resultfeed as $key => $feed) {
                    $feed_expenses = $feed['daily_feed'];
                }


                $fingerling_query = "SELECT sum(total_cost)as total_cost from tbl_fingerlingpurchased where pond_name='" . $pond_name . "' AND date(`created_date`) BETWEEN '" . $start_date . "' AND '" . $end_date . "' ";
                $fingerlingdata = $this->db->query($fingerling_query);
                $resultfingerling = $fingerlingdata->result_array();
                foreach ($resultfingerling as $key => $fingerling) {
                    $fingerling_expenses = $fingerling['total_cost'];
                }
            } else if ($pond_name != '') {
                $uid = $this->session->userdata('uid');

//$uid=6;
                $nooflivestock = "SELECT total_stocked FROM `tbl_livestock_catfish`where scid='" . $pond_name . "'";
                $nooflivestockquery = $this->db->query($nooflivestock);
                $resultlivestock = $nooflivestockquery->result_array();
                foreach ($resultlivestock as $key => $totlivestock) {
                    $totlivestock['total_stocked'];
                }
                $totnooflivestock = "SELECT total_stocked,avg_weight,sum(total_stocked) as total_stock FROM `tbl_livestock_catfish`where uid='$uid'";
                $totnooflivestockquery = $this->db->query($totnooflivestock);
                $resulttotlivestock = $totnooflivestockquery->result_array();
                foreach ($resulttotlivestock as $key => $restotlivestock) {
                    $restotlivestock['total_stock'];
                }
                $other_expense = "SELECT sum(amount) as amount FROM `tbl_catfishgenpurchased` where uid='$uid'";
                $otherexpquery = $this->db->query($other_expense);
                $resultotherexp = $otherexpquery->result_array();
                foreach ($resultotherexp as $key => $otherexp) {
                    $otherexp['amount'];
                }
                $otherexp1 = ($otherexp['amount'] / $restotlivestock['total_stock']);
                $other_expenses = $otherexp1 * $totlivestock['total_stocked'];
                $salaries = "SELECT sum(amount) as amount,created_date FROM `tbl_salaries` where uid='" . $uid . "'";

                $salexpquery = $this->db->query($salaries);
                $resultsalexp = $salexpquery->result_array();
                foreach ($resultsalexp as $key => $resultsalexpenses) {
                    $date = 30;
                }
                $salcalc = ($resultsalexpenses['amount'] / $date) / $restotlivestock['total_stock'];
                $salary_expenses = $salcalc * $totlivestock['total_stocked'];
                $feeding_query = "SELECT feed.size,feed.total_cost,cat.feedweight,(cat.feedweight*(feed.total_cost/feed.size)) as daily_feed FROM    `tbl_feedpurchased` feed join tbl_catfishfeed cat where cat.feedbrand=feed.brand and cat.scid='" . $pond_name . "'";
                //print_r($feeding_query);
                $feedingdata = $this->db->query($feeding_query);
                $resultfeed = $feedingdata->result_array();
                foreach ($resultfeed as $key => $feed) {
                    $feed_expenses = $feed['daily_feed'];
                }
                $fingerling_query = "SELECT sum(total_cost)as total_cost from tbl_fingerlingpurchased where pond_name='".$pond_name."'  ";
                //print_r($fingerling_query);
                $fingerlingdata = $this->db->query($fingerling_query);
                $resultfingerling = $fingerlingdata->result_array();
                foreach ($resultfingerling as $key => $fingerling) {
                    $fingerling_expenses = $fingerling['total_cost'];
                    //print_r($fingerling_expenses);
                }
            }
        $data[0][] = 'other_expenses';
        $data[0][] = number_format($other_expenses, 2, '.', '');
        $data[1][] = 'feed_expenses';
        $data[1][] = number_format($feed_expenses, 2, '.', '');
        $data[2][] = 'salary_expenses';
        $data[2][] = number_format($salary_expenses, 2, '.', '');
        $data[3][] = 'fingerling_expenses';
        $data[3][] = number_format($fingerling_expenses, 2, '.', ''); */

        //echo "<pre>";                print_r($data); echo "<pre>";  

        print json_encode($data, JSON_NUMERIC_CHECK);
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
