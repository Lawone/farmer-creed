<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:62%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-7" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2>Add Catfish Expense</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >
							<fieldset>
								<div class="row">
									<section class="col col-10">
										<div class="form-group">
											<label>Date</label>
											   <div class="input-group">
												<input type="text" name="date" class="form-control datepicker" data-dateformat="dd/mm/yy">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</section>
									<section class="col col-10">
									<label class="label">Pond Name</label>
									<label class="select">
										<select name="pond_name">
										<option value="" selected="" disabled="">Please select Pond name</option>
										<?php
											 if ($getpondname != false) {
												 foreach ($getpondname as $getpondnames) {
													echo '<option value="'.$getpondnames['scid'].'">'.$getpondnames['pond_name'].'</option>';
												}
											}
											?>
										</select> <i></i>
										<?php echo form_error('pond_name'); ?>
										</label>
									</section>
									<section class="col col-10">
									<label class="label">Feed</label>
										<label class="input">
										<input id="feed" type="text" name="feed" style="text-align: right">
										</label>
										<?php echo form_error('feed'); ?>
										</label>
									</section>
									<section class="col col-10">
									<label class="label">Salaries</label>
										<label class="input">
										<input id="salaries" type="text" name="salaries" style="text-align: right">
										</label>
										<?php echo form_error('salaries'); ?>
										</label>
									</section>
									<section class="col col-10">
									<label class="label">Fingerling</label>
										<label class="input">
										<input id="fingerling" type="text" name="fingerling" style="text-align: right">
										</label>
										<?php echo form_error('fingerling'); ?>
										</label>
									</section>
									<section class="col col-10">
									<label class="label">Other Exp</label>
										<label class="input">
										<input id="other_exp" type="text" name="other_exp" style="text-align: right">
										</label>
										<?php echo form_error('other_exp'); ?>
										</label>
									</section>
								</div>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary">
									Save
								</button>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" onclick="window.history.back();" class="btn btn-default" type="button">
													Back
												</button>
							</footer>
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>
			</div>

		</div>
		<script type="text/javascript">
		
			runAllForms();

			$(function() {

				// Time 
				$('#time').timepicker();

				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						date : {
							required : true
						},
						pond_name : {
							required : true
						},
						feed : {
							required : true,
							number : true
						},
						salaries : {
							required : true,
							number : true
						},
						fingerling : {
							required : true,
							number : true
						},
						other_exp : {
							required : true,
							number : true
						},						
					},

					// Messages for form validation
					messages : {
						date : {
							required : 'Please Select Date'
						},
						pond_name : {
							required : 'Please Select Pond Name'
						},
						feed : {
							required : 'Please Enter Feed',
							number : 'Please Enter only valid number'
						},
						salaries : {
							required : 'Please Enter Salaries',
							number : 'Please Enter only valid number'
						},
						fingerling : {
							required : 'Please Enter Fingerling',
							number : 'Please Enter only valid number'
						},
						other_exp : {
							required : 'Please Enter Other Exp',
							number : 'Please Enter only valid number'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
		
