<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
 @media (max-width : 360px){
 .mob {
		margin-top:-2.5em !important;
		margin-left:12em !important;
}
 @media (max-width : 320px){
 .mob {
		margin-top:-2.5em !important;
		margin-left:11em !important;
}
}
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:62%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-7" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2>Edit Feed Purchased</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">

						<form action="" id="smart-form-farmer" class="smart-form" method="post" >

							<fieldset>
								<div class="row">
									<section class="col col-10">
									<label class="label">No. of Bags</label>
										<label class="input">
											<input type="text" name="no_of_bags" id="no_of_bags" style="text-align:right;" value="<?php echo set_value('no_of_bags', isset($broilerfeedpurchased_e['no_of_bags']) ? $broilerfeedpurchased_e['no_of_bags'] : ''); ?>">
										</label>
										<?php echo form_error('no_of_bags'); ?>
									</section>
								</div>
								<div class="row">
									<section class="col col-10">
									<label class="label">Type</label>
									<label class="select">
										<select name="type">
											<option value="" selected="" disabled="">Please Select Type</option>
											<option value="Broiler Starter" <?php if(isset($broilerfeedpurchased_e['type']) && $broilerfeedpurchased_e['type'] == 'Broiler Starter'){ echo "selected='selected'";}?>>Broiler Starter</option>
											<option value="Broiler Finisher" <?php if(isset($broilerfeedpurchased_e['type']) && $broilerfeedpurchased_e['type'] == 'Broiler Finisher'){ echo "selected='selected'";}?>>Broiler Finisher</option>
										</select> <i></i>
									</label>
									<?php echo form_error('type'); ?>
									</section>
								</div>
								<div class="row">
									<section class="col col-10">
									<label class="label">Weight per bag</label>
										<label class="input">
											<input type="text" name="weight" id="weight" style="text-align:right;" value="<?php echo set_value('weight', isset($broilerfeedpurchased_e['weight']) ? $broilerfeedpurchased_e['weight'] : ''); ?>" >
										</label>
										<?php echo form_error('weight'); ?>
									</section>
									<section class="col col-2">
										<label class="label">&#160;</label>
										<label class="input mob">
											<span style="float:left; margin-left:-24px;margin-top:5px">(kg)</span>
										</label>
									</section>
								</div>
								<div class="row">
									<section class="col col-10">
									<label class="label">Brand</label>
										<label class="input">
											<input type="text" name="brand" id="brand" value="<?php echo set_value('brand', isset($broilerfeedpurchased_e['brand']) ? $broilerfeedpurchased_e['brand'] : ''); ?>" >
										</label>
									<?php echo form_error('brand'); ?>
									</section>
								</div>
								<div class="row">
									<section class="col col-10">
									<label class="label">Total Cost</label>
										<label class="input">
											<input type="text" name="total_cost" id="total_cost" style="text-align:right;" value="<?php echo set_value('total_cost', isset($broilerfeedpurchased_e['total_cost']) ? $broilerfeedpurchased_e['total_cost'] : ''); ?>" >
										</label>
									<?php echo form_error('total_cost'); ?>
									</section>
								</div>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary">
									Save
								</button>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" onclick="window.history.back();" class="btn btn-default" type="button">
													Back
												</button>
							</footer>
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>
			</div>

		</div>
		<script type="text/javascript">
	
			runAllForms();

			$(function() {
			
			
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						no_of_bags : {
							required : true,
							number : true
						},
						weight : {
							required : true,
							number : true
						},
						brand : {
							required : true
						},
						total_cost : {
							required : true,
							number : true
						},
					},

					// Messages for form validation
					messages : {
						no_of_bags : {
							required : 'Please enter NO of bags',
							number : 'Please enter valid number only'
						},
						weight : {
							required : 'Please enter Weight',
							number : 'Please enter valid number only'
						},
						brand : {
							required : 'Please enter Brand'
						},
						total_cost : {
							required : 'Please enter Total Cost',
							number : 'Please enter valid number only'
						},
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
		
