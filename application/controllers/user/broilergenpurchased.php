<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broilergenpurchased extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('broilergenpurchased_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function broilergenpurchased_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->broilergenpurchased_model->broilergenpurchased_add();
			$this->session->set_flashdata('message', 'Broiler general purchased has been added successfully.');
			redirect(base_url().'user/broilergenpurchased/broilergenpurchased_add', 'refresh');
		}
		
		$this->load->view('user/broilergenpurchased_add');
	}
	
	//user edit for notification
	public function broilergenpurchased_edit($bgpid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->broilergenpurchased_model->broilergenpurchased_edit($bgpid);
			$this->session->set_flashdata('message', 'Broiler general purchased has been changed successfully.');
			redirect(base_url().'user/broilergenpurchased/broilergenpurchased_view', 'refresh');
		}
		$data['broilergenpurchased_e']=$this->broilergenpurchased_model->getBroilergenpurchased($bgpid);
		$this->load->view('user/broilergenpurchased_edit',$data);
	}
	
	// domain view method
	public function broilergenpurchased_view() {
		$this->db->select('bgpid,date,description,amount,user_type,staff_id')->from('tbl_broilergenpurchased');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['broilergenpurchased_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/broilergenpurchased_view', $data);
	}
	
	//user delete method
	public function broilergenpurchased_delete($bgpid){
		
		$this->broilergenpurchased_model->broilergenpurchased_delete($bgpid);
		$this->session->set_flashdata('message', 'Catfish general purchased has been deleted successfully.');
		redirect(base_url().'user/broilergenpurchased/broilergenpurchased_view', 'refresh');
	}
}
