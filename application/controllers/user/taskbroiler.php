<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taskbroiler extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('taskbroiler_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function litterchange_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
						
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->litterchange_add();
			$this->session->set_flashdata('message', 'Litterchange has been added successfully.');
			redirect(base_url().'user/taskbroiler/litterchange_add', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getlitterchangepond();
		$this->load->view('user/litterchange_add', $data);
	}
	
	//user edit for notification
	public function litterchange_edit($tlbid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->litterchange_edit($tlbid);
			$this->session->set_flashdata('message', 'Litterchange has been changed successfully.');
			redirect(base_url().'user/taskbroiler/litterchange_view', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getlitterchangepond();
		$data['litterchange_e']=$this->taskbroiler_model->gettasklitterchange($tlbid);
		$this->load->view('user/litterchange_edit',$data);
		}
	
	// domain view method
	public function litterchange_view() {
				$this->db->select('tlb.tlbid,tlb.created_date,tlb.created_date,tlb.user_type,sb.pen_name,tlb.staff_id')->from('tbl_litterchange tlb');
				$this->db->join('tbl_structure_broiler sb','tlb.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('tlb.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('tlb.staff_id' => $this->session->userdata('uid')));
		}
		// data subset
		$query = $this->db->get();
		$data['litterchange_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;
		
		$this->load->view('user/litterchange_view', $data);
	}
	//user delete method
		public function litterchange_delete($tlbid){
		
		$this->taskbroiler_model->litterchange_delete($tlbid);
		$this->session->set_flashdata('message', 'Litterchange has been deleted successfully.');
		redirect(base_url().'user/taskbroiler/litterchange_view', 'refresh');
	}
	/*============Broiler Feeding===================*/
	
	public function broilerfeed_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedweight', 'Feed Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedbrand', 'Feed Brand', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('feedtype', 'Feed Type', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');	
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->broilerfeed_add();
			$this->session->set_flashdata('message', 'Broiler Feeding has been added successfully.');
			redirect(base_url().'user/taskbroiler/broilerfeed_add', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getbroilerfeedpond();
		$data['getbroilerfeedbrand'] = $this->taskbroiler_model->getBroilerfeedbrand();
		$this->load->view('user/broilerfeed_add', $data);
	}
	
	//user edit for notification
	public function broilerfeed_edit($bfid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedweight', 'Feed Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedbrand', 'Feed Brand', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('feedtype', 'Feed Type', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->broilerfeed_edit($bfid);
			$this->session->set_flashdata('message', 'Broiler Feeding has been changed successfully.');
			redirect(base_url().'user/taskbroiler/broilerfeed_view', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getBroilerfeedpond();
		$data['getbroilerfeedbrand'] = $this->taskbroiler_model->getBroilerfeedbrand();
		$data['broilerfeed_e']=$this->taskbroiler_model->getBroilerfeeding($bfid);
		$this->load->view('user/broilerfeed_edit',$data);
	}
	
	// domain view method
	public function broilerfeed_view() {
		$this->db->select('bf.bfid,bf.created_date,bf.user_type,bf.feedtype,bf.feedweight,bf.feedbrand,bf.staff_id,sb.pen_name,bfp.brand')->from('tbl_broilerfeed bf');
		$this->db->join('tbl_structure_broiler sb','bf.sbid = sb.sbid');
		$this->db->join('tbl_broilerfeedpurchased bfp','bfp.bfpid = bf.bfpid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('bf.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('bf.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['broilerfeed_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/broilerfeed_view', $data);
	}
	
	//user delete method
	public function broilerfeed_delete($bfid){
		
		$this->taskbroiler_model->broilerfeed_delete($bfid);
		$this->session->set_flashdata('message', 'Broiler Feeding has been deleted successfully.');
		redirect(base_url().'user/taskbroiler/broilerfeed_view', 'refresh');
	}
	
	
	/*============Broiler Weight===================*/
	
	public function broilerweight_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');
					
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->broilerweight_add();
			$this->session->set_flashdata('message', 'Broiler Weight has been added successfully.');
			redirect(base_url().'user/taskbroiler/broilerweight_add', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getBroilerweightpond();
		$this->load->view('user/broilerweight_add', $data);
	}
	
	//user edit for notification
	public function broilerweight_edit($uid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Feed Weight', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->broilerweight_edit($uid);
			$this->session->set_flashdata('message', 'Broiler Weight has been changed successfully.');
			redirect(base_url().'user/taskbroiler/broilerweight_view', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getBroilerweightpond();
		$data['broilerweight_e']=$this->taskbroiler_model->getBroilerweight($uid);
		$this->load->view('user/broilerweight_edit',$data);
	}
	
	// domain view method
	public function broilerweight_view() {
		$this->db->select('bw.bwid,bw.created_date,bw.avg_weight,bw.user_type,sb.pen_name,bw.staff_id')->from('tbl_broilerweight bw');
		$this->db->join('tbl_structure_broiler sb','bw.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('bw.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('bw.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['broilerweight_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;
		$this->load->view('user/broilerweight_view', $data);
	}
	//user delete method
	public function broilerweight_delete($bwid){
	
		$this->taskbroiler_model->broilerweight_delete($bwid);
		$this->session->set_flashdata('message', 'Broiler Feeding has been deleted successfullY.');
		redirect(base_url().'user/taskbroiler/broilerweight_view', 'refresh');
		}
		
		/*============Broiler Mortality===================*/
	
	public function broilermortality_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('mortality', 'Mortality', 'trim|required|callback_checkMortality|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');			
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->broilermortality_add();
			$this->session->set_flashdata('message', 'Broiler Mortality has been added successfully.');
			redirect(base_url().'user/taskbroiler/broilermortality_add', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getMortalitypond();
		$this->load->view('user/broilermortality_add', $data);
	}
	public function checkMortality()
	{
		$this->db->select('total_stocked')->from('tbl_livestock_broiler')->where(array('sbid' => $this->input->post('sbid')));
		$noofMortality = $this->db->get();
		$Mortality = $noofMortality->row_array();
		
		if($Mortality['total_stocked'] > $this->input->post('mortality')){
			return true;
		}
		else{
			$this->form_validation->set_message('checkMortality', 'Mortality is greater than total no of stocked.');
			return false;
		}
	}
	//user edit for notification
	public function broilermortality_edit($brmid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('mortality', 'Mortality', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->broilermortality_edit($brmid);
			$this->session->set_flashdata('message', 'Broiler Mortality has been changed successfully.');
			redirect(base_url().'user/taskbroiler/broilermortality_view', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getMortalitypond();
		$data['broilermortality_e']=$this->taskbroiler_model->getBroilerMortality($brmid);
		$this->load->view('user/broilermortality_edit',$data);
	}
	
	// domain view method
	public function broilermortality_view() {
		$this->db->select('bm.bmid,bm.created_date,bm.user_type,bm.mortality,sb.pen_name,bm.staff_id')->from('tbl_broiler_mortality bm');
		$this->db->join('tbl_structure_broiler sb','bm.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('bm.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('bm.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['broilermortality_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;
		$this->load->view('user/broilermortality_view', $data);
	}
	//user delete method
	public function broilermortality_delete($cfmid){
	
		$this->taskbroiler_model->broilermortality_delete($cfmid);
		$this->session->set_flashdata('message', 'Broiler Mortality has been deleted successfullY.');
		redirect(base_url().'user/taskbroiler/broilermortality_view', 'refresh');
		}
	/*============Broiler Treatment===================*/
	
	public function broilertreatment_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('vaccine', 'Vaccine', 'trim|required|xss_clean');
		$this->form_validation->set_rules('treatment', 'Treatment', 'trim|required|xss_clean');
					
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->broilertreatment_add();
			$this->session->set_flashdata('message', 'Broiler Treatment has been added successfully.');
			redirect(base_url().'user/taskbroiler/broilertreatment_add', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getTreatmentpond();
		$this->load->view('user/broilertreatment_add', $data);
	}
	
	//user edit for notification
	public function broilertreatment_edit($btid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('vaccine', 'Vaccine', 'trim|required|xss_clean');
		$this->form_validation->set_rules('treatment', 'Treatment', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskbroiler_model->broilertreatment_edit($btid);
			$this->session->set_flashdata('message', 'Broiler Treatment has been changed successfully.');
			redirect(base_url().'user/taskbroiler/broilertreatment_view', 'refresh');
		}
		$data['getpondname'] = $this->taskbroiler_model->getTreatmentpond();
		$data['broilertreatment_e']=$this->taskbroiler_model->getBroilerTreatment($btid);
		$this->load->view('user/broilertreatment_edit',$data);
	}
	
	// domain view method
	public function broilertreatment_view() {
				$this->db->select('bt.btid,bt.created_date,bt.created_date,bt.user_type,bt.vaccine,bt.treatment,sb.pen_name,bt.staff_id')->from('tbl_broilertreatment bt');
				$this->db->join('tbl_structure_broiler sb','bt.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('bt.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('bt.staff_id' => $this->session->userdata('uid')));
		}
		// data subset
		$query = $this->db->get();
		$data['broilertreatment_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;
		
		$this->load->view('user/broilertreatment_view', $data);
	}
	//user delete method
	public function broilertreatment_delete($btid){
	
		$this->taskbroiler_model->broilertreatment_delete($btid);
		$this->session->set_flashdata('message', 'Broiler Treatment has been deleted successfullY.');
		redirect(base_url().'user/taskbroiler/broilertreatment_view', 'refresh');
		}		
}
