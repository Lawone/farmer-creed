<?php
Class Salaries_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function salaries_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$salaries_add = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'staff' => $this->input->post('staff'),
			'amount' => $this->input->post('amount'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),	
		);
		//print_r($chickpurchased_add);exit;
		$this->db->insert('tbl_salaries', $salaries_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function salaries_edit($ssid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$salaries_edit = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'staff' => $this->input->post('staff'),
			'amount' => $this->input->post('amount'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
		);
			
		$this->db->where('ssid', $ssid);
		$this->db->update('tbl_salaries',$salaries_edit);
		
	}
	
	public function getSalaries($ssid)
	{	
		$this->db->select('*')->from('tbl_salaries')->where('ssid', $ssid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}

	public function getSalariesStaff()
	{	
		$this->db->select('*')->from('tbl_users')->where('parent_uid', $this->session->userdata('uid'));
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}	
	
	public function salaries_delete($ssid){
		
		$where = array('ssid' => $ssid);
		$this->db->where($where);
		$this->db->delete('tbl_salaries');
		
	}
	public function getStaffname($staff_id){
	
		$this->db->select('id,firstname')->from('tbl_users')->where('id', $staff_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;		
	}
}
