<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px" >

			<!-- MAIN CONTENT -->
			<div id="content" class="container" >

				<div class="row" >
					<!-- NEW COL START -->
					<?php
					if($this->session->flashdata('message')): echo '<div class="alert alert-success" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
					if($this->session->flashdata('error_message')): echo '<div class="alert alert-danger" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('error_message').'</div>'; endif;
		?>
					<center>
						<article class="col-sm-12 col-md-12 col-lg-6" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
				
							<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2 style="font-size: 100%">Select the stocking system that would suit your goal.</h2>
				</header>
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
												
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >
							<fieldset>
									<div>
									<section class="col col-6 col-6a">
									<label>All In - All Out</label>
									</section>
									<section class="col col-6 col-6a">
									 <label>
											<input type="checkbox" name="allin_allout" id="allin_allout"  value="1" <?php echo set_checkbox('allin_allout', '1',(['allin_allout']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
									<section class="col col-6 col-6a">
									<label>Batch</label>
									</section>
									<section class="col col-6 col-6a">
										<label>
											<input type="checkbox" name="batch" id="batch"  value="1" <?php echo set_checkbox('batch', '1',(['batch']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
						
										</div>
										</fieldset>
<div style="margin-left:2em;">	
<p>All In - All Out: <br> Stock all ponds at the same time and have a large harvest with a long waiting period to next harvest.</p>
<p>&#160;</p>
<p>Batch Stocking:<br>
Having a staggered stocking system, to ensure continuous harvest of smaller quantities at various times.</p>
</div>
																	<footer>
								<a class="btn btn-primary" href="<?php echo base_url().'user/settings/running_expenses'?>">Next</a>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" class="btn btn-default" type="submit">
													Submit
												</button>
							</footer>
										</form>
										
															<!--<script type="text/javascript">
		
			runAllForms();

			$(function() {
			
			
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						allin_allout   : {
							required : true
													},
						batch : {
							required : true
													},
					},

					// Messages for form validation
					messages : {
						allin_allout  : {
							required : 'Please Enter All In - All Out',
							},
						batch : {
							required : 'Please Enter Batch',
							},
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script> -->
					
</body>
</html>
										
										
