<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:62%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-7" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2>Edit Catfish Sales</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">

						<form action="" id="smart-form-farmer" class="smart-form" method="post" >

							<fieldset>
								<div class="row">
									<section class="col col-10">
										<div class="form-group">
											<label>Date</label>
											   <div class="input-group">
												<input type="text" readonly name="date" class="form-control datepicker" data-dateformat="dd/mm/yy" value="<?php echo set_value('date', isset($catfishsales_e['date']) ? $catfishsales_e['date'] : ''); ?>">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										<?php echo form_error('date'); ?>
										</div>
									</section>
									<section class="col col-10">
									<label class="label">Pond Name</label>
									<label class="select">
									 	<select name="pond_name">
											<?php 
											 if ($getpondname != false) {
												 foreach ($getpondname as $getpondnames) {
													if($getpondnames['scid'] == $catfishsales_e['pond_name'])
													echo '<option value="'.$getpondnames['scid'].'" selected>'.$getpondnames['pond_name'].'</option>';
													else
														echo '<option value="'.$getpondnames['scid'].'">'.$getpondnames['pond_name'].'</option>';
					
												}
											}
											?>
										</select> <i></i>
										<?php echo form_error('pond_name'); ?>
										</label>
									</section>
									<section class="col col-10">
										<div class="sales">
											<label class="label">No of Fish</label>
										<label class="input">
											<input type="text" name="no_of_fish" id="no_of_fish" style="text-align: right" value="<?php echo set_value('no_of_fish', isset($catfishsales_e['no_of_fish']) ? $catfishsales_e['no_of_fish'] : ''); ?>">
<?php echo form_error('no_of_fish'); ?></label>
										</div>
									</section>
									<div>
									<section class="col col-10">
										<div class="total_weight">
											<label class="label">Total Weight</label>
										<label class="input">
											<input type="text" name="total_weight" id="total_weight" style="text-align: right" value="<?php echo set_value('total_weight', isset($catfishsales_e['total_weight']) ? $catfishsales_e['total_weight'] : ''); ?>">
<?php echo form_error('total_weight'); ?></label>
</section>
									<section class="col col-2">
										<label class="label">&#160;</label>
										<label class="input mob">
											<span style="float:left; margin-left:-24px;margin-top:5px">(kg)</span>
										</label>
									</section>
										</div>
																		<section class="col col-10">
										<div class="total_cost">
											<label class="label">Total Cost</label>
										<label class="input">
											<input type="text" name="total_cost" id="total_cost" style="text-align: right" value="<?php echo set_value('total_cost', isset($catfishsales_e['total_cost']) ? $catfishsales_e['total_cost'] : ''); ?>">
<?php echo form_error('total_cost'); ?></label>
										</div>
									</section>
								</div>
							<footer>
								<button type="submit" class="btn btn-primary">
									Save
								</button>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" onclick="window.history.back();" class="btn btn-default" type="button">
													Back
												</button>
							</footer>
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>
			</div>

		</div>
		<script type="text/javascript">
	
			runAllForms();

			$(function() {
				// Time 					
				$('#time').timepicker();
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						pond_name : {
							required : true
						},
						no_of_fish : {
							required : true,
							number : true
						},
						total_weight : {
							required : true,
							number : true
						},
						total_cost : {
							required : true,
							number : true
						}
					},

					// Messages for form validation
					messages : {
						pond_name : {
							required : 'Please Select Pond Name'
						},
						no_of_fish : {
							required : 'Please Enter No of fish',
							number : 'Please Enter valid numbers only'
						},
						total_weight : {
							required : 'Please Enter Total Weight',
							number : 'Please Enter valid numbers only'
						},
						total_cost : {
							required : 'Please Enter Total Cost',
							number : 'Please Enter valid numbers only'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
		
