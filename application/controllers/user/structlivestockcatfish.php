<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Structlivestockcatfish extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('structlivestockcatfish_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|callback_alreadyExistpond|xss_clean');
		$this->form_validation->set_rules('stock_density', 'Stock Density', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_stocked', 'Total Stocked', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
			
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->structlivestockcatfish_model->add();
			$this->session->set_flashdata('message', 'Catfish has been added successfully.');
			redirect(base_url().'user/structlivestockcatfish/add', 'refresh');
		}
		$data['getpondname'] = $this->structlivestockcatfish_model->getStructlivestockcatfish();
		$this->load->view('user/structlivestockcatfish_add',$data);
	}
	public function alreadyExistpond() {
		$this->db->select('scid')->from('tbl_livestock_catfish')->where(array('scid' => $this->input->post('scid'),'uid' => $this->session->userdata('uid')));
		$pond = $this->db->get();
		$existsPond = ($pond -> num_rows() > 0) ? $pond->row_array() : false;
		if($existsPond != false){
			$this->form_validation->set_message('alreadyExistpond', 'Sorry this pondname is Already Exist.');
			return false;
		}
		else{
			return true;
		}
	}
	//user edit for notification
	public function edit($uid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('stock_density', 'Stock Density', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_stocked', 'Total Stocked', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->structlivestockcatfish_model->edit($uid);
			$this->session->set_flashdata('message', 'Catfish has been changed successfully.');
			redirect(base_url().'user/structlivestockcatfish/view', 'refresh');
		}
		$data['getpondname'] = $this->structlivestockcatfish_model->getStructlivestockcatfish();
		$data['structlivestockcatfish_e']=$this->structlivestockcatfish_model->getStructlivestock($uid);
		$this->load->view('user/structlivestockcatfish_edit',$data);
	}
	
	// domain view method
	public function view() {
		$this->db->select('lc.lcid,lc.scid,lc.created_date,lc.stock_density,lc.total_stocked,lc.avg_weight,lc.staff_id,sc.pond_name')->from('tbl_livestock_catfish lc');
		$this->db->join('tbl_structure_catfish sc','lc.scid = sc.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('lc.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('lc.staff_id' => $this->session->userdata('uid')));
		}
		// data subset
		$query = $this->db->get();
		$data['structlivestockcatfish_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/structlivestockcatfish_view', $data);
	}
	
	//user delete method
	public function delete($scid){
		
		$this->structlivestockcatfish_model->delete($scid);
		$this->session->set_flashdata('message', 'Catfish has been deleted successfully.');
		redirect(base_url().'user/structlivestockcatfish/view', 'refresh');
	}
}

