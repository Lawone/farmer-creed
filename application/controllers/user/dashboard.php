<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->library(array('email', 'encrypt'));  // load library classes
        $this->load->model(array('user_model','catfishsales_model','broilersales_model','mail_model'));
    }

    public function index() {
        /* if($this->session->userdata('user'))
          {
          $data['user']	= $this->session->userdata('user');
          $this->load->view('home', $data);
          }
          else
          {
          //If no session, redirect to login page
          redirect('login', 'refresh');
          } */
    }

    // Client login method
    public function login() {
        // loading libraries and helpers
        $this->load->library(array('form_validation'));
        $this->load->helper(array('form'));
        // validation rules
        $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]|max_length[20]|xss_clean');
        // Error deliminators
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        // Form validation action
        if ($this->form_validation->run() == TRUE) {
            $this->user_model->check_login();
        }
        // load default view
        $this->load->view('user/login');
    }

    public function home() {
        $data['getpondname'] = $this->catfishsales_model->getcatfishsalespond();
        $data['getpenname'] = $this->broilersales_model->getBroilersalespen();
        $this->load->view('user/home',$data);
    }

    public function register() {
        if (isset($_POST['register'])) {
            $this->form_validation->set_error_delimiters('<em class="invalid">', '</em>');
            //print_r($_POST);exit;
            $this->form_validation->set_rules('username', 'User Name', 'trim|required|callback_alreadyExistUser|max_length[150]|xss_clean');
            $this->form_validation->set_rules('email', 'User E-mail', 'trim|required|callback_alreadyExistEmail|max_length[128]|xss_clean');
            $this->form_validation->set_rules('password', 'password', 'required|min_length[8]|max_length[20]|sha1|xss_clean');
            $this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]|xss_clean');
            //$this->form_validation->set_rules('farmname','Farm','trim|required|max_length[32]');
            $this->form_validation->set_rules('phone', 'Phone number', 'trim|required|max_length[40]|xss_clean');
            //$this->form_validation->set_rules('gender','Gender','trim|required|max_length[10]');
            //$this->form_validation->set_message('is_unique', 'Username is already Exists');
            //$this->form_validation->set_message('is_unique', 'User E-mail is already Exists');
			
			$vfcnCode = $this->generate_code();
			
            //check validation true
            if ($this->form_validation->run() == TRUE) {
                $arr['username'] = $this->input->post('username');
                $arr['email'] = $this->input->post('email');
                $arr['password'] = $this->input->post('password');
                $arr['firstname'] = $this->input->post('firstname');
                $arr['farmname'] = $this->input->post('farmname');
                $arr['phone'] = $this->input->post('phone');
                $arr['gender'] = $this->input->post('gender');
                $arr['address1'] = $this->input->post('address1');
                $livestock = implode(",", $this->input->post('livestock'));
                $arr['livestock'] = $livestock;
                $arr['marital_status'] = $this->input->post('marital_status');
                $arr['verify_code'] = $vfcnCode;
                //$arr['status'] = 1;


                //$arr['user_type']   = $this->input->post('type');
                $arr['email_subscribe'] = '';
                $this->user_model->register($arr);
                $insert_id = $this->db->insert_id();
                if ($insert_id) {
					$to = $this->input->post('email');
					$subject = "Welcome to Farmers Creed - Please confirm verification link";
					$info = array('firstname' => $this->input->post('firstname'),'vfcnCode' => $vfcnCode,'email' => $this->input->post('email'));
					$message = $this->mail_model->regTemp($info);
					$regInfo = array('to' => $to,'subject' => $subject,'message' => $message);
					$this->mail_model->sendEmail($regInfo);
					
					$adminTo = "info@farmerscreed.com";
					$adminSubject = "New user has been registered";
					$adminInfo = array('firstname' => $this->input->post('firstname'),'username' => $this->input->post('username'),'email' => $this->input->post('email'),'farmname' => $this->input->post('farmname'),'phone' => $this->input->post('phone'),'address' => $this->input->post('address1'));
					$adminMessage = $this->mail_model->regInfoAdmin($adminInfo);
					$adminRegInfo = array('to' => $adminTo,'subject' => $adminSubject,'message' => $adminMessage);
					$this->mail_model->sendEmail($adminRegInfo);
                    
					//$this->session->set_flashdata('success_message', 'Farmer registration has been Saved Successfully.');
					$this->session->set_flashdata('success_message', 'verification link has been sent to your email address. please click the link to activate it.');
                    redirect(base_url().'user/dashboard/register', 'refresh');
                    
                }
            }
        }
        $data['list'] = $this->input->post('gender');
        $data['list1'] = $this->input->post('marital_status');
        $this->load->view('user/fregister', $data);
    }
	
	public function generate_code() {
		$string  = date('Y-m-d H:i:s');
		return md5($string);
	}

    public function alreadyExistUser() {
        $this->db->select('username')->from('tbl_users')->where(array('username' => $this->input->post('username')));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->form_validation->set_message('alreadyExistUser', 'Username already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function alreadyExistEmail() {
        $this->db->select('email')->from('tbl_users')->where(array('email' => $this->input->post('email')));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->form_validation->set_message('alreadyExistEmail', 'Email already exists.');
            return false;
        } else {
            return true;
        }
    }

    //forget password
    public function forgot_password() {
        $data['page_name'] = 'dashboard';
        // loading libraries and helpers
        $this->load->library(array('form_validation'));
        $this->load->helper(array('form'));
        // validation rules
        $this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|xss_clean');
        // Error deliminators
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        // Form validation action
        if ($this->form_validation->run() == TRUE) {
            $this->user_model->recover_password();
            $this->session->set_flashdata('success_message', 'New password has been sent to your email address.');
            redirect(base_url() . "user/dashboard/forgot_password", "refresh");
        }
        $this->load->view('user/forgot_password', $data);
    }

    public function emailVerification($token) {
        $email_verification = $this->user_model->emailVerificationCheck($token);
        if($email_verification != false){
			if($email_verification['status'] == 1){
				$this->session->set_flashdata('success_message', 'Your email has been already verified.');
				redirect(base_url().'user/dashboard/login', 'refresh');
			}else if($email_verification['status'] == 0){
				$this->user_model->emailStatusChange($token);
				$this->session->set_flashdata('success_message', 'Email has been verified successfully. Now login to your account by entering the details below.');
				redirect(base_url().'user/dashboard/login', 'refresh');
			}
		}else{
			$this->session->set_flashdata('invalidCode', 'Verification code is invalid.');
			redirect(base_url().'user/dashboard/login', 'refresh');
		}
    }

    public function chgpassword($uid, $token) {
        $matchValue = $this->user_mdl->tokenVerificationCheck($token);
        if ($matchValue['token'] == $token) {
            $data['page_name'] = 'dashboard';
            // loading libraries and helpers
            $this->load->library(array('form_validation'));
            $this->load->helper(array('form'));
            // validation rules
            $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]|max_length[25]|alpha_dash|xss_clean');
            $this->form_validation->set_rules('passwordConfirm', 'confirm password', 'trim|required|matches[password]|alpha_dash|min_length[8]|max_length[25]|xss_clean');
            // Error deliminators
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            // Form validation action
            if ($this->form_validation->run() == TRUE) {
                $this->user_mdl->change_password($uid);
                $this->session->set_flashdata('success_message', 'Password has been changed successfully.');
                redirect(base_url() . "user/dashboard/login", "refresh");
            }
            $this->load->view('user/chgpassword', $data);
        }
    }

    //Client logout 
    public function logout() {
        $data = array('username' => 0, 'uid' => 0, 'user_login' => false);
        $this->session->sess_destroy();
        $this->session->unset_userdata($data);
        redirect(base_url() . "user/dashboard/login");
    }
	
	public function testEmail()
	{
		/* $clientMailConfig = array('to' => 'muthukumar@ancyinfotech.com', 'subject' => 'Congratulations! Test Email Sent', 'message' => 'Test Content here..');
		// send mail
		$this->mail_model->sendEmail($clientMailConfig); */		
	}
	
	public function userEmail()
	{
		$info = array('firstname' => 'Muthukumar','email' => 'muthukumar@ancyinfotech.com','vfcnCode' => 'kdfhkjdhfkdfd');
		$userName = $info['firstname'];
		$email = $info['email'];
		$vfcnPath = base_url().'user/dashboard/emailVerification/'.$info['vfcnCode'];
		$logoPath = base_url().'img/logo.png';
		$template = '<!DOCTYPE html>
						<html>
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<meta name="viewport" content="initial-scale=1.0">
						<style type="text/css">
						::selection
						{
						color:#fff;
						background-color:#49A4A2;}
						::-moz-selection
						{
						color:#fff; background-color:#49A4A2;}
						.top_bar {
							display:none;
							}
						@media only screen and (max-width: 480px) {
							table[class=table], td[class=cell] { width: 300px !important; }
							table[class=table], td[class=cell] { width: 300px !important; }
						}
						.link-btn {
							background-color: #797979;
							border-radius: 15px;
							box-shadow: none;
							color: #FFFFFF;
							float: right;
							font-family: Arial,Helvetica,sans-serif;
							font-size: 16px;
							font-weight: bold;
							height: auto;
							padding: 5px 40px;
							min-width: 50px;
						}
						</style>
							<title>Welcome to Farmers Creed</title>
						</head>
						<body style="margin:0; padding:0; background-color:#F4F4F4; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:14px;">	

							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
							<tr>
								<td bgcolor="#F4F4F4" align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td><div style="max-width:700px; margin:0 auto;">
										<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="center" bgcolor="#656565" style="padding-top:10px; padding-bottom:10px;"><img src="'.$logoPath.'" style="width:161px;"></td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="30">&nbsp;</td>
														<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td height="20">&nbsp;</td>
														</tr>
														<tr>
															<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear '.$userName.' </strong></td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left">Thank you for registering with Farmer`s Creed.<br><br>
															To verify your email address and continue, please click the following link:<br><br>
															</td>
														</tr>
														  <tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7" style="border:1px solid #F0F0F0; -webkit-box-shadow: 2px 2px 3px 1px #9E9E9E;
														box-shadow: 2px 2px 3px 1px #9E9E9E;">
														  <tr>
															<td style="font-size:13px; padding-top:7px; padding-right:10px; padding-bottom:5px; padding-left:10px;">
															
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
														  <tr>
															<td width="100%" style="word-wrap:break-word;  font-family:Arial, Helvetica, sans-serif; padding-right:10px;" class="link">
															<a href="'.$vfcnPath.'" target="_blank" style="font-size:13px; color:#2C6CB8;" >'.$vfcnPath.'</a></td>
															<td width="80"><a href="'.$vfcnPath.'" class="link-btn" > Go </a></td>
														  </tr>
														</table>

															</td>
														  </tr>
														</table>

															</td>
														  </tr>
						  <tr>
							<td style="font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left"><br>
						<br>

							Your Email ID: 
							<a href="#" style="color:#2C6CB8; font-size:13px; text-decoration:none;">'.$email.'</a>
						</td>
						  </tr>
						  <tr>
							<td style="font-size:15px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left"><br>
						<br>
						<strong>creedapps.com</strong><br>
						<br>

						</td>
						  </tr>

						</table></td>
							<td width="30">&nbsp;</td>
						  </tr>
						</table>


							
							
						</td>
						  </tr>
						  
						</table>
						</td>
										</tr>
										<tr>
										<td bgcolor="#656565" style="padding-top:22px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="19%">&nbsp;</td>
							<td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#459d75" style="-webkit-border-radius: 15px;
						border-radius: 15px;">
						  <tr>
							<td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
							Copyright © 2014 Creed Apps, All rights reserved.</td>
						  </tr>
						</table>
						</td>
							<td width="19%">&nbsp;</td>
						  </tr>
						</table>

									   
										</td>
										</tr>
										<tr>
										<td bgcolor="#656565" style="padding-top:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td style="color:#d2f736; font-size:11px; font-family:Arial, Helvetica, sans-serif;" align="center">You received this email because you registered at Creed Apps<br><br>

						<br>
						</td>
						  </tr>
						</table>


									   
										</td>
										</tr>
										
									  </table>
									</div></td>
								</tr>
							  </table>
							  </td>
						  </tr>
						</table>
						</body>
						</html>';
		echo $template;
	}
	
	public function adminEmail()
	{
		$info = array('firstname' => 'Muthukumar','email' => 'muthukumar@ancyinfotech.com','username' => 'testuser','farmname' => 'Paana','phone' => '9999999888','address' => 'testAddress');
		$firstname = $info['firstname'];
		$username = $info['username'];
		$farmname = $info['farmname'];
		$email = $info['email'];
		$phone = $info['phone'];
		$address = $info['address'];
		$logoPath = base_url().'img/logo.png';
		$template = '<!DOCTYPE html>
						<html>
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<meta name="viewport" content="initial-scale=1.0">
						<!-- So that mobile webkit will display zoomed in -->
						<!-- CSS for mobile devices. Attribute selector used so that Yahoo! mail will ignore styles -->
						<style type="text/css">
						::selection
						{
						color:#fff;
						background-color:#49A4A2;}
						::-moz-selection
						{
						color:#fff; background-color:#49A4A2;}
						.top_bar {
							display:none;
							}
						@media only screen and (max-width: 480px) {
							table[class=table], td[class=cell] { width: 300px !important; }
							table[class=table], td[class=cell] { width: 300px !important; }
						}
						.link-btn {
							background-color: #797979;
							border-radius: 15px;
							box-shadow: none;
							color: #FFFFFF;
							float: right;
							font-family: Arial,Helvetica,sans-serif;
							font-size: 16px;
							font-weight: bold;
							height: auto;
							padding: 5px 40px;
							width: auto;
						}
						</style>
							<title>Welcome to Uptimeweb</title>
						</head>
						<body style="margin:0; padding:0; background-color:#F4F4F4; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:14px;">	

							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
							<tr>
								<td bgcolor="#F4F4F4" align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td><div style="max-width:700px; margin:0 auto;">
										<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="center" bgcolor="#656565" style="padding-top:10px; padding-bottom:10px;"><img src="'.$logoPath.'" style="width:161px;"></td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="30">&nbsp;</td>
														<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td height="20">&nbsp;</td>
														</tr>
														<tr>
															<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear admin, </strong></td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left"><strong>User information mentioned below.</strong>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Name<span style="margin-left:8.1em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$firstname.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Email<span style="margin-left:8.2em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$email.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																User name<span style="margin-left:5.7em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$username.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Farm name<span style="margin-left:5.4em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$farmname.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Phone<span style="margin-left:7.6em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$phone.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Address<span style="margin-left:6.8em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$address.'</span>
															</td>
														</tr>
						</table></td>
							<td width="30">&nbsp;</td>
						  </tr>
						</table>


							
							
						</td>
						  </tr>
						  
						</table>
						</td>
										</tr>
										<tr>
										<td bgcolor="#656565" style="padding:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="19%">&nbsp;</td>
							<td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#459d75" style="-webkit-border-radius: 15px;
						border-radius: 15px;">
						  <tr>
							<td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
							Copyright © 2014 Creed Apps,All rights reserved.</td>
						  </tr>
						</table>
						</td>
							<td width="19%">&nbsp;</td>
						  </tr>
						</table>

									   
										</td>
										</tr>
										
									  </table>
									</div></td>
								</tr>
							  </table>
							  </td>
						  </tr>
						</table>
						</body>
						</html>';
						echo $template;
	}
	
	public function frgtPwdEmail()
	{
		$info = array('firstname' => 'Muthukumar','password' => 'shdsdsdjsj');
		$userName = $info['firstname'];
		$password = $info['password'];
		$logoPath = base_url().'img/logo.png';
		$loginPath = base_url().'user/dashboard/login';
		$template = '<!DOCTYPE html>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="initial-scale=1.0">
		<!-- So that mobile webkit will display zoomed in -->
		<!-- CSS for mobile devices. Attribute selector used so that Yahoo! mail will ignore styles -->
		<style type="text/css">
		::selection
		{
		color:#fff;
		background-color:#49A4A2;}
		::-moz-selection
		{
		color:#fff; background-color:#49A4A2;}
		.top_bar {
			display:none;
			}
		@media only screen and (max-width: 480px) {
			table[class=table], td[class=cell] { width: 300px !important; }
			table[class=table], td[class=cell] { width: 300px !important; }
		}
		.link-btn {
			background-color: #797979;
			border-radius: 15px;
			box-shadow: none;
			color: #FFFFFF;
			float: right;
			font-family: Arial,Helvetica,sans-serif;
			font-size: 16px;
			font-weight: bold;
			height: auto;
			padding: 5px 40px;
			width: auto;
		}
		</style>
			<title>Welcome to Uptimeweb</title>
		</head>
		<body style="margin:0; padding:0; background-color:#F4F4F4; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:14px;">	

			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
			<tr>
				<td bgcolor="#F4F4F4" align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td><div style="max-width:700px; margin:0 auto;">
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center" bgcolor="#656565" style="padding-top:10px; padding-bottom:10px;"><img src="'.$logoPath.'" style="width:161px;"></td>
								</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="30">&nbsp;</td>
										<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td height="20">&nbsp;</td>
										</tr>
										<tr>
											<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:3px;" align="left"><strong>Dear&nbsp'.$userName.', </strong></td>
										</tr>
										<tr>
											<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:3px;" align="left">
												<p style="line-height:20px;"><strong>Your password was reset successfully.</strong></p>
											</td>
										</tr>
										<tr>
											<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:18px;" align="left">
												Your new password for farmers creed is :&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.$password.'</strong>
											</td>
										</tr>
										<tr>
											<td style="font-size:14px;line-height:20px;font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:18px;" align="left">
												Once logged in, please change your password to something you can remember.Never share your password anybody.
											</td>
										</tr>
										<tr>
											<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
												In order to login with new password, please&nbsp;<a href="'.$loginPath.'">click here</a>
											</td>
										</tr>
		</table></td>					
			<td width="30">&nbsp;</td>
		  </tr>
		</table>


			
			
		</td>
		  </tr>
		  
		</table>
		</td>
						</tr>
						<tr>
						<td bgcolor="#656565" style="padding:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="19%">&nbsp;</td>
			<td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#459d75" style="-webkit-border-radius: 15px;
			border-radius: 15px;">
			  <tr>
				<td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
				Copyright © 2014 Creed Apps,All rights reserved.</td>
			  </tr>
			</table>
			</td>
			<td width="19%">&nbsp;</td>
		  </tr>
		</table>

					   
						</td>
						</tr>
						
					  </table>
					</div></td>
				</tr>
			  </table>
			  </td>
		  </tr>
		</table>
		</body>
		</html>';
		echo $template;
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
