<?php
Class Catfishsales_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function catfishsales_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishsales_add = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'pond_name' => $this->input->post('pond_name'),
			'no_of_fish' => $this->input->post('no_of_fish'),
			'total_weight' => $this->input->post('total_weight'),
			'total_cost' => $this->input->post('total_cost'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),	
		);
		
		$this->db->insert('tbl_catfishsales', $catfishsales_add);
		$incomeid = $this->db->insert_id();
		$catfishincome_add = array(
			'uid' => $uid,
			'csid' => $incomeid,
			'pond_name' => $this->input->post('pond_name'),
			'no_of_fish' => $this->input->post('no_of_fish'),
			'total_cost' => $this->input->post('total_cost'),
			'income_type' => 'sales',			
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date'))))
		);
		
		$this->db->insert('tbl_catfishincome', $catfishincome_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function catfishsales_edit($csid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishsales_edit = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'pond_name' => $this->input->post('pond_name'),
			'no_of_fish' => $this->input->post('no_of_fish'),
			'total_weight' => $this->input->post('total_weight'),
			'total_cost' => $this->input->post('total_cost'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
		);
		$this->db->where('csid', $csid);
		$this->db->update('tbl_catfishsales',$catfishsales_edit);
		
		$catfishincome_edit = array(
			'uid' => $uid,
			'pond_name' => $this->input->post('pond_name'),
			'no_of_fish' => $this->input->post('no_of_fish'),
			'total_cost' => $this->input->post('total_cost'),
			'income_type' => 'sales',			
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
		);

		$this->db->where('csid', $csid);
		$this->db->update('tbl_catfishincome',$catfishincome_edit);

	}
	
	public function getcatfishsales($csid)
	{	
		$this->db->select('*')->from('tbl_catfishsales')->where('csid', $csid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}

	public function getcatfishsalespond()
	{	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('uid' => $this->session->userdata('parent_uid')));
		}
		$this->db->select('scid,pond_name')->from('tbl_structure_catfish');
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}	
	
	public function catfishsales_delete($csid){
		
		$where = array('csid' => $csid);
		$this->db->where($where);
		$this->db->delete('tbl_catfishsales');

		$where = array('csid' => $csid);
		$this->db->where($where);
		$this->db->delete('tbl_catfishincome');
		
	}
	public function getStaffname($staff_id){
	
		$this->db->select('id,firstname')->from('tbl_users')->where('id', $staff_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;		
	}
}
