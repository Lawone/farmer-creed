<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Taskcatfish extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('taskcatfish_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function waterchange_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->waterchange_add();
			$this->session->set_flashdata('message', 'waterchange has been added successfully.');
			redirect(base_url().'user/taskcatfish/waterchange_add', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getWaterchangepond();
		$this->load->view('user/waterchange_add', $data);
	}
	
	//user edit for notification
	public function waterchange_edit($wcid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->waterchange_edit($wcid);
			$this->session->set_flashdata('message', 'waterchange has been changed successfully.');
			redirect(base_url().'user/taskcatfish/waterchange_view', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getWaterchangepond();
		$data['waterchange_e']=$this->taskcatfish_model->getWaterchangecatfish($wcid);
		$this->load->view('user/waterchange_edit',$data);
	}
	
	// domain view method
	public function waterchange_view() {
		$this->db->select('wc.wcid,wc.created_date,wc.created_date,wc.user_type,sc.pond_name,wc.staff_id')->from('tbl_waterchange wc');
		$this->db->join('tbl_structure_catfish sc','wc.scid = sc.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('wc.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('wc.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['waterchange_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/waterchange_view', $data);
	}
	
	//user delete method
	public function waterchange_delete($wcid){
		
		$this->taskcatfish_model->waterchange_delete($wcid);
		$this->session->set_flashdata('message', 'Waterchange has been deleted successfully.');
		redirect(base_url().'user/taskcatfish/waterchange_view', 'refresh');
	}
	
	/*============Catfish Feeding===================*/
	
	public function catfishfeed_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedsize', 'Feed Size', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedweight', 'Feed Weight', 'trim|required|callback_checkFeedWeight|xss_clean');
		$this->form_validation->set_rules('feedbrand', 'Feed Brand', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('time', 'Time', 'trim|required|xss_clean');

		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->catfishfeed_add();
			$this->session->set_flashdata('message', 'Catfish Feeding has been added successfully.');
			redirect(base_url().'user/taskcatfish/catfishfeed_add', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getcatfishfeedpond();
		$data['getcatfishfeedbrand'] = $this->taskcatfish_model->getCatfishfeedbrand();
		$this->load->view('user/catfishfeed_add', $data);
	}
	public function checkFeedWeight()
	{
		if($this->input->post('avlfeedweight') >= $this->input->post('feedweight')){
			return true;
		}else{
			$this->form_validation->set_message('checkFeedWeight', 'Feedweight is greater than Available weight.');
			return false;
		}
	}
	//user edit for notification
	public function catfishfeed_edit($cffid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedsize', 'Feed Size', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedweight', 'Feed Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('feedbrand', 'Feed Brand', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('time', 'Time', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->catfishfeed_edit($cffid);
			$this->session->set_flashdata('message', 'Catfish Feeding has been changed successfully.');
			redirect(base_url().'user/taskcatfish/catfishfeed_view', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getCatfishfeedpond();
		$data['getcatfishfeedbrand'] = $this->taskcatfish_model->getCatfishfeedbrand();
		$data['catfishfeed_e']=$this->taskcatfish_model->getCatfishfeeding($cffid);
		$this->load->view('user/catfishfeed_edit',$data);
	}
	
	// domain view method
	public function catfishfeed_view() {
		$this->db->select('cff.cffid,cff.user_type,cff.created_date,cff.feedsize,cff.feedbrand,cff.feedweight,cff.staff_id,sc.pond_name,fp.brand,fp.size')->from('tbl_catfishfeed cff');
		$this->db->join('tbl_structure_catfish sc','cff.scid = sc.scid');
		$this->db->join('tbl_feedpurchased fp','fp.fpid = cff.fpid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('cff.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('cff.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get(); 
		$data['catfishfeed_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;
		$this->load->view('user/catfishfeed_view', $data);
	}
	
	//user delete method
	public function catfishfeed_delete($cffid){
		
		$this->taskcatfish_model->catfishfeed_delete($cffid);
		$this->session->set_flashdata('message', 'Catfish Feeding has been deleted successfully.');
		redirect(base_url().'user/taskcatfish/catfishfeed_view', 'refresh');
	}
	
	/*========= Catfish Sorting ============*/
	public function sorting_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('from_scid', 'From', 'trim|required|callback_alreadySelectPond|xss_clean');
		$this->form_validation->set_rules('to_scid', 'To', 'trim|required|callback_alreadySelectPond|xss_clean');
		$this->form_validation->set_rules('no_of_fish', 'No.of Fish', 'trim|required|callback_checkNoFish|xss_clean');
		//$this->form_validation->set_rules('total_weight', 'Total Weight', 'trim|required|callback_checkWeight|xss_clean');
		$this->form_validation->set_rules('total_weight', 'Total Weight', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->sorting_add();
			$this->session->set_flashdata('message', 'Catfish Sorting has been added successfully.');
			redirect(base_url().'user/taskcatfish/sorting_add', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getSortingpond();
		$this->load->view('user/sorting_add',$data);
	}
	public function alreadySelectPond()
	{
		if($this->input->post('from_scid') == $this->input->post('to_scid')){
			$this->form_validation->set_message('alreadySelectPond', 'Please Select Different Pond Name.');
			return false;
		}
		else{
			return true;
		}
	}
	public function checkNoFish()
	{
		$this->db->select('total_stocked')->from('tbl_livestock_catfish')->where(array('scid' => $this->input->post('from_scid')));
		$noofFish = $this->db->get();
		$CheckFish = $noofFish->row_array();
		
		if($CheckFish['total_stocked'] > $this->input->post('no_of_fish')){
			return true;
		}
		else{
			$this->form_validation->set_message('checkNoFish', 'No of fish is greater than total no of stocked.');
			return false;
		}
	}
	public function checkWeight()
	{
		$this->db->select('avg_weight')->from('tbl_livestock_catfish')->where(array('scid' => $this->input->post('from_scid')));
		$avgWeight = $this->db->get();
		$CheckWeight = $avgWeight->row_array();
		
		if($CheckWeight['avg_weight'] > $this->input->post('total_weight')){
			return true;
		}
		else{
			$this->form_validation->set_message('checkWeight', 'Total Weight is greater than Avg Weight.');
			return false;
		}
	}
	//user edit for notification
	public function sorting_edit($sid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('from_scid', 'From', 'trim|required|callback_alreadySelectPond|xss_clean');
		$this->form_validation->set_rules('to_scid', 'To', 'trim|required|callback_alreadySelectPond|xss_clean');
		$this->form_validation->set_rules('no_of_fish', 'No.of Fish', 'trim|required|callback_checkNoFish|xss_clean');
		//$this->form_validation->set_rules('total_weight', 'Total Weight', 'trim|required|callback_checkWeight|xss_clean');
		$this->form_validation->set_rules('total_weight', 'Total Weight', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->sorting_edit($sid);
			$this->output->enable_profiler(TRUE);
			$this->session->set_flashdata('message', 'Catfish Feeding has been changed successfully.');
			redirect(base_url().'user/taskcatfish/sorting_view', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getSortingpond();
		$data['sorting_e']=$this->taskcatfish_model->getSortingtask($sid);
		$this->load->view('user/sorting_edit',$data);
	}
	
	// domain view method
	public function sorting_view() {
		
		$this->db->select('srt.sid,srt.no_of_fish,srt.total_weight,srt.created_by,srt.created_date,srt.user_type,sc.pond_name as fromPond, sct.pond_name as toPond,srt.staff_id')->from('tbl_sorting srt');
		$this->db->join('tbl_structure_catfish sc','srt.from_scid = sc.scid');
		$this->db->join('tbl_structure_catfish sct','srt.to_scid = sct.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('srt.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('srt.staff_id' => $this->session->userdata('uid')));
		}
		
		// data subset
		$query = $this->db->get();
		$data['sorting_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/sorting_view', $data);
	}
	
	//user delete method
	public function sorting_delete($sid){
		
		$this->taskcatfish_model->sorting_delete($sid);
		$this->session->set_flashdata('message', 'Catdish Sorting has been deleted successfully.');
		redirect(base_url().'user/taskcatfish/sorting_view', 'refresh');
	}
	
	/*============Catfish Weight===================*/
	
	public function catfishweight_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');
					
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->catfishweight_add();
			$this->session->set_flashdata('message', 'Catfish Weight has been added successfully.');
			redirect(base_url().'user/taskcatfish/catfishweight_add', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getCatfishweightpond();
		$this->load->view('user/catfishweight_add', $data);
	}
	
	//user edit for notification
	public function catfishweight_edit($cwid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->catfishweight_edit($cwid);
			$this->session->set_flashdata('message', 'Catfish Weight has been changed successfully.');
			redirect(base_url().'user/taskcatfish/catfishweight_view', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getCatfishweightpond();
		$data['catfishweight_e']=$this->taskcatfish_model->getCatfishweight($cwid);
		$this->load->view('user/catfishweight_edit',$data);
	}
	
	// domain view method
	public function catfishweight_view() {
		$this->db->select('cw.cwid,cw.created_date,cw.user_type,cw.avg_weight,sc.pond_name,cw.staff_id')->from('tbl_catfishweight cw');
		$this->db->join('tbl_structure_catfish sc','cw.scid = sc.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('cw.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('cw.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['catfishweight_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;
		$this->load->view('user/catfishweight_view', $data);
	}
	//user delete method
	public function catfishweight_delete($cwid){
	
		$this->taskcatfish_model->catfishweight_delete($cwid);
		$this->session->set_flashdata('message', 'Catfish Weight has been deleted successfullY.');
		redirect(base_url().'user/taskcatfish/catfishweight_view', 'refresh');
		}

		/*============Catfish Mortality===================*/
	
	public function catfishmortality_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('mortality', 'Mortality', 'trim|required|callback_checkMortality|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->catfishmortality_add();
			$this->session->set_flashdata('message', 'Catfish Mortality has been added successfully.');
			redirect(base_url().'user/taskcatfish/catfishmortality_add', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getSortingpond();
		$this->load->view('user/catfishmortality_add', $data);
	}
	public function checkMortality()
	{
		$this->db->select('total_stocked')->from('tbl_livestock_catfish')->where(array('scid' => $this->input->post('scid')));
		$noofMortality = $this->db->get();
		$Mortality = $noofMortality->row_array();
		
		if($Mortality['total_stocked'] > $this->input->post('mortality')){
			return true;
		}
		else{
			$this->form_validation->set_message('checkMortality', 'Mortality is greater than total no of stocked.');
			return false;
		}
	}
	//user edit for notification
	public function catfishmortality_edit($cfmid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('scid', 'Pond Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('mortality', 'Mortality', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->taskcatfish_model->catfishmortality_edit($cfmid);
			$this->session->set_flashdata('message', 'Catfish Mortality has been changed successfully.');
			redirect(base_url().'user/taskcatfish/catfishmortality_view', 'refresh');
		}
		$data['getpondname'] = $this->taskcatfish_model->getSortingpond();
		$data['catfishmortality_e']=$this->taskcatfish_model->getCatfishmortality($cfmid);
		$this->load->view('user/catfishmortality_edit',$data);
	}
	
	// domain view method
	public function catfishmortality_view() {
		$this->db->select('cm.cmid,cm.created_date,cm.user_type,cm.mortality,sc.pond_name,cm.staff_id')->from('tbl_catfish_mortality cm');
		$this->db->join('tbl_structure_catfish sc','cm.scid = sc.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('cm.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('cm.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['catfishmortality_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;
		$this->load->view('user/catfishmortality_view', $data);
	}
	//user delete method
	public function catfishmortality_delete($cfmid){
	
		$this->taskcatfish_model->catfishmortality_delete($cfmid);
		$this->session->set_flashdata('message', 'Catfish Mortality has been deleted successfullY.');
		redirect(base_url().'user/taskcatfish/catfishmortality_view', 'refresh');
		}
}
