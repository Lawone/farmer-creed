<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<?php 
error_reporting(0);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fcr_broiler extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('taskbroiler_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	// domain view method
	public function fcr_broiler_view() {
            
	    if(isset($_REQUEST['search'])){
			$f_arr = array();
                 $date_arr = array();
                 $sql_query = "SELECT SUM( `avg_weight` ) as  avg_weight,date(created_date) as created_date FROM `tbl_broilerweight` WHERE";
                 $sql_query1 = "SELECT SUM( `feedweight` ) as feedweight,date(created_date) as created_date FROM `tbl_broilerfeed` WHERE";
               	 $sql_query2 = "SELECT sum(total_stocked) as total_stock FROM `tbl_livestock_broiler` WHERE";
				 
                 if((isset($_POST['pen_name'])) ){
                    $sql_query .=  "  `sbid` = '".$_POST['pen_name']."' ";
                    $sql_query1 .=  "  `sbid` = '".$_POST['pen_name']."' ";
                    $sql_query2 .=  "  `sbid` = '".$_POST['pen_name']."' ";
                    
                 }
			  
                 if((!empty($_POST['pen_name'])) &&  (!empty($_POST['start-date']))  &&  (!empty($_POST['end-date']))) {
          
                    $sql_query .=  "  AND (`created_date` BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
                    $sql_query1 .=  " AND  (`created_date` BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
                   
                 }
                 
                    $sql_query  .= " GROUP BY date( `created_date` )";
					$sql_query  .= "order by date(created_date) desc";
                    //echo "<br />";
                    $query = $this->db->query($sql_query);
                    $result = $query->result_array();
	//print_r( $result);exit;				
                    foreach ($result as $key => $value) {
                        
                        $now_count = count($f_arr);
                        $date_arr[$now_count] = $value['created_date'];
                        $f_arr[$now_count]['date'] = $value['created_date'];
                        $f_arr[$now_count]['avg_weight'] = $value['avg_weight'];
                    }
                    
                    $sql_query1 .= " GROUP BY date( `created_date` )";
					
					$query2 = $this->db->query($sql_query2);
                    $result2 = $query2->row_array();

                    $query1 = $this->db->query($sql_query1);
                    $result1 = $query1->result_array();
                //  print_r( $result1);exit;	
                    foreach ($result1 as $key => $value1) {
                        foreach ($result as $key => $value) {
                         if(in_array($value['created_date'], $date_arr)){
                        $now_count = array_search ($value['created_date'], $date_arr);
                        $date_arr[$now_count] = $value1['created_date'];
                        $f_arr[$now_count]['date'] = $value1['created_date'];
						$f_arr[$now_count]['total_no'] = $result2['total_stock'];
                        $f_arr[$now_count]['feedweight'] = $value1['feedweight'];
                        $f_arr[$now_count]['fcr'] = number_format(($value1['feedweight']) / ($value['avg_weight']),2,'.','');
                    }
                    }
                    }
                    
                 
                    $data['fcrbroiler_v'] = $f_arr;
               
		}
		
        $data['getpenname']=$this->taskbroiler_model->getlitterchangepond();
		$this->load->view('user/fcr_broiler_view', $data);
	}
	
	
}
