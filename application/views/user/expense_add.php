<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
.select2_container
{
	width: 245px !important;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-12" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#000;background:#efefef;">
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Add Expense</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >

							
							<fieldset>
							<div class="row">
							<div class="col6">
									<section class="col col-6 col-a">
									<label class="label">From</label>
										<label class="select">
											<select name="expense_from">
												<option value="" selected="" disabled="">From</option>
												<?php
													$count = 5;
													$curYear = date('Y');
													for($i = 0; $i < 5; $i++){
														$year = date('Y',strtotime('-'.$count.' year', strtotime($curYear)));
														echo '<option value="'.$year.'">'.$year.'</option>';
														$count--;
													}
													echo '<option value="'.$curYear.'">'.$curYear.'</option>';
												?>
											</select> <i></i> </label>
											<?php echo form_error('expense_from'); ?>
									</section>

									<section class="col col-6 col-a">
									<label class="label">To</label>
										<label class="select">
											<select name="expense_to">
												<option value="" selected="" disabled="">To</option>
												<?php
													$count = 4;
													$curYear = date('Y');
													for($i = 0; $i < 4; $i++){
														$year = date('Y',strtotime('-'.$count.' year', strtotime($curYear)));
														echo '<option value="'.$year.'">'.$year.'</option>';
														$count--;
													}
													echo '<option value="'.$curYear.'">'.$curYear.'</option>';
												?>
											</select> <i></i> </label>
											<?php echo form_error('expense_to'); ?>
									</section>
									</div>
								</div>
								<div class="row edit_button" style="float:right;margin-bottom: 10px;margin-top:-14px;margin-right: 0;">
									<button type="button" class="btn btn-warning btn-xs" id="addExpenseRow" ><i class="fa fa-plus-square"></i> Add row</button>
								</div>
								<div class="expense-table">
								<table class="table table-bordered table-striped table-hover" id="expenseTable">
											<thead>
												<tr>
													<th>S.No</th>
													<th>Crop</th>
													<th>Total land(Acres)</th>
													<th>Labour hired(N)</th>
													<th>Fertilizers(N)</th>
													<th>Chemicals(N)</th>
													<th>Rent – Machinery(N)</th>
													<th>Rent – Land(N)</th>
													<th>Repairs and Main.(N)</th>
													<th>Seeds and Plants(N)</th>
													<th>Other Exp.(N)</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td style="text-align:center; vertical-align: middle">1.</td>
													  <td>
															<select class="select2 select2_container" name="cid[]" placeholder="Please select products or crops">
															<!--<option></option>-->
															<?php
																 if ($crop != false) {
																	 foreach ($crop as $crops) {
																		echo '<option value="'.$crops['cid'].'">'.$crops['crop_name'].'</option>';
																	}
																}
																?>
															</select>
															<?php echo form_error('cid[]'); ?>
														</td>
													  <td><label class="input"><input id="" class="textAlignRight" style="width:75px;" type="text" name="acres[]" placeholder="0.00" ></label><?php echo form_error('acres[]'); ?></td>
													  <td><label class="input">
											<input type="text" name="labour_hired[]" id="" placeholder="0.00" class="textAlignRight" style="width:75px;">
										</label><?php echo form_error('labour_hired[]'); ?></td>
													  <td><label class="input">
											<input type="text" name="fertilizers_lime[]" id="" placeholder="0.00" class="textAlignRight" style="width:75px;">
										</label><?php echo form_error('sell_price[]'); ?></td>
													  <td><label class="input">
											<input type="text" name="chemicals[]" id="" placeholder="0.00" class="textAlignRight" style="width:75px;">
										</label><?php echo form_error('chemicals[]'); ?></td>
													  <td><label class="input">
											<input type="text" name="rent_machinery_equip_vehi[]" id="" class="textAlignRight" style="width:75px;" placeholder="0.00">
										</label><?php echo form_error('rent_machinery_equip_vehi[]'); ?></td>
													  <td><label class="input">
											<input type="text" name="rent_land[]" id="" class="textAlignRight" style="width:75px;" placeholder="0.00">
										</label><?php echo form_error('rent_land[]'); ?></td>
										<td><label class="input">
											<input type="text" name="repairs_maintain[]" id="" class="textAlignRight" style="width:75px;" placeholder="0.00">
										</label><?php echo form_error('repairs_maintain[]'); ?></td>
										<td><label class="input">
											<input type="text" name="seeds_plants[]" id="" class="textAlignRight" style="width:75px;" placeholder="0.00">
										</label><?php echo form_error('seeds_plants[]'); ?></td>
										<td><label class="input">
											<input type="text" name="other_expense[]" id="" class="textAlignRight" style="width:75px;" placeholder="0.00">
										</label><?php echo form_error('other_expense[]'); ?></td>
										
													  <td style="text-align:center; vertical-align: middle"><a href="#" class="DeleteRow"><i class="fa fa-times" ></i></a></td>
												</tr>
											</tbody>
										</table>
								</div>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary">
									Save
								</button>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" onclick="window.history.back();" class="btn btn-default" type="button">
													Back
												</button>
							</footer>
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>
			</div>

		</div>
		<script type="text/javascript">
		$(document).ready(function()
		{
			$('#addExpenseRow').click(function(){
			var 
			table = $('#expenseTable'),
			sno = table.find('tbody > tr').length + 1,
			newRow = table.find('tbody > tr:first').clone(),
			selectBox = table.find('tbody > tr:first select[name="cid[]"]').clone(),
			acres = table.find('tbody > tr:first input[name="acres[]"]').clone(),
			labour_hired = table.find('tbody > tr:first input[name="labour_hired[]"]').clone(),
			fertilizers_lime = table.find('tbody > tr:first input[name="fertilizers_lime[]"]').clone(),
			chemicals = table.find('tbody > tr:first input[name="chemicals[]"]').clone(),
			rent_machinery_equip = table.find('tbody > tr:first input[name="rent_machinery_equip[]"]').clone(),
			rent_land = table.find('tbody > tr:first input[name="rent_land[]"]').clone(),
			repairs_maintain = table.find('tbody > tr:first input[name="repairs_maintain[]"]').clone(),
			seeds_plants = table.find('tbody > tr:first input[name="seeds_plants[]"]').clone(),
			other_expense = table.find('tbody > tr:first input[name="other_expense[]"]').clone(),
			//selectBox = newRow.find('select[name="item_id[]"]'),
			secondTd = newRow.find('td:nth-child(2)');
			newRow.find('td:first').text(sno + '.');
			newRow.css('display', 'none').appendTo( table.find('tbody') ).fadeIn().find(':input').val('');
			secondTd.html('');
			selectBox.attr('id', 'item_id' + sno).appendTo(secondTd).select2({
				placeholder: $(this).attr('placeholder'),
				allowClear: true
			});
			
		});

		$(document).on('click', 'a.DeleteRow', function(e){
			e.preventDefault();
			var
			tbody = $(this).parents('tbody'),
			tr = $(this).parents('tr'),
			sno = 1;
			if( tbody.find('tr').length > 1 ) {
				tr.fadeOut(function(){
					$(this).remove();
					tbody.find('tr').each(function(){
						$(this).find('td:first').text(sno+'.');
						sno++;	
					});
					$('#itemsBody [name="item_amount[]"]:first').trigger('subTotalCalc');
				});	
			}
		});
		});
		runAllForms();

			$(function() {
			
			
				 // Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						expense_from : {
							required : true
						},
						expense_to : {
							required : true
						},
						'cid[]' : {
							required : true
						},
						'acres[]' : {
							required : true,
							number : true
						},
						'labour_hired[]' : {
							required : true,
							number : true
						},
						'fertilizers_lime[]' : {
							required : true,
							number : true
						},
						'chemicals[]' : {
							required : true,
							number : true
						},
						'rent_machinery_equip_vehi[]' : {
							required : true,
							number : true
						},
						'rent_land[]' : {
							required : true,
							number : true
						},
						'repairs_maintain[]' : {
							required : true,
							number : true
						},
						'seeds_plants[]' : {
							required : true,
							number : true
						},
						'other_expense[]' : {
							required : true,
							number : true
						}
					},

					// Messages for form validation
					messages : {
						expense_from : {
							required : 'Please select Expense from'
						},
						expense_to : {
							required : 'Please Select Expense to'
						},
						'cid[]' : {
							required : 'Please select crop name'
						},
						'acres[]' : {
							required : 'Please Enter your Acres'
						},
						'labour_hired[]' : {
							required : 'Please Enter your Labour hired'
						},
						'fertilizers_lime[]' : {
							required : 'Please Enter your Fertilized and lime'
						},
						'chemicals[]' : {
							required : 'Please Enter your Chemicals'
						},
						'rent_machinery_equip_vehi[]' : {
							required : 'Please Enter your Rent – Machinery'
						},
						'rent_land[]' : {
							required : 'Please Enter your Rent – Land'
						},
						'repairs_maintain[]' : {
							required : 'Please Enter your Repairs and maintenance'
						},
						'seeds_plants[]' : {
							required : 'Please Enter your Seeds and Plants'
						},
						'other_expense[]' : {
							required : 'Please Enter your Other Expenses'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				}); 
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
		