<?php

error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class feedstock_inventory_broiler extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->app_access->user(); // check access permission for user
        $this->load->model(array('catfishsales_model'));
    }

    // Registration
    public function index() {
        
    }

    //user add for notification
    //user edit for notification
    // domain view method
    public function feedinventorybroiler_view() {

        if (isset($_REQUEST['search'])) {

            if ((!empty($_POST['start-date'])) && (!empty($_POST['end-date']))) {

                $sql_query = "SELECT date_format(created_date,'%Y-%m-%d') as created_date, type,brand, sum(weight) as feedweight, sum(avl_weight) as avl_weight,no_of_bags FROM  `tbl_broilerfeedpurchased`  WHERE 
                       date_format(created_date,'%Y-%m-%d') BETWEEN '" . $_POST['start-date'] . "' AND '" . $_POST['end-date'] . "' and uid = '".$this->session->userdata('uid')."' group by  date_format(created_date,'%Y-%m-%d'), type, brand ";
                $query = $this->db->query($sql_query);
                $result = $query->result_array();
            }

            $data['broilerfeed_v'] = $result;
        }
        $data['broilerfeed_v'] = $result;
      
        $this->load->view('user/feedstock_inventory_broiler', $data);         
    }
  
}
