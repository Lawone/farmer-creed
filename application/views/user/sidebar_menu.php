<header>
    <style>
        nav ul li a:hover {
            color: #fff;
            text-decoration: none;
        }
        nav ul li a {
            color: #a8a8a8;
            display: block;
            font-size: 14px;
            font-weight: 400;
            line-height: normal;
            padding: 10px 10px 10px 11px;
            position: relative;
            text-decoration: none !important;
        }
    </style>

    <aside id="left-panel" style="min-height: 2207px; margin-top:2.42em;">

        <!-- User info -->

        <!-- end user info -->

        <!-- NAVIGATION : This navigation is also responsive

        To make this navigation dynamic please make sure to link the node
        (the reference to the nav > ul) after page load. Or the navigation
        will not initialize.
        -->
        <nav>
                <!-- NOTE: Notice the gaps after each icon usage <i></i>..
                Please note that these links work a bit different than
                traditional hre="" links. See documentation for details.
            -->

            <ul>
                <li>
                    <a href="<?php echo base_url() . 'user/dashboard/home' ?>" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">DASHBOARD</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-tasks"></i> <span class="menu-item-parent">TASK</span></a>
                    <ul>
                        <?php
                        $arrLivestock = explode(",", $this->session->userdata('livestock'));

                        for ($i = 0; $i < count($arrLivestock); $i++) {
                            if ($arrLivestock[$i] == 1) {
                                ?>
                                <li><a href="#">Catfish</a>
                                    <ul>
                                        <li><a href="<?php echo base_url() . 'user/taskcatfish/waterchange_view'; ?>">Water Change</a></li>
                                        <li><a href="<?php echo base_url() . 'user/taskcatfish/catfishfeed_view' ?>">Feeding</a></li>
                                        <li><a href="<?php echo base_url() . 'user/taskcatfish/sorting_view' ?>">Sorting</a></li>
                                        <li><a href="<?php echo base_url() . 'user/taskcatfish/catfishweight_view' ?>">Weight Sampling</a></li>
                                        <li><a href="<?php echo base_url() . 'user/taskcatfish/catfishmortality_view' ?>">Mortality</a></li></ul></li><?php
                            } else if ($arrLivestock[$i] == 2) {
                                ?>
                                <li><a href="#">Broiler</a>
                                    <ul>
                                        <li><a href="<?php echo base_url() . 'user/taskbroiler/litterchange_view' ?>">Litter Change</a></li>
                                        <li><a href="<?php echo base_url() . 'user/taskbroiler/broilerfeed_view' ?>">Feeding</a></li>
                                        <li><a href="<?php echo base_url() . 'user/taskbroiler/broilerweight_view' ?>">Weight Sampling</a></li>
                                        <li><a href="<?php echo base_url() . 'user/taskbroiler/broilermortality_view' ?>">Mortality</a></li>
                                        <li><a href="<?php echo base_url() . 'user/taskbroiler/broilertreatment_view' ?>">Treatment</a></li></ul></li><?php
                            }
                        }
                        ?></ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-dollar"></i> <span class="menu-item-parent">ACCOUNTING</span></a>						
                    <ul>
                        <li><a href="#">Purchases</a>
                            <ul>
                                <li><a href="#">Feed Purchase</a><ul>
                                        <?php
                                        $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                        for ($i = 0; $i < count($arrLivestock); $i++) {
                                            if ($arrLivestock[$i] == 1) {
                                                ?>
                                                <li><a href="<?php echo base_url() . 'user/feedpurchased/feedpurchased_view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                                ?>
                                                <li><a href="<?php echo base_url() . 'user/broilerfeedpurchased/broilerfeedpurchased_view' ?>">Broiler</a></li><?php } ?>
                                        <?php } ?></ul></li>
                                <li><a href="#">General Purchase</a><ul>
                                        <?php
                                        $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                        for ($i = 0; $i < count($arrLivestock); $i++) {
                                            if ($arrLivestock[$i] == 1) {
                                                ?>
                                                <li><a href="<?php echo base_url() . 'user/catfishgenpurchased/catfishgenpurchased_view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                                ?>
                                                <li><a href="<?php echo base_url() . 'user/broilergenpurchased/broilergenpurchased_view' ?>">Broiler</a></li><?php } ?>
                                        <?php } ?></ul></li>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/fingerlingpurchased/fingerlingpurchased_view' ?>">Fingerling Purchase</a></li><?php
                                    } else if($arrLivestock[$i] == 2){ ?>
										<li><a href="<?php echo base_url() . 'user/chickpurchased/chickpurchased_view' ?>">Chick Purchase</a></li>
                                <?php } }
                                ?></ul>
                        <li><a href="#">Sales</a><ul>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/catfishsales/catfishsales_view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/broilersales/broilersales_view' ?>">Broiler</a></li><?php } ?>
<?php } ?></ul></li>
                                <?php if (($this->session->userdata('user_type') == 0)) { ?>
                            <li><a href="<?php echo base_url() . 'user/salaries/salaries_view' ?>">Salaries</a></li><?php } ?>
                        <li><a href="#">Feed Conversion Ratio</a><ul>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/fcr_catfish/fcr_catfish_view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/fcr_broiler/fcr_broiler_view' ?>">Broiler</a></li><?php } ?>
                <?php } ?></ul></li></ul>			
                </li>
<?php if (($this->session->userdata('user_type') == 0)) { ?>
                    <li>
                        <a href="#"><i class="fa fa-lg fa-fw fa-cog"></i> <span class="menu-item-parent">SETTINGS</span></a>						
                        <ul>
                            <li><a href="#">Structure</a>
                                <ul>
                                    <?php
                                    $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                    for ($i = 0; $i < count($arrLivestock); $i++) {
                                        if ($arrLivestock[$i] == 1) {
                                            ?>

                                            <li><a href="<?php echo base_url() . 'user/structcatfish/view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                            ?>
                                            <li><a href="<?php echo base_url() . 'user/structbroiler/view' ?>">Broiler</a></li><?php } ?>
                                    <?php } ?></ul></li>
                            <li><a href="#">Newly Stocked Livestock</a><ul>
                                    <?php
                                    $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                    for ($i = 0; $i < count($arrLivestock); $i++) {
                                        if ($arrLivestock[$i] == 1) {
                                            ?>
                                            <li><a href="<?php echo base_url() . 'user/structlivestockcatfish/view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                            ?>
                                            <li><a href="<?php echo base_url() . 'user/structlivestockbroiler/view' ?>">Broiler</a></li><?php } ?>
    <?php } ?></ul></li>
                            <li><a href="<?php echo base_url() . 'user/staff/view' ?>">Staff Login</a></li>
                        </ul>				
                    </li>
<?php } ?>
                <?php if (($this->session->userdata('user_type') == 0)) { ?>
				<li>
                    <a href="#"><i class="fa fa-lg fa-fw fa-book"></i> <span class="menu-item-parent">REPORTS</span></a>						
                    <ul>
                        <li><a href="#">Income Reports</a>
                            <ul>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>	
                                        <li><a href="<?php echo base_url() . 'user/income_catfish/incomecatfish_view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/income_broiler/incomebroiler_view' ?>">Broiler</a></li><?php } ?>
                                <?php } ?></ul></li>
 <li><a href="#">Expense Reports</a>
                            <ul>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>	
                                        <li><a href="<?php echo base_url() . 'user/expense_catfish/expensecatfish_view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/expense_broiler/expensebroiler_view' ?>">Broiler</a></li><?php } ?>
                                <?php } ?></ul></li>
                        <li><a href="#">Livestock Inventory</a>
                            <ul>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>	
                                        <li><a href="<?php echo base_url() . 'user/livestock_inventory_catfish/catfishincome_view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                        ?>
                                      <li><a href="<?php echo base_url() . 'user/livestock_inventory_broiler/broiler_view' ?>">Broiler</a></li><?php } ?>
                                <?php } ?></ul></li>
                        <li><a href="#">Feed Inventory</a>
                            <ul>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>	
                                        <li><a href="<?php echo base_url() . 'user/feedstock_inventory_catfish/feedinventory_view' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/feedstock_inventory_broiler/feedinventorybroiler_view' ?>">Broiler</a></li><?php } ?>
                                <?php } ?></ul></li>
                        <!--<li><a href="#">Income and Expenses</a>
                            <ul>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>	
                                        <li><a href="<?php echo base_url() . 'user/reports/income_expenses_report' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/reports/income_expenses_report_broiler_barchart' ?>">Broiler</a></li><?php } ?>
                                <?php } ?></ul></li>
                        <li><a href="#">Business Expenses</a>
                            <ul>
                                <?php
                                $arrLivestock = explode(",", $this->session->userdata('livestock'));

                                for ($i = 0; $i < count($arrLivestock); $i++) {
                                    if ($arrLivestock[$i] == 1) {
                                        ?>	
                                        <li><a href="<?php echo base_url() . 'user/reports/business_expenses_reports' ?>">Catfish</a></li><?php } else if ($arrLivestock[$i] == 2) {
                                        ?>
                                        <li><a href="<?php echo base_url() . 'user/reports/business_expenses_broiler_reports' ?>">Broiler</a></li><?php } ?>
<?php } ?></ul></li>-->
                </li><?php } ?></ul>
			<?php if (($this->session->userdata('user_type') == 0)) { ?>
            <li><a href="<?php echo base_url() . 'user/profile/edit/'.$this->session->userdata('uid') ?>"><i class="fa fa-lg fa-fw fa-user"></i>PROFILE</a><?php } ?>
            <li><a href="<?php echo base_url() . 'user/dashboard/logout' ?>"><i class="fa fa-lg fa-fw fa-power-off"></i>LOGOUT</a>
            </li>
			</li>


    </aside>
