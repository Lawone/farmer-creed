<?php
Class Structlivestockbroiler_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		
	}
	
	public function add() {
		$livestockbroiler_add = array(
			'uid' => $this->session->userdata('uid'),
			'sbid' => $this->input->post('sbid'),
			'stock_density' => $this->input->post('stock_density'),
			'total_stocked' => $this->input->post('total_stocked'),
			'avg_weight' => $this->input->post('avg_weight'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('tbl_livestock_broiler', $livestockbroiler_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function edit($uid) {
		$livestockbroiler_edit = array(
			'uid' => $this->session->userdata('uid'),
			'sbid' => $this->input->post('sbid'),
			'stock_density' => $this->input->post('stock_density'),
			'total_stocked' => $this->input->post('total_stocked'),
			'avg_weight' => $this->input->post('avg_weight'),
			'created_by' => $this->session->userdata('username')
		);
			
		$this->db->where('lbid', $uid);
		$this->db->update('tbl_livestock_broiler',$livestockbroiler_edit);
		
	}
	public function view() {
		
		$this->db->select('lb.sbid,lb.created_date,lb.stock_density,lb.total_stocked,lb.avg_weight,sb.pen_name')->from('tbl_livestock_broiler lb');
		$this->db->join('tbl_structure_broiler sb','lb.sbid = sb.sbid');
		$query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    	}
	//change profile
	public function getLivestockbroiler($lbid)
	{
		$this->db->select('*')->from('tbl_livestock_broiler')->where('lbid', $lbid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function getStructlivestockbroiler()
	{
		$this->db->select('sbid,pen_name')->from('tbl_structure_broiler')->where('uid', $this->session->userdata('uid'));
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	
	public function delete($lbid){
		
		$where = array('lbid' => $lbid);
		$this->db->where($where);
		$this->db->delete('tbl_livestock_broiler');
		
	}
	
}
