<?php 
error_reporting(0);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class income_catfish extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('catfishsales_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	
	
	// domain view method
	public function incomecatfish_view() {
            
            $fi_arr = array();
            if (isset($_REQUEST['search'])) {
                $sql_query = "SELECT  sum(`no_of_fish`) as`no_of_fish`, sum(`total_cost`) as `total_cost`, date(created_date) as `created_date` FROM `tbl_catfishincome` WHERE `uid` = '".$this->session->userdata('uid')."' ";
                if ((!empty($_POST['start-date'])) && (!empty($_POST['end-date']))) {
                    
                    $sql_query .= " AND pond_name='$_POST[pond_name]' AND date(created_date) between '".$_POST['start-date']."' and '".$_POST['end-date']."' ";
                }

                if((isset($_REQUEST['pond_name'])) && (empty($_POST['start-date'])) && (empty($_POST['end-date']))) {
                    $sql_query .= " AND pond_name='$_POST[pond_name]'";
                }
                $sql_query .= " GROUP BY (`created_date`) ";
                $query = $this->db->query($sql_query);
                $result = $query->result_array();
                
            }
            
            //echo "<pre>"; print_r($result); echo "</pre>";
            //exit();
            $data['catfishincome_v'] = $result;
            $data['getpondname']=$this->catfishsales_model->getcatfishsalespond();
            $this->load->view('user/income_catfish', $data);
    }
    
	
	
}
