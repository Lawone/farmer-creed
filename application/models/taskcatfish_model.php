<?php
Class Taskcatfish_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function waterchange_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$waterchange_add = array(
			'uid' => $uid,
			'scid' => $this->input->post('scid'),
			'user_type' => $this->session->userdata('user_type'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id
		);
		
		$this->db->insert('tbl_waterchange', $waterchange_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function waterchange_edit($wcid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$waterfish_edit = array(
			'uid' => $uid,
			'scid' => $this->input->post('scid'),
			'user_type' => $this->session->userdata('user_type'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id
		);
			
		$this->db->where('wcid', $wcid);
		$this->db->update('tbl_waterchange',$waterfish_edit);
		
	}
	public function getWaterchangepond()
	{	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('uid' => $this->session->userdata('parent_uid')));
		}
		$this->db->select('scid,pond_name')->from('tbl_structure_catfish');
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	//change profile
	public function getWaterchangecatfish($wcid)
	{	
		$this->db->select('*')->from('tbl_waterchange')->where('wcid', $wcid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function waterchange_delete($wcid){
		
		$where = array('wcid' => $wcid);
		$this->db->where($where);
		$this->db->delete('tbl_waterchange');
		
	}

	/*============Catfish Feeding===================*/

	public function catfishfeed_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishfeed_add = array(
			'uid' => $uid,
			'scid' => $this->input->post('scid'),
			'user_type' => $this->session->userdata('user_type'),
                        'fpid' => $this->input->post('feedid'),
			'feedsize' => $this->input->post('feedsize'),
			'feedweight' => $this->input->post('feedweight'),
			'feedbrand' => $this->input->post('feedbrand'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id,
			//'time' => $this->input->post('time')
		);
		
		$this->db->insert('tbl_catfishfeed', $catfishfeed_add);
		$ihid = $this->db->insert_id();
		
                $feedpurchased = array("avl_weight" => ( $this->input->post('avlfeedweight') - $this->input->post('feedweight') ) );
                $this->db->where('fpid', $this->input->post('feedid'));
		$this->db->update('tbl_feedpurchased',$feedpurchased);
                
	}
	
	//update profile
	public function catfishfeed_edit($cffid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishfeed_edit = array(
			'uid' => $uid,
			'scid' => $this->input->post('scid'),
			'user_type' => $this->session->userdata('user_type'),
			'feedsize' => $this->input->post('feedsize'),
			'feedweight' => $this->input->post('feedweight'),
			'feedbrand' => $this->input->post('feedbrand'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id,
			//'time' => $this->input->post('time')
		);
		$this->db->where('cffid', $cffid);
		$this->db->update('tbl_catfishfeed',$catfishfeed_edit);
		
	}
	public function getCatfishfeedpond()
	{	
		$this->db->select('lsc.scid,sc.pond_name')->from('tbl_livestock_catfish lsc');
		$this->db->join('tbl_structure_catfish sc','lsc.scid = sc.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('lsc.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('lsc.uid' => $this->session->userdata('parent_uid')));
		}
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	public function getCatfishfeedbrand()
	{	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('uid' => $this->session->userdata('parent_uid')));
		}
                $this->db->where("avl_weight !=", 0.0 );
		$this->db->select('fpid,brand,size,avl_weight,weight')->from('tbl_feedpurchased');
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	//change profile
	public function getCatfishfeeding($cffid)
	{	
		$this->db->select('*')->from('tbl_catfishfeed')->where('cffid', $cffid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function catfishfeed_delete($cffid){
		
                $this->db->select('feedweight,fpid')->from('tbl_catfishfeed')->where(array('cffid' => $cffid));
		$catfishfeeding = $this->db->get();
		$catfishfeeding_arr = $catfishfeeding->row_array();
            
                //echo "<pre>"; print_r($catfishfeeding_arr); echo "</pre>"; 
                
                $this->db->where('fpid', $catfishfeeding_arr['fpid']);
                $this->db->set('avl_weight', 'avl_weight+'.$catfishfeeding_arr['feedweight'], FALSE);
                $this->db->update('tbl_feedpurchased');
                
                
                
		$where = array('cffid' => $cffid);
		$this->db->where($where);
		$this->db->delete('tbl_catfishfeed');
		
	}
	
	/*========== Catfish Sorting ============*/

	public function sorting_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$srtng_info = array(
			'uid' => $uid,
			'from_scid' => $this->input->post('from_scid'),
			'to_scid' => $this->input->post('to_scid'),
			'no_of_fish' => $this->input->post('no_of_fish'),
			'total_weight' => $this->input->post('total_weight'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id,
			'user_type' => $this->session->userdata('user_type')
		);
		
		$this->db->insert('tbl_sorting', $srtng_info);
		$ihid = $this->db->insert_id();
		
		//frompond update stock deletion
		$this->db->select('stock_deletion,weight_deletion')->from('tbl_livestock_catfish')->where(array('uid' => $uid, 'scid' => $this->input->post('from_scid')));
		$stockFpond = $this->db->get();
		$stockFpondData = $stockFpond->row_array();

		$newStockDel1 = $stockFpondData['stock_deletion'] + $this->input->post('no_of_fish');
		$newStockDel = abs($newStockDel1);

		$newWeightDel1 = $stockFpondData['weight_deletion'] + $this->input->post('total_weight');
		$newWeightDel = abs($newWeightDel1);		

		$this->db->where(array('uid' => $uid, 'scid' => $this->input->post('from_scid')));
		$this->db->update('tbl_livestock_catfish',array('stock_deletion' => $newStockDel,'weight_deletion' => $newWeightDel));


		//topond update stock deletion
		$this->db->select('stock_addition,weight_addition')->from('tbl_livestock_catfish')->where(array('uid' => $uid, 'scid' => $this->input->post('to_scid')));
		$stockTpond = $this->db->get();
		$stockTpondData = $stockTpond->row_array();

		$newStockAdd1 = $stockTpondData['stock_addition'] + $this->input->post('no_of_fish');
		$newStockAdd = abs($newStockAdd1);

		$newWeightAdd1 = $stockTpondData['weight_addition'] + $this->input->post('total_weight');
		$newWeightAdd = abs($newWeightAdd1);

		$this->db->where(array('uid' => $uid, 'scid' => $this->input->post('to_scid')));
		$this->db->update('tbl_livestock_catfish',array('stock_addition' => $newStockAdd,'weight_addition' => $newWeightAdd));
		
	}
	
	//update profile
	public function sorting_edit($sid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$srting_edit = array(
			'from_scid' => $this->input->post('from_scid'),
			'to_scid' => $this->input->post('to_scid'),
			'no_of_fish' => $this->input->post('no_of_fish'),
			'total_weight' => $this->input->post('total_weight'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id,
			'user_type' => $this->session->userdata('user_type')
		);
		$this->db->where('sid', $sid);
		$this->db->update('tbl_sorting',$srting_edit);
		
		//frompond update stock deletion
		$this->db->select('stock_deletion,weight_deletion')->from('tbl_livestock_catfish')->where(array('uid' => $uid, 'scid' => $this->input->post('from_scid')));
		$stockFpond = $this->db->get();
		$stockFpondData = $stockFpond->row_array();
		
		if($this->input->post('hidden_fish') == $this->input->post('no_of_fish'))
		{
			$newfishupdt1 = $this->input->post('no_of_fish');
			$newStockDel = abs($newfishupdt1);
		}
		elseif($this->input->post('hidden_fish') < $this->input->post('no_of_fish'))
		{	
			$newStockDel2 = $this->input->post('no_of_fish') - $this->input->post('hidden_fish');
			$newStockDel1 = $stockFpondData['stock_deletion'] + $newStockDel2;
			$newStockDel = abs($newStockDel1);
		}
		elseif($this->input->post('hidden_fish') > $this->input->post('no_of_fish'))
		{
			$newfishupdt = $this->input->post('hidden_fish') - $this->input->post('no_of_fish');
			$newfishupdt1 = $stockFpondData['stock_deletion'] - $newfishupdt;
			$newStockDel = abs($newfishupdt1);
		}
		
		if($this->input->post('hidden_weight') == $this->input->post('total_weight'))
		{
			$newWeightDel1 = $this->input->post('total_weight');
			$newWeightDel = abs($newWeightDel1);
		}
		elseif($this->input->post('hidden_weight') < $this->input->post('total_weight'))
		{
			$newfishupdt = $this->input->post('total_weight') - $this->input->post('hidden_weight');
			$newWeightDel1 = $stockFpondData['weight_deletion'] + $newfishupdt;
			$newWeightDel = abs($newWeightDel1);
		}
		elseif($this->input->post('hidden_weight') > $this->input->post('total_weight'))
		{
			$newweightupdt = $this->input->post('hidden_weight') - $this->input->post('total_weight');
			$newWeightDel1 = $stockFpondData['weight_deletion'] - $newweightupdt;
			$newWeightDel = abs($newWeightDel1);
		}
		
		$this->db->where(array('uid' => $uid, 'scid' => $this->input->post('from_scid')));
		$this->db->update('tbl_livestock_catfish',array('stock_deletion' => $newStockDel,'weight_deletion' => $newWeightDel));
		
		$this->db->where(array('uid' => $uid, 'scid' => $this->input->post('to_scid')));
		$this->db->update('tbl_livestock_catfish',array('stock_addition' => $newStockDel,'weight_addition' => $newWeightDel));
	
	}
	public function getSortingpond()
	{	
		$this->db->select('srtcat.pond_name,srtcat.scid')->from('tbl_livestock_catfish srtlive');
		$this->db->join('tbl_structure_catfish srtcat','srtcat.scid = srtlive.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('srtcat.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('srtcat.uid' => $this->session->userdata('parent_uid')));
		}
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	//change profile
	public function getSortingtask($sid)
	{
		$this->db->select('*')->from('tbl_sorting')->where('sid', $sid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function sorting_delete($sid)
	{
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$this->db->select('from_scid,to_scid,no_of_fish,total_weight')->from('tbl_sorting')->where(array('uid' => $uid, 'sid' => $sid));
		$Stock_Data = $this->db->get();
		$Stock_Update = $Stock_Data->row_array();
		
		//From Pond	
		$this->db->select('stock_deletion,weight_deletion')->from('tbl_livestock_catfish')->where(array('uid' => $uid, 'scid' =>$Stock_Update['from_scid']));
		$stockFpond = $this->db->get();
		$stockFpondData = $stockFpond->row_array();
		
		//print_r($stockFpondData);exit;
		
		$newStockDel1 = $stockFpondData['stock_deletion'] - $Stock_Update['no_of_fish'];
		$newStockDel = abs($newStockDel1);

		$newWeightDel1 = $stockFpondData['weight_deletion'] - $Stock_Update['total_weight'];
		$newWeightDel = abs($newWeightDel1);

		
		//To Pond	
		$this->db->select('stock_addition,weight_addition')->from('tbl_livestock_catfish')->where(array('uid' => $uid, 'scid' =>$Stock_Update['to_scid']));
		$stockTpond = $this->db->get();
		$stockTpondData = $stockTpond->row_array();
		
		$newStockAdd1 = $stockTpondData['stock_addition'] - $Stock_Update['no_of_fish'];
		$newStockAdd = abs($newStockAdd1);

		$newWeightAdd1 = $stockTpondData['weight_addition'] - $Stock_Update['total_weight'];
		$newWeightAdd = abs($newWeightAdd1);

		$this->db->where(array('uid' => $uid, 'scid' => $Stock_Update['from_scid']));
		$this->db->update('tbl_livestock_catfish',array('stock_deletion' => $newStockDel,'weight_deletion' => $newWeightDel));
		
		$this->db->where(array('uid' => $uid, 'scid' => $Stock_Update['to_scid']));
		$this->db->update('tbl_livestock_catfish',array('stock_addition' => $newStockAdd,'weight_addition' => $newWeightAdd));
		
		$where = array('sid' => $sid);
		$this->db->where($where);
		$this->db->delete('tbl_sorting');
		
	}

	/*============Catfish Weight===================*/

	public function catfishweight_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishweight_add = array(
			'uid' => $uid,
			'scid' => $this->input->post('scid'),
			'avg_weight' => $this->input->post('avg_weight'),
			'user_type' => $this->input->post('user_type'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id,
			);
		$this->db->insert('tbl_catfishweight', $catfishweight_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function catfishweight_edit($cwid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishweight_edit = array(
			'uid' => $uid,
			'scid' => $this->input->post('scid'),
			'avg_weight' => $this->input->post('avg_weight'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id,
		);
			
		$this->db->where('cwid', $wid);
		$this->db->update('tbl_catfishweight',$catfishweight_edit);
	}
	public function getCatfishweightpond()
	{	
		$this->db->select('lsc.scid,sc.pond_name')->from('tbl_livestock_catfish lsc');
		$this->db->join('tbl_structure_catfish sc','lsc.scid = sc.scid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('lsc.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('lsc.uid' => $this->session->userdata('parent_uid')));
		}
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	//change profile
	public function getCatfishweight($cwid)
	{	
		$this->db->select('*')->from('tbl_catfishweight')->where('cwid', $cwid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	public function catfishweight_delete($cwid)
	{
		$where = array('cwid' => $cwid);
		$this->db->where($where);
		$this->db->delete('tbl_catfishweight');
	}

	/*============Catfish Mortality===================*/

	public function catfishmortality_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishmortality_add = array(
			'uid' => $uid,
			'scid' => $this->input->post('scid'),
			'mortality' => $this->input->post('mortality'),
			'user_type' => $this->input->post('user_type'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),
			'staff_id' => $staff_id,
			);
		$this->db->insert('tbl_catfish_mortality', $catfishmortality_add);
		$ihid = $this->db->insert_id();
		
		//mortality pond Add
		$this->db->select('mortality_addition')->from('tbl_livestock_catfish')->where(array('uid' => $uid, 'scid' => $this->input->post('scid')));
		$mortalitypond = $this->db->get();
		$mortalityData = $mortalitypond->row_array();
		
		$newMortalityAdd1 = $mortalityData['mortality_addition'] + $this->input->post('mortality');
		$newMortalityAdd = abs($newMortalityAdd1);

		$this->db->where(array('uid' => $uid, 'scid' => $this->input->post('scid')));
		$this->db->update('tbl_livestock_catfish',array('mortality_addition' => $newMortalityAdd));
		
	}
	
	//update profile
	public function catfishmortality_edit($cfmid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishmortality_edit = array(
			'uid' => $uid,
			'scid' => $this->input->post('scid'),
			'mortality' => $this->input->post('mortality'),
			'created_by' => $this->session->userdata('username'),
			'staff_id' => $staff_id,
		);
			
		$this->db->where('cmid', $cfmid);
		$this->db->update('tbl_catfish_mortality',$catfishmortality_edit);

		//mortality pond Add
		$this->db->select('mortality_addition')->from('tbl_livestock_catfish')->where(array('uid' => $uid, 'scid' => $this->input->post('scid')));
		$mortalitypond = $this->db->get();
		$mortalityData = $mortalitypond->row_array();
		
		if($this->input->post('hidden_mortality') == $this->input->post('mortality'))
		{
			$newMortalityAdd1 = $this->input->post('mortality');
			$newMortalityAdd = abs($newMortalityAdd1);
		}
		elseif($this->input->post('hidden_mortality') < $this->input->post('mortality'))
		{
			$newMortalityUpt = $this->input->post('mortality') - $this->input->post('hidden_mortality');
			$newMortalityAdd1 = $mortalityData['mortality_addition'] + $newMortalityUpt;
			$newMortalityAdd = abs($newMortalityAdd1);
		}
		elseif($this->input->post('hidden_mortality') > $this->input->post('mortality'))
		{
			$newMortalityUpt = $this->input->post('hidden_mortality') - $this->input->post('mortality');
			$newMortalityAdd1 = $mortalityData['mortality_addition'] - $newMortalityUpt;
			$newMortalityAdd = abs($newMortalityAdd1);
		}
		$this->db->where(array('uid' => $uid, 'scid' => $this->input->post('scid')));
		$this->db->update('tbl_livestock_catfish',array('mortality_addition' => $newMortalityAdd));
	}
	//change profile
	public function getCatfishmortality($cmid)
	{	
		$this->db->select('*')->from('tbl_catfish_mortality')->where('cmid', $cmid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	public function catfishmortality_delete($cmid)
	{
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$this->db->select('scid,mortality')->from('tbl_catfish_mortality')->where(array('uid' => $uid, 'cmid' => $cmid));
		$Mortality_Data = $this->db->get();
		$Mortality_Delete = $Mortality_Data->row_array();
		
		//Mortality Delete	
		$this->db->select('mortality_addition')->from('tbl_livestock_catfish')->where(array('uid' => $uid, 'scid' =>$Mortality_Delete['scid']));
		$Mortality = $this->db->get();
		$MortalityData = $Mortality->row_array();
		
		$MortalityAdd1 = $MortalityData['mortality_addition'] - $Mortality_Delete['mortality'];
		$MortalityAdd = abs($MortalityAdd1);

		$this->db->where(array('uid' => $uid, 'scid' => $Mortality_Delete['scid']));
		$this->db->update('tbl_livestock_catfish',array('mortality_addition' => $MortalityAdd));
		
		$where = array('cmid' => $cmid);
		$this->db->where($where);
		$this->db->delete('tbl_catfish_mortality');
	}
	
	public function getStaffname($staff_id){
	
		$this->db->select('id,firstname')->from('tbl_users')->where('id', $staff_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;		
	}
}
