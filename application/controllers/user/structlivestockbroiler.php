<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Structlivestockbroiler extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('structlivestockbroiler_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|callback_alreadyExistpen|xss_clean');
		$this->form_validation->set_rules('stock_density', 'Stock Density', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_stocked', 'Total Stocked', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->structlivestockbroiler_model->add();
			$this->session->set_flashdata('message', 'Livestock broiler has been added successfully.');
			redirect(base_url().'user/structlivestockbroiler/add', 'refresh');
		}
		$data['getpondname'] = $this->structlivestockbroiler_model->getStructlivestockbroiler();
		$this->load->view('user/structlivestockbroiler_add', $data);
	}
	public function alreadyExistpen() {
		$this->db->select('sbid')->from('tbl_livestock_broiler')->where(array('sbid' => $this->input->post('sbid'),'uid' => $this->session->userdata('uid')));
		$pen = $this->db->get();
		$existsPen = ($pen -> num_rows() > 0) ? $pen->row_array() : false;
		if($existsPen != false){
			$this->form_validation->set_message('alreadyExistpen', 'Sorry this penname is Already Exist.');
			return false;
		}
		else{
			return true;
		}
	}
	
	//user edit for notification
	public function edit($uid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('sbid', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('stock_density', 'Stock Density', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_stocked', 'Total Stocked', 'trim|required|xss_clean');
		$this->form_validation->set_rules('avg_weight', 'Avg Weight', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->structlivestockbroiler_model->edit($uid);
			$this->session->set_flashdata('message', 'Livestock Broiler has been changed successfully.');
			redirect(base_url().'user/structlivestockbroiler/view', 'refresh');
		}
		$data['getpondname'] = $this->structlivestockbroiler_model->getStructlivestockbroiler();
		$data['structlivestockbroiler_e']=$this->structlivestockbroiler_model->getLivestockbroiler($uid);
		$this->load->view('user/structlivestockbroiler_edit',$data);
	}
	
	// domain view method
	public function view() {
		$this->db->select('lb.lbid,lb.sbid,lb.created_date,lb.stock_density,lb.total_stocked,lb.avg_weight,lb.staff_id,sb.pen_name')->from('tbl_livestock_broiler lb');
		$this->db->join('tbl_structure_broiler sb','lb.sbid = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('lb.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('lb.staff_id' => $this->session->userdata('uid')));
		}
		// data subset
		$query = $this->db->get();
		$data['structlivestockbroiler_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/structlivestockbroiler_view', $data);
	}
	
	//user delete method
	public function delete($lbid){
		
		$this->structlivestockbroiler_model->delete($lbid);
		$this->session->set_flashdata('message', 'Livestock Broiler has been deleted successfully.');
		redirect(base_url().'user/structlivestockbroiler/view', 'refresh');
	}
	
}
