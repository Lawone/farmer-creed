<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broilerfeedpurchased extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('broilerfeedpurchased_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function broilerfeedpurchased_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('no_of_bags', 'No Of Bags', 'trim|required|xss_clean');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('weight', 'Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');

		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->broilerfeedpurchased_model->broilerfeedpurchased_add();
			$this->session->set_flashdata('message', 'Broiler Feed Purchased has been added successfully.');
			redirect(base_url().'user/broilerfeedpurchased/broilerfeedpurchased_add', 'refresh');
		}
		$this->load->view('user/broilerfeedpurchased_add');
	}
	
	//user edit for notification
	public function broilerfeedpurchased_edit($bfpid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('no_of_bags', 'No Of Bags', 'trim|required|xss_clean');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');
		$this->form_validation->set_rules('weight', 'Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->broilerfeedpurchased_model->broilerfeedpurchased_edit($bfpid);
			$this->session->set_flashdata('message', 'Broiler Feed Purchased has been changed successfully.');
			redirect(base_url().'user/broilerfeedpurchased/broilerfeedpurchased_view', 'refresh');
		}
		$data['broilerfeedpurchased_e']=$this->broilerfeedpurchased_model->getBroilerfeedpurchased($bfpid);
		$this->load->view('user/broilerfeedpurchased_edit',$data);
	}
	
	// domain view method
	public function broilerfeedpurchased_view() {
		$this->db->select('*')->from('tbl_broilerfeedpurchased');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['broilerfeedpurchased_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/broilerfeedpurchased_view', $data);
	}
	
	//user delete method
	public function broilerfeedpurchased_delete($bfpid){
		
		$this->broilerfeedpurchased_model->broilerfeedpurchased_delete($bfpid);
		$this->session->set_flashdata('message', 'Broiler Feed Purchased has been deleted successfully.');
		redirect(base_url().'user/broilerfeedpurchased/broilerfeedpurchased_view', 'refresh');
	}
}
