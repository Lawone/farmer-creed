<?php
Class Catfishgenpurchased_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function catfishgenpurchased_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishgenpurchased_add = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'description' => $this->input->post('description'),
			'amount' => $this->input->post('amount'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),	
		);
		
		$this->db->insert('tbl_catfishgenpurchased', $catfishgenpurchased_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function catfishgenpurchased_edit($cgpid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$catfishgenpurchased_edit = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'description' => $this->input->post('description'),
			'amount' => $this->input->post('amount'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
		);
			
		$this->db->where('cgpid', $cgpid);
		$this->db->update('tbl_catfishgenpurchased',$catfishgenpurchased_edit);
		
	}
	
	public function getCatfishgenpurchased($cgpid)
	{	
		$this->db->select('*')->from('tbl_catfishgenpurchased')->where('cgpid', $cgpid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}

	public function catfishgenpurchased_delete($cgpid){
		
		$where = array('cgpid' => $cgpid);
		$this->db->where($where);
		$this->db->delete('tbl_catfishgenpurchased');
		
	}
	public function getStaffname($staff_id){
	
		$this->db->select('id,firstname')->from('tbl_users')->where('id', $staff_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;		
	}
}
