<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px" >

			<!-- MAIN CONTENT -->
			<div id="content" class="container" >

				<div class="row" >
					<!-- NEW COL START -->
					<?php
					if($this->session->flashdata('message')): echo '<div class="alert alert-success" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
					if($this->session->flashdata('error_message')): echo '<div class="alert alert-danger" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('error_message').'</div>'; endif;
		?>
					<center>
						<article class="col-sm-12 col-md-12 col-lg-6" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
				
							<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Add Capital</h2>				
					
				</header>


				<!-- widget div-->
				<div>
									
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >

							
							<fieldset>
								<div class="row">
									<section class="col col-6">
									<label class="label">Name</label>
										<label class="input">
											<input type="text" name="name" id="name">
										</label>
									</section>
									</div>
									<div class="row">
									<section class="col col-6">
									<label class="label">Type</label>
										<label class="input">
											<input type="text" name="type" id="type">
										</label>
									</section>
									</div>
									<div class="row">
									<section class="col col-6">
									<label class="label">Dimension</label>
										<label class="input">
											<input type="text" name="dimension" id="dimension">
										</label>
									</section>
									</div>
									<div class="row">
									<section class="col col-6">
									<label class="label">Density</label>
										<label class="input">
											<input type="text" name="density" id="density">
										</label>
									</section>
								</div>
							</fieldset>
																	<footer>
								<a class="btn btn-primary" href="<?php echo base_url().'user/settings/'?>">Next</a>
																<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" class="btn btn-default" type="submit">
													Submit
												</button>
												
							</footer>
										</form>
				<script type="text/javascript">
		
			runAllForms();

			$(function() {
			
			
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						name : {
							required : true,
							
						},
						type : {
							required : true,
							
						},
						dimension: {
							required : true,
							number: true,
						},
						density: {
							required : true,
							number: true,
							}
					},

					// Messages for form validation
					messages : {
						capital_sales : {
							required : 'Please enter Capital sales',
							number: 'Please enter valid number only'
						},
						capital_contrib : {
							required : 'Please enter Capital contributions',
							number: 'Please enter valid number only'
						},
						dimension : {
							
							number: 'Please enter valid number only'
						},
						density: {
							
							number: 'Please enter valid number only'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		