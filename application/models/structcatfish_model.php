<?php
Class Structcatfish_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function add() {
		$catfish_info = array(
			'uid' => $this->session->userdata('uid'),
			'pond_name' => $this->input->post('pond_name'),
			'type' => $this->input->post('type'),
			'size' => $this->input->post('size'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('tbl_structure_catfish', $catfish_info);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function edit($uid) {
		$catfishedit_info = array(
			'uid' => $this->session->userdata('uid'),
			'pond_name' => $this->input->post('pond_name'),
			'type' => $this->input->post('type'),
			'size' => $this->input->post('size'),
			'created_by' => $this->session->userdata('username')
		);
			
		$this->db->where('scid', $uid);
		$this->db->update('tbl_structure_catfish',$catfishedit_info);
		
	}
	public function view() {
		
		$this->db->select('*')->from('tbl_structure_catfish');
		$query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }
	//change profile
	public function getStructcatfish($scid)
	{
		$this->db->select('*')->from('tbl_structure_catfish')->where('scid', $scid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function delete($scid){
		
		$where = array('scid' => $scid);
		$this->db->where($where);
		$this->db->delete('tbl_structure_catfish');
		
	}
	
}
