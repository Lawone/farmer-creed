<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:75%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-10" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2>Feed Conversion Ratio Broiler</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >
							<fieldset>
								<div class="row">
									<section class="col col-6">
									<label class="label">Pen Name</label>
									<label class="select">
										<select name="pen_name">
										<option value="" selected="" disabled="">Please select Pen name</option>
										<?php
											 if ($getpenname != false) {
												 foreach ($getpenname as $getpennames) {
													echo '<option '.(($_POST['pen_name']==$getpennames['sbid'])?'selected':"").' value="'.$getpennames['sbid'].'">'.$getpennames['pen_name'].'</option>';
												}
											}
											?>
										</select> 
                                                                                                                                                                                <i></i></label>
										</section>
									
									<section class="col col-6">
										<div class="form-group" style="margin-top:5px">
											<label>Start Date</label>
											   <div class="input-group">
												<input type="text" name="start-date" value="<?php echo $_POST['start-date'];?>"  id="start-date" class="form-control datepicker" data-dateformat="yy-mm-dd">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</section>
									</div>
									
									<div class="row">
									<section class="col col-6">
										<div class="form-group">
											<label>End Date</label>
											   <div class="input-group">
												<input type="text" name="end-date" value="<?php echo $_POST['end-date'];?>" id="end-date" class="form-control datepicker" data-dateformat="yy-mm-dd">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
											
										</div>
									</section>
									<section class="col col-6">
										<div class="form-group">
										<div class="input-group">
									<button class="btn btn-primary" name="search" style="margin:23px 5px 4px 2px; padding: 4px; background-color: #3399ff" type="submit"> Search </button>
										</div>	   
										</div>
									</section>
									
									
									
									</div>
							</fieldset></div>
							<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
									</div>	
									<p>&#160;</p>
									<p>&#160;</p>
									
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="row">
										<table id="dt_basic" class="table table-striped table-bordered table-hover" style="margin-top:20px;">
											<thead>
												<tr>
                                                     <th>Date</th>
                                                      <th>No. of Broiler</th>
													<th>Est. Total Weight</th>
													<th>Feed Consumed</th>
													<th>FCR</th>
												</tr>
											</thead>
											<tbody>
											<?php
													// Listing domains
													foreach ($fcrbroiler_v as $fcrbroiler) {
														if($fcrbroiler['avg_weight'] != '' && $fcrbroiler['feedweight']){
													?>
												<tr> 
													<td><?php echo $fcrbroiler['date']; ?></td>
													<td><?php echo $fcrbroiler['total_no']; ?></td>		
													<td><?php echo $fcrbroiler['avg_weight']; ?></td>
													<td><?php echo $fcrbroiler['feedweight']; ?></td>
                                                       <td><?php echo $fcrbroiler['fcr']; ?></td>
							
												</tr>
												<?php } }?>
										</tbody>
										</table>
										</div>
									
									<!-- end widget content -->
				
								</div>
							
							
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			
		
				
						</article>
						</center>
						<!-- END COL -->
				</div>


			
											</div>
		<script type="text/javascript">
		
			runAllForms();

			$(function() {

				// Time 
				$('#time').timepicker();

				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
                                                                                                         pen_name : {
							required : true,
						},
					},

					// Messages for form validation
					messages : {
						pen_name : {
							required : 'Please Select Pen Name'
						},
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
				
				
		$(document).ready(function() {
			
			$(".headerDiv").fadeOut();
			$(".searchMe").fadeOut();
			
			/*
			 * BASIC
			 */
			$('#dt_basic').dataTable({
				"sPaginationType" : "bootstrap_full"/* ,
				//"aLengthMenu": [[1, 2, 3, -1], [1, 2, 3, "All"]],
				"iDisplayLength": row,
				"aoColumnDefs" : [ {
					'bSortable' : false,
					'aTargets' : [ 0,1,7 ]
				} ] */
			});
	
			/* END BASIC */
			
			//Delete user
			$('.DeleteMe').click(function(e){
				e.preventDefault();
				var gid = $(this).attr('href').split('#')[1];
				jConfirm('Are you sure you want to delete this broiler?', 
						 'Confirmation',
						 function(r) {
							if(r){
								window.location = "<?php echo base_url().'user/taskbroiler/broilerfeed_delete/'; ?>"+gid;	
							}
						 }
				);	
			});
			
			pageSetUp();			
	
			/* Add the events etc before DataTables hides a column */
			$("#datatable_fixed_column thead input").keyup(function() {
				oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
			});
	
			$("#datatable_fixed_column thead input").each(function(i) {
				this.initVal = this.value;
			});
			$("#datatable_fixed_column thead input").focus(function() {
				if (this.className == "search_init") {
					this.className = "";
					this.value = "";
				}
			});
			$("#datatable_fixed_column thead input").blur(function(i) {
				if (this.value == "") {
					this.className = "search_init";
					this.value = this.initVal;
				}
			});		
			
	
			var oTable = $('#datatable_fixed_column').dataTable({
				"sDom" : "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
				//"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
				"oLanguage" : {
					"sSearch" : "Search all columns:"
				},
				"bSortCellsTop" : true
			});		
				
			/* TABLE TOOLS */
			$('#datatable_tabletools').dataTable({
				"sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
				"oTableTools" : {
					"aButtons" : ["copy", "print", {
						"sExtends" : "collection",
						"sButtonText" : 'Save <span class="caret" />',
						"aButtons" : ["csv", "xls", "pdf"]
					}],
					"sSwfPath" : "<?php echo base_url(); ?>js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
				},
				"fnInitComplete" : function(oSettings, json) {
					$(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
						$(this).addClass('btn-sm btn-default');
					});
				}
			});
		
		/* END TABLE TOOLS */
		
		});
		
				
			});
		</script>
		
		

		
