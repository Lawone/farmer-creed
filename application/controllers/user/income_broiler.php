<?php 
error_reporting(0);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class income_broiler extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('broilersales_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	
	
	// domain view method
	public function incomebroiler_view() {
            /*
             if (isset($_REQUEST['search'])) {

            if ((isset($_REQUEST['pen_name']))) {
                $sql_query = "SELECT ci.pen_name,SUM(ci.poultry) as  poultry,SUM(ci.total_cost) as total_cost,ci.created_date, cat.pen_name FROM `tbl_structure_broiler` cat JOIN tbl_broilerincome ci WHERE cat.sbid = ci.pen_name AND ci.pen_name='$_POST[pen_name]'";
                
                //$sql_query = "SELECT ci.pond_name,ci.no_of_fish,ci.total_cost,ci.created_date, cat.pond_name FROM  `tbl_structure_catfish` cat JOIN tbl_catfishincome ci WHERE cat.scid = ci.pond_name AND ci.pond_name='$_POST[pond_name]' AND ci.`created_date` BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."' ";
                $query = $this->db->query($sql_query);
                $result = $query->result_array();
            }
            
            if ((isset($_REQUEST['pen_name'])) &&(!empty($_POST['start-date'])) && (!empty($_POST['end-date']))) {
                $sql_query = "SELECT ci.pen_name,SUM(ci.poultry) as  poultry,SUM(ci.total_cost) as total_cost,ci.created_date, cat.pen_name FROM `tbl_structure_broiler` cat JOIN tbl_broilerincome ci WHERE cat.sbid = ci.pen_name AND ci.pen_name='$_POST[pen_name]' AND date(ci.`created_date`) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."'";
                
                //$sql_query = "SELECT ci.pond_name,ci.no_of_fish,ci.total_cost,ci.created_date, cat.pond_name FROM  `tbl_structure_catfish` cat JOIN tbl_catfishincome ci WHERE cat.scid = ci.pond_name AND ci.pond_name='$_POST[pond_name]' AND ci.`created_date` BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."' ";
                $query = $this->db->query($sql_query);
                $result = $query->result_array();
            }

           
          
            
        } */
        
            if (isset($_REQUEST['search'])) {
                $sql_query = "SELECT  sum(`poultry`) as`poultry`, sum(`total_cost`) as `total_cost`, date(created_date) as `created_date` FROM `tbl_broilerincome` WHERE `uid` = '".$this->session->userdata('uid')."' ";
                if ((!empty($_POST['start-date'])) && (!empty($_POST['end-date']))) {
                    
                    
                    $sql_query .= " AND pen_name='".$_POST["pen_name"]."' AND date(created_date) between '".$_POST['start-date']."' and '".$_POST['end-date']."'";
                }

                if((isset($_REQUEST['pen_name'])) && (empty($_POST['start-date'])) && (empty($_POST['end-date']))) {
                    $sql_query .= " AND pen_name='".$_POST["pen_name"]."'";
                }
                $sql_query .= " GROUP BY (`created_date`) ";
                $query = $this->db->query($sql_query);
                $result = $query->result_array();
                
            }
        
        $data['broilerincome_v'] = $result;
        $data['getpenname']=$this->broilersales_model->getbroilersalespen();
        $this->load->view('user/income_broiler', $data);
    }
    
	
	
}
