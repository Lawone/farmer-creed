<?php

error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class feedstock_inventory_catfish extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->app_access->user(); // check access permission for user
        $this->load->model(array('catfishsales_model'));
    }

    // Registration
    public function index() {
        
    }

    //user add for notification
    //user edit for notification
    // domain view method
    public function feedinventory_view() {

        if (isset($_REQUEST['search'])) {

            if ((!empty($_POST['start-date'])) && (!empty($_POST['end-date']))) {

                /*$this->db->where("DATE_FORMAT(created_date,'%Y-%m-%d') >=", $_POST['start-date']);
                $this->db->where("DATE_FORMAT(created_date,'%Y-%m-%d') <=", $_POST['end-date']);
                $this->db->select('sum(weight) as initialStock')->from('tbl_feedpurchased')->where(array('uid' => $this->session->userdata('uid')));
                $initialStockQuery = $this->db->get();
                $initialTotalStockData = $initialStockQuery->row_array();
                $initialTotalStock = ($initialStockQuery->num_rows() > 0) ? $initialTotalStockData['initialStock'] : 0;*/


                $sql_query = "SELECT date_format(created_date,'%Y-%m-%d') as created_date, size, brand,sum(avl_weight) as avl_weight ,sum(weight)  as weight,no_of_bags  FROM  `tbl_feedpurchased` WHERE 
                       date_format(created_date,'%Y-%m-%d') BETWEEN '" . $_POST['start-date'] . "' AND '" . $_POST['end-date'] . "' and uid = '" . $this->session->userdata('uid') . "' group by  date_format(created_date,'%Y-%m-%d'),  size, brand";
                $query = $this->db->query($sql_query);
                $result = ($query->num_rows() > 0) ? $query->result_array() : false;

                $feedArr = array();
                $feedResArr = array();
                //$baseStock = $initialTotalStock;
                foreach ($result as $feed) {

                    /*$this->db->where("DATE_FORMAT(created_date,'%Y-%m-%d')", $feed['created_date']);
                    $this->db->select('weight')->from('tbl_feedpurchased')->where(array('uid' => $this->session->userdata('uid')));

                    $purchaseQuery = $this->db->get();
                    $purchaseQueryData = $purchaseQuery->row_array();
                    $stockPurchase = ($purchaseQuery->num_rows() > 0) ? $purchaseQueryData['weight'] : 0;*/
                    $feedArr['created_date'] = $feed['created_date'];
                    $feedArr['avl_weight'] = $feed['avl_weight'];
					$feedArr['feedsize'] = $feed['size'];
                    $feedArr['feedbrand'] = $feed['brand'];
                    //$feedArr[$key]['stockPurchase'] = $stockPurchase;
                    $feedArr['feedweight'] = $feed['weight']  * $feed['no_of_bags'];
                    $feedArr['currstock'] = ($feed['weight'] * $feed['no_of_bags']) - ($feed['avl_weight']);
                    
                    array_push($feedResArr,$feedArr);
                    //$feedArr[$key]['currStock'] = number_format($feedArr[$key]['stockPurchase'] - $feedArr[$key]['feedweight'], 2, '.', '');
                }
            }
        }
        $data['catfishincome_v'] = $feedResArr;
        $data['getpondname'] = $this->catfishsales_model->getcatfishsalespond();
        $this->load->view('user/feedstock_inventory_catfish', $data);
    }

}