<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="initial-scale=1.0">
<!-- So that mobile webkit will display zoomed in -->
<!-- CSS for mobile devices. Attribute selector used so that Yahoo! mail will ignore styles -->
<style type="text/css">
::selection
{
color:#fff;
background-color:#49A4A2;}
::-moz-selection
{
color:#fff; background-color:#49A4A2;}
.top_bar {
	display:none;
	}
@media only screen and (max-width: 480px) {
	table[class=table], td[class=cell] { width: 300px !important; }
	table[class=table], td[class=cell] { width: 300px !important; }
}
.link-btn {
    background-color: #797979;
    border-radius: 15px;
    box-shadow: none;
    color: #FFFFFF;
    float: right;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 16px;
    font-weight: bold;
    height: auto;
    padding: 5px 40px;
    width: auto;
}
</style>
	<title>Welcome to Uptimeweb</title>
</head>
<body style="margin:0; padding:0; background-color:#F4F4F4; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:14px;">	
	<?php if($type == 'actEmail') { ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
	<tr>
		<td bgcolor="#F4F4F4" align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
			<td><div style="max-width:700px; margin:0 auto;">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center" bgcolor="#D7D7D7" style="padding-top:10px; padding-bottom:10px;"><img src="<?php echo base_url();?>img/logo.png" style="width:200px;"></td>
						</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="30">&nbsp;</td>
								<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td height="20">&nbsp;</td>
								</tr>
								<tr>
									<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear <?php echo $info['firstname']; ?> </strong></td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left">Thank you for registering with Cloud Simulation.<br><br>
									Our admin has been approved your account, please&nbsp;&nbsp;<a href="<?php echo base_url().'user/dashboard/login'; ?>" target="_blank" style="font-size:14px; color:#2C6CB8;" >click here</a>&nbsp;&nbsp;to login your account<br>
									</td>
								</tr>
  <tr>
    <td style="font-size:15px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left"><br>
<br>
<strong>cloudsimulation.com</strong><br>
<br>

</td>
  </tr>

</table></td>
    <td width="30">&nbsp;</td>
  </tr>
</table>


    
    
</td>
  </tr>
  
</table>
</td>
                </tr>
                <tr>
                <td bgcolor="#D7D7D7" style="padding-top:22px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="19%">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333" style="-webkit-border-radius: 15px;
border-radius: 15px;">
  <tr>
    <td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
	Copyright © 2014 Cloud Simulation. All rights reserved.</td>
  </tr>
</table>
</td>
    <td width="19%">&nbsp;</td>
  </tr>
</table>

               
                </td>
                </tr>
                <tr>
                <td bgcolor="#D7D7D7" style="padding-top:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="color:#4A4A4A; font-size:11px; font-family:Arial, Helvetica, sans-serif;" align="center">You have received this email cause you have subscribed to cloud simulation
© 2014 Your Company<br><br>

<br>
</td>
  </tr>
</table>


               
                </td>
                </tr>
                
              </table>
            </div></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
<?php }else if($type == 'usrRegAdmin') { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
	<tr>
		<td bgcolor="#F4F4F4" align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
			<td><div style="max-width:700px; margin:0 auto;">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center" bgcolor="#D7D7D7" style="padding-top:10px; padding-bottom:10px;"><img src="<?php echo base_url();?>img/logo.png" style="width:200px;"></td>
						</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="30">&nbsp;</td>
								<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td height="20">&nbsp;</td>
								</tr>
								<tr>
									<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear admin, </strong></td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left"><strong>User information mentioned below.</strong>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										Name<span style="margin-left:4em;">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info['firstname']; ?></span>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										Username<span style="margin-left:4.1em;">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info['username']; ?></span>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										Email<span style="margin-left:4.1em;">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info['email']; ?></span>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										User Type<span style="margin-left:1.8em;">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info['userType']; ?></span>
									</td>
								</tr>

</table></td>
    <td width="30">&nbsp;</td>
  </tr>
</table>


    
    
</td>
  </tr>
  
</table>
</td>
                </tr>
                <tr>
                <td bgcolor="#D7D7D7" style="padding:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="19%">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333" style="-webkit-border-radius: 15px;
border-radius: 15px;">
  <tr>
    <td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
	Copyright © 2014 Cloud simulation. All rights reserved.</td>
  </tr>
</table>
</td>
    <td width="19%">&nbsp;</td>
  </tr>
</table>

               
                </td>
                </tr>
                
              </table>
            </div></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
<?php }else if($type == 'fgtPwd') { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
	<tr>
		<td bgcolor="#F4F4F4" align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
			<td><div style="max-width:700px; margin:0 auto;">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center" bgcolor="#D7D7D7" style="padding-top:10px; padding-bottom:10px;"><img src="<?php echo base_url();?>img/logo.png" style="width:200px;"></td>
						</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="30">&nbsp;</td>
								<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td height="20">&nbsp;</td>
								</tr>
								<tr>
									<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear&nbsp;&nbsp;<?php echo $info['firstname']; ?>, </strong></td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										<p style="line-height:20px;">You have asked to reset your password in Cloud Simulation, The Cloud computing in Agriculture sector.</p>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										Your new password for Cloud simulation, The Cloud computing in Agriculture sector is :&nbsp;&nbsp;<a href="#" style="font-size:14px; color:#2C6CB8;" ><?php echo $info['newPwd']; ?></a>.
									</td>
								</tr>
</table></td>
    <td width="30">&nbsp;</td>
  </tr>
</table>


    
    
</td>
  </tr>
  
</table>
</td>
                </tr>
                <tr>
                <td bgcolor="#D7D7D7" style="padding:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="19%">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333" style="-webkit-border-radius: 15px;
border-radius: 15px;">
  <tr>
    <td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
	Copyright © 2014 Cloud simulation. All rights reserved.</td>
  </tr>
</table>
</td>
    <td width="19%">&nbsp;</td>
  </tr>
</table>

               
                </td>
                </tr>
                
              </table>
            </div></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
<?php }else if($type == 'adminReg') { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
	<tr>
		<td bgcolor="#F4F4F4" align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
			<td><div style="max-width:700px; margin:0 auto;">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center" bgcolor="#D7D7D7" style="padding-top:10px; padding-bottom:10px;"><img src="<?php echo base_url();?>img/logo.png" style="width:200px;"></td>
						</tr>
						</table>
					</td>
				</tr>
                <tr>
					<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="30">&nbsp;</td>
								<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td height="20">&nbsp;</td>
								</tr>
								<tr>
									<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear <?php echo $info['firstname']; ?> </strong></td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left">Thank you for joining with Cloud Simulation.<br><br>
									Our admin has been created your account with temporary username and password. You must change the username and password the next time sign in. <br><br>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left"><strong>Login information mentioned below.</strong>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										Username<span style="margin-left:4em;">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info['username']; ?></span>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										password<span style="margin-left:4.1em;">:&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $info['pwd']; ?></span>
									</td>
								</tr>
								<tr>
									<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
										please&nbsp;&nbsp;<a href="<?php echo base_url().'admin/dashboard/login'; ?>" target="_blank" style="font-size:14px; color:#2C6CB8;" >click here</a>&nbsp;&nbsp;to login your account
									</td>
								</tr>

</table></td>
    <td width="30">&nbsp;</td>
  </tr>
</table>


    
    
</td>
  </tr>
  
</table>
</td>
                </tr>
                <tr>
                <td bgcolor="#D7D7D7" style="padding:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="19%">&nbsp;</td>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333" style="-webkit-border-radius: 15px;
border-radius: 15px;">
  <tr>
    <td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
	Copyright © 2014 Cloud simulation. All rights reserved.</td>
  </tr>
</table>
</td>
    <td width="19%">&nbsp;</td>
  </tr>
</table>

               
                </td>
                </tr>
                
              </table>
            </div></td>
        </tr>
      </table>
      </td>
  </tr>
</table>
<?php } ?>
</body>
</html>