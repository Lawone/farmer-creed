<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px" >

			<!-- MAIN CONTENT -->
			<div id="content" class="container" >

				<div class="row" >
					<!-- NEW COL START -->
					<?php
					if($this->session->flashdata('message')): echo '<div class="alert alert-success" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
					if($this->session->flashdata('error_message')): echo '<div class="alert alert-danger" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('error_message').'</div>'; endif;
		?>
					<center>
						<article class="col-sm-12 col-md-12 col-lg-6" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
				
							<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2 style="font-size: 100%">Estimated Investment</h2>
				</header>
				
				<!-- widget div-->
				<div>
					<p>Investment <span style="padding-left:7.1em; color:red;">500,000</a></p>
					<p>Input an estimate of what you want to invest and see a breakdown of how this money should be utilized.</p>
										<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
					</div>
										<!-- end widget edit box -->
					
					<!-- widget content -->
					<p><b>FIXED EXPENSES</b></p>
					<p>Type of Pond   <span style="padding-left:5.8em; color:grey;">Fiberglass Ponds</span></p>
					<p>Space Required  <span style="padding-left:4.7em; color:grey;">125sqm</span></p>
					<p>Budgeted Amount  <span style="padding-left:3.4em; color:grey;">150,000</span></p>
					<p>&#160;</p>
					<p><b>RUNNING EXPENSES</b></p>
					<p>Feeding Budget   <span style="padding-left:4.6em; color:grey;">180,000</span></p>
					<p>Fingerling Budget   <span style="padding-left:3.7em; color:grey;">50,000</span></p>
					<p>Other Runn. Exp.  <span style="padding-left:3.9em; color:grey;">50,000</span></p>
					<p>&#160;</p>
					<p><b>PROFITABILITY</b></p>
					<p>Estimated Sales   <span style="padding-left:4.5em; color:grey;">600,000</span></p>
					
						<footer>
								<a class="btn btn-primary" style="float:right;" href="<?php echo base_url().'user/settings/site_selection'?>">Next</a>
																
																
																</footer>
					
</body>
</html>
										
										
