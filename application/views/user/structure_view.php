<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Cloud Simulation </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:62%;">

				<div class="option" style="margin-bottom:1%"><a class="btn btn-warning btn-xs" href="<?php echo base_url(); ?>user/structure/structure_add"><i class="fa fa-plus-square"></i> Add</a>&#160;&#160;&#160;&#160;&#160;&#160;<a class="btn btn-danger btn-xs" href="<?php echo base_url(); ?>user/structure/view"><i class="fa fa-list"></i> View All</a>
			</div>
			
			<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success alert-block"> <button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
					if($this->session->flashdata('warning')): echo '<div class="alert alert-danger alert-block"> <button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('warning').'</div>'; endif;
					?>
				<div class="row" >
					<!-- NEW COL START -->
					<center>
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12a">
				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
								<!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				
								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"
				
								-->
								<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
									
									<h2>Structure View</h2>
				
								</header>
				
								<!-- widget div-->
								<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
										
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									
		<script type="text/javascript">
		$(document).ready(function() {
			
			$(".headerDiv").fadeOut();
			$(".searchMe").fadeOut();
			
			/*
			 * BASIC
			 */
			var aoColumns = [
			  				{ "bSortable": false },
				null,
				null,
				null,
				null,
				{ "bSortable": false }
			];
			$('#dt_basic').dataTable({
				"sPaginationType" : "bootstrap_full",
				//"aLengthMenu": [[1, 2, 3, -1], [1, 2, 3, "All"]],
				"aoColumnDefs" : [ {
						"bVisible": false, 
						"aTargets": [0]
					} ],
				"aoColumns": aoColumns,
				"aaSorting" : [[0, 'desc']]
			});
	
			/* END BASIC */
			
			//Delete user
			$('.DeleteMe').click(function(e){
				e.preventDefault();
				var gid = $(this).attr('href').split('#')[1];
				jConfirm('Are you sure you want to delete this capital?', 
						 'Confirmation',
						 function(r) {
							if(r){
								window.location = "<?php echo base_url().'user/capital/delete/'; ?>"+gid;	
							}
						 }
				);	
			});
			
			pageSetUp();			
	
			/* Add the events etc before DataTables hides a column */
			$("#datatable_fixed_column thead input").keyup(function() {
				oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
			});
	
			$("#datatable_fixed_column thead input").each(function(i) {
				this.initVal = this.value;
			});
			$("#datatable_fixed_column thead input").focus(function() {
				if (this.className == "search_init") {
					this.className = "";
					this.value = "";
				}
			});
			$("#datatable_fixed_column thead input").blur(function(i) {
				if (this.value == "") {
					this.className = "search_init";
					this.value = this.initVal;
				}
			});		
			
	
			var oTable = $('#datatable_fixed_column').dataTable({
				"sDom" : "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
				//"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
				"oLanguage" : {
					"sSearch" : "Search all columns:"
				},
				"bSortCellsTop" : true
			});		
				
			/* TABLE TOOLS */
			$('#datatable_tabletools').dataTable({
				"sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
				"oTableTools" : {
					"aButtons" : ["copy", "print", {
						"sExtends" : "collection",
						"sButtonText" : 'Save <span class="caret" />',
						"aButtons" : ["csv", "xls", "pdf"]
					}],
					"sSwfPath" : "<?php echo base_url(); ?>js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
				},
				"fnInitComplete" : function(oSettings, json) {
					$(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
						$(this).addClass('btn-sm btn-default');
					});
				}
			});
		
		/* END TABLE TOOLS */
		
		});
		</script>		