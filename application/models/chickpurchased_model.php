<?php
Class Chickpurchased_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function chickpurchased_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$chickpurchased_add = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'pen_name' => $this->input->post('pen_name'),
			'total_number' => $this->input->post('total_number'),
			'total_cost' => $this->input->post('total_cost'),
			'avg_weight' => $this->input->post('avg_weight'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),	
		);
		//print_r($chickpurchased_add);exit;
		$this->db->insert('tbl_chickpurchased', $chickpurchased_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function chickpurchased_edit($ckpid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$chickpurchased_edit = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'pen_name' => $this->input->post('pen_name'),
			'total_number' => $this->input->post('total_number'),
			'total_cost' => $this->input->post('total_cost'),
			'avg_weight' => $this->input->post('avg_weight'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
		);
			
		$this->db->where('ckpid', $ckpid);
		$this->db->update('tbl_chickpurchased',$chickpurchased_edit);
		
	}
	
	public function getChickpurchased($ckpid)
	{	
		$this->db->select('*')->from('tbl_chickpurchased')->where('ckpid', $ckpid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}

	public function getChickpurchasedpen()
	{	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('uid' => $this->session->userdata('parent_uid')));
		}
		$this->db->select('sbid,pen_name')->from('tbl_structure_broiler');
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}	
	
	public function chickpurchased_delete($ckpid){
		
		$where = array('ckpid' => $ckpid);
		$this->db->where($where);
		$this->db->delete('tbl_chickpurchased');
		
	}
	public function getStaffname($staff_id){
	
		$this->db->select('id,firstname')->from('tbl_users')->where('id', $staff_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;		
	}
}
