<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports extends CI_Controller {
    
    function __construct() 
    {
        parent::__construct();
        $this->load->model(array('catfishsales_model','broilersales_model'));
    }

    // domain view method
    public function livestock_inventory_catfish() {
        $this->load->view('user/livestock_inventory_catfish');
    }

    public function livestock_inventory_broiler() {
        $this->load->view('user/livestock_inventory_broiler');
    }

    public function income_expenses_report() {
        $data['getpondname'] = $this->catfishsales_model->getcatfishsalespond();
        $this->load->view('user/income_expenses_reports_barchart', $data);
    }

    public function income_expenses_report_broiler_barchart() {
        $data['getpenname'] = $this->broilersales_model->getBroilersalespen();
        $this->load->view('user/income_expenses_reports_barchart_broiler', $data);
    }

    public function business_expenses_broiler_reports() {
        $data['getpenname'] = $this->broilersales_model->getBroilersalespen();
        $this->load->view('user/business_expenses_broiler_reports', $data);
    }

    public function business_expenses_reports() {
        $data['getpondname'] = $this->catfishsales_model->getcatfishsalespond();
        $this->load->view('user/business_expenses_reports', $data);
    }

    public function jsondata() {
        $this->load->view('user/data');
    }

    //user delete method
}
