-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: sql209.byethost10.com
-- Generation Time: Jan 20, 2015 at 01:48 AM
-- Server version: 5.6.21-70.1
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `b10_15188465_farmerscreed`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_broilerfeed`
--

CREATE TABLE IF NOT EXISTS `tbl_broilerfeed` (
  `bfid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sbid` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `feedtype` int(11) NOT NULL,
  `feedweight` float(9,2) NOT NULL,
  `feedbrand` varchar(50) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  `time` varchar(20) NOT NULL,
  PRIMARY KEY (`bfid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tbl_broilerfeed`
--

INSERT INTO `tbl_broilerfeed` (`bfid`, `uid`, `sbid`, `user_type`, `feedtype`, `feedweight`, `feedbrand`, `created_by`, `created_date`, `staff_id`, `time`) VALUES
(21, 38, 27, 0, 1, 78.00, 'NEW BR', 'kannan', '2014-12-15 01:10:30', 0, ''),
(20, 6, 28, 0, 3, 50.00, 'suguna', 'venkat', '2014-12-14 23:38:23', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_broilerfeedpurchased`
--

CREATE TABLE IF NOT EXISTS `tbl_broilerfeedpurchased` (
  `bfpid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `no_of_bags` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `weight` float(9,2) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `total_cost` float(9,2) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  PRIMARY KEY (`bfpid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_broilerfeedpurchased`
--

INSERT INTO `tbl_broilerfeedpurchased` (`bfpid`, `uid`, `no_of_bags`, `type`, `weight`, `brand`, `total_cost`, `staff_id`, `created_date`, `created_by`) VALUES
(6, 6, 12, 'Broiler Starter', 23.00, 'suguna', 333.00, 0, '2014-12-12 01:34:05', 'venkat'),
(2, 17, 50, '', 45.00, 'NEW', 500.00, 0, '2014-12-06 01:23:07', 'madhan'),
(3, 17, 60, '', 88.00, 'NEW', 800.00, 0, '2014-12-06 00:26:00', 'madhan'),
(4, 17, 88, '', 80.00, 'NEW FEED', 800.00, 0, '2014-12-06 01:23:46', 'madhan'),
(5, 22, 2, '', 20.00, 'local', 999.99, 0, '2014-12-09 09:42:52', 'law'),
(7, 38, 10, 'Broiler Starter', 67.00, 'NEW BR', 890.00, 0, '2014-12-13 06:43:44', 'kannan'),
(8, 38, 20, 'Broiler Finisher', 90.00, 'NEW NEW', 900.00, 0, '2014-12-13 06:44:53', 'kannan'),
(9, 6, 20, 'Broiler Starter', 50.00, 'chicken', 200.00, 0, '2014-12-26 00:55:25', 'venkat');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_broilergenpurchased`
--

CREATE TABLE IF NOT EXISTS `tbl_broilergenpurchased` (
  `bgpid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `amount` float(9,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`bgpid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_broilergenpurchased`
--

INSERT INTO `tbl_broilergenpurchased` (`bgpid`, `uid`, `date`, `description`, `amount`, `user_type`, `staff_id`, `created_by`, `created_date`) VALUES
(4, 38, '12/12/2014', 'ttt', 8000.00, 0, 0, 'kannan', '2014-12-12 06:51:46'),
(3, 6, '12/12/2014', 'This is test', 500.00, 0, 0, 'venkat', '2014-12-11 23:43:29'),
(5, 6, '26/12/2014', 'This one also test', 500.00, 0, 0, 'venkat', '2014-12-26 00:56:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_broilerincome`
--

CREATE TABLE IF NOT EXISTS `tbl_broilerincome` (
  `biid` int(11) NOT NULL AUTO_INCREMENT,
  `bsid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `pen_name` int(11) NOT NULL,
  `poultry` int(11) NOT NULL,
  `total_cost` float(9,2) NOT NULL,
  `income_type` varchar(50) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`biid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_broilerincome`
--

INSERT INTO `tbl_broilerincome` (`biid`, `bsid`, `uid`, `pen_name`, `poultry`, `total_cost`, `income_type`, `user_type`, `staff_id`, `created_by`, `created_date`) VALUES
(1, 6, 6, 28, 100, 999.99, 'sales', 0, 0, 'venkat', '11/12/2014'),
(5, 10, 38, 27, 8000, 8000.00, 'sales', 0, 0, 'kannan', '12/12/2014'),
(6, 11, 38, 27, 600, 600.00, 'sales', 0, 0, 'kannan', '12/12/2014'),
(7, 12, 6, 28, 100, 5000.00, 'sales', 0, 0, 'venkat', '01/12/2014'),
(8, 13, 38, 27, 900, 900.00, 'sales', 0, 0, 'kannan', '14/01/2015'),
(9, 14, 6, 30, 100, 33.00, 'sales', 0, 0, 'venkat', '14/01/2015'),
(10, 15, 6, 30, 100, 999.99, 'sales', 0, 0, 'venkat', '14/01/2015'),
(11, 16, 6, 28, 100, 33.00, 'sales', 0, 0, 'venkat', '14/01/2015'),
(12, 17, 38, 27, 560, 560.00, 'sales', 0, 0, 'kannan', '14/01/2015');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_broilersales`
--

CREATE TABLE IF NOT EXISTS `tbl_broilersales` (
  `bsid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `pen_name` int(11) NOT NULL,
  `poultry` int(11) NOT NULL,
  `total_weight` float(9,2) NOT NULL,
  `total_cost` float(9,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`bsid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_broilersales`
--

INSERT INTO `tbl_broilersales` (`bsid`, `uid`, `date`, `pen_name`, `poultry`, `total_weight`, `total_cost`, `user_type`, `staff_id`, `created_by`, `created_date`) VALUES
(1, 6, '08/12/2014', 17, 600, 0.00, 0.00, 0, 0, 'venkat', '2014-12-08 01:37:47'),
(2, 17, '08/12/2014', 18, 300, 0.00, 0.00, 0, 0, 'madhan', '2014-12-08 02:05:22'),
(3, 17, '08/12/2014', 21, 56, 0.00, 0.00, 0, 0, 'madhan', '2014-12-09 01:28:59'),
(4, 17, '08/12/2014', 21, 56, 0.00, 0.00, 0, 0, 'madhan', '2014-12-09 01:33:26'),
(5, 22, '10/12/2014', 23, 667, 0.00, 0.00, 0, 0, 'law', '2014-12-09 11:15:54'),
(6, 6, '11/12/2014', 28, 100, 100.00, 999.99, 0, 0, 'venkat', '2015-01-14 00:33:52'),
(10, 38, '12/12/2014', 27, 8000, 8000.00, 8000.00, 0, 0, 'kannan', '2015-01-14 00:32:43'),
(11, 38, '12/12/2014', 27, 600, 600.00, 600.00, 0, 0, 'kannan', '2015-01-14 00:31:58'),
(12, 6, '01/12/2014', 28, 100, 100.00, 5000.00, 0, 0, 'venkat', '2015-01-14 00:33:46'),
(13, 38, '14/01/2015', 27, 900, 900.00, 900.00, 0, 0, 'kannan', '2015-01-14 00:52:52'),
(14, 6, '14/01/2015', 30, 100, 100.00, 33.00, 0, 0, 'venkat', '2015-01-14 00:34:09'),
(15, 6, '14/01/2015', 30, 100, 100.00, 999.99, 0, 0, 'venkat', '2015-01-14 00:34:20'),
(16, 6, '14/01/2015', 28, 100, 100.00, 33.00, 0, 0, 'venkat', '2015-01-14 00:34:38'),
(17, 38, '14/01/2015', 27, 560, 560.00, 560.00, 0, 0, 'kannan', '2015-01-14 00:54:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_broilertreatment`
--

CREATE TABLE IF NOT EXISTS `tbl_broilertreatment` (
  `btid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sbid` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `vaccine` varchar(50) NOT NULL,
  `treatment` varchar(50) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`btid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbl_broilertreatment`
--

INSERT INTO `tbl_broilertreatment` (`btid`, `uid`, `sbid`, `user_type`, `vaccine`, `treatment`, `created_by`, `created_date`, `staff_id`) VALUES
(14, 38, 25, 0, 'Newcastle Disease', '35', 'kannan', '2014-12-10 08:34:54', 0),
(11, 17, 18, 0, 'Gumboro', '66', 'madhan', '2014-12-08 01:40:26', 0),
(12, 22, 23, 0, 'Newcastle Disease', '23', 'law', '2014-12-09 09:55:55', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_broilerweight`
--

CREATE TABLE IF NOT EXISTS `tbl_broilerweight` (
  `bwid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sbid` int(11) NOT NULL,
  `avg_weight` float(5,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`bwid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tbl_broilerweight`
--

INSERT INTO `tbl_broilerweight` (`bwid`, `uid`, `sbid`, `avg_weight`, `user_type`, `created_by`, `created_date`, `staff_id`) VALUES
(9, 6, 17, 55.00, 0, 'venkat', '2014-11-28 05:27:11', 0),
(19, 6, 20, 100.00, 0, 'venkatstaff', '2014-12-10 00:44:27', 21),
(18, 22, 23, 45.00, 0, 'law', '2014-12-09 09:52:06', 0),
(15, 17, 18, 24.00, 0, 'madhan', '2014-12-02 06:57:00', 0),
(16, 17, 18, 24.00, 0, 'madhan', '2014-12-02 08:07:24', 0),
(17, 17, 21, 26.00, 0, 'madhan', '2014-12-03 01:44:31', 0),
(20, 38, 27, 800.00, 0, 'kannan', '2014-12-11 06:21:53', 0),
(21, 6, 28, 100.00, 0, 'venkat', '2014-12-13 04:03:12', 0),
(22, 38, 27, 25.00, 0, 'kannan', '2014-12-15 01:15:28', 0),
(23, 38, 27, 12.00, 0, 'kannan', '2014-12-15 23:42:05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_broiler_mortality`
--

CREATE TABLE IF NOT EXISTS `tbl_broiler_mortality` (
  `bmid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sbid` int(11) NOT NULL,
  `mortality` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`bmid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_broiler_mortality`
--

INSERT INTO `tbl_broiler_mortality` (`bmid`, `uid`, `sbid`, `mortality`, `user_type`, `created_by`, `created_date`, `staff_id`) VALUES
(3, 17, 21, 3, 0, 'madhan', '2014-12-03 00:04:26', 0),
(4, 17, 21, 5, 0, 'madhan', '2014-12-03 02:06:48', 0),
(5, 17, 18, 1, 0, 'madhan', '2014-12-03 05:07:29', 0),
(7, 22, 23, 12, 0, 'law', '2014-12-09 09:53:14', 0),
(8, 38, 27, 3, 0, 'kannan', '2014-12-12 07:34:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catfishfeed`
--

CREATE TABLE IF NOT EXISTS `tbl_catfishfeed` (
  `cffid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `scid` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `feedsize` float(9,2) NOT NULL,
  `feedbrand` varchar(50) NOT NULL,
  `feedweight` float(9,2) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  `time` varchar(20) NOT NULL,
  PRIMARY KEY (`cffid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `tbl_catfishfeed`
--

INSERT INTO `tbl_catfishfeed` (`cffid`, `uid`, `scid`, `user_type`, `feedsize`, `feedbrand`, `feedweight`, `created_by`, `created_date`, `staff_id`, `time`) VALUES
(37, 22, 18, 0, 6.00, 'COOPENS', 12.00, 'law', '2014-12-19 08:53:41', 0, ''),
(38, 38, 22, 0, 8.00, 'BB Brand', 90.00, 'kannan', '2014-12-22 09:24:29', 0, ''),
(31, 38, 22, 0, 9.00, 'NEW N', 24.00, 'kannan', '2014-12-13 05:05:28', 0, ''),
(32, 38, 23, 0, 9.00, 'BB Brand', 100.00, 'kannan', '2014-12-23 00:27:38', 0, ''),
(29, 6, 13, 0, 100.00, 'chicken', 500.00, 'venkat', '2014-12-13 05:49:33', 0, ''),
(28, 6, 14, 0, 100.00, 'golden', 20.00, 'venkat', '2014-12-13 02:44:57', 0, ''),
(34, 22, 18, 0, 6.00, 'COOPENS', 20.00, 'law', '2014-12-15 07:57:14', 0, ''),
(27, 6, 6, 0, 10.00, 'golden', 1.00, 'venkat', '2014-12-12 05:55:12', 0, ''),
(35, 22, 18, 0, 6.00, 'COOPENS', 1.00, 'law', '2014-12-19 08:46:45', 0, ''),
(36, 22, 18, 0, 6.00, 'COOPENS', 1.00, 'law', '2014-12-19 08:52:21', 0, ''),
(33, 17, 10, 0, 55.00, 'NEW', 34.00, 'madhan', '2014-12-15 01:43:13', 0, ''),
(26, 6, 7, 0, 100.00, 'chicken', 10.00, 'venkat', '2014-12-02 05:16:40', 0, ''),
(40, 38, 22, 0, 8.00, 'BB Brand', 5.00, 'kannan', '2014-12-23 00:06:34', 0, ''),
(41, 38, 22, 0, 7.00, 'NEW GH Brand', 50.00, 'kannan', '2014-12-23 08:27:54', 0, ''),
(42, 6, 7, 0, 10.00, 'chicken', 2.00, 'venkat', '2015-01-14 05:55:32', 0, ''),
(43, 6, 7, 0, 100.00, 'chicken', 2.00, 'venkat', '2015-01-14 05:55:58', 0, ''),
(44, 6, 7, 0, 100.00, 'chicken', 2.00, 'venkat', '2015-01-14 05:59:12', 0, ''),
(45, 6, 7, 0, 100.00, 'chicken', 2.00, 'venkat', '2015-01-14 06:00:06', 0, ''),
(46, 6, 7, 0, 100.00, 'chicken', 2.00, 'venkat', '2015-01-14 06:07:09', 0, ''),
(47, 6, 7, 0, 10.00, 'golden', 10.00, 'venkat', '2015-01-14 06:07:26', 0, ''),
(48, 6, 7, 0, 100.00, 'chicken', 2.00, 'venkat', '2015-01-14 06:08:13', 0, ''),
(49, 6, 7, 0, 100.00, 'chicken', 2.00, 'venkat', '2015-01-14 06:18:41', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catfishgenpurchased`
--

CREATE TABLE IF NOT EXISTS `tbl_catfishgenpurchased` (
  `cgpid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `amount` float(9,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`cgpid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_catfishgenpurchased`
--

INSERT INTO `tbl_catfishgenpurchased` (`cgpid`, `uid`, `date`, `description`, `amount`, `user_type`, `staff_id`, `created_by`, `created_date`) VALUES
(11, 38, '16/12/2014', 'Stationaries', 300.00, 0, 0, 'kannan', '2014-12-23 08:48:07'),
(10, 6, '01/11/2014', '1234', 10000.00, 0, 0, 'venkat', '2014-11-15 06:08:59'),
(9, 38, '12/12/2014', 'purchase', 900.00, 0, 0, 'kannan', '2014-12-23 08:48:14'),
(6, 6, '11/11/2014', 'This is test', 500.00, 0, 0, 'venkat', '2014-12-11 08:44:08'),
(12, 38, '16/12/2014', 'Hardware Expenses', 140.00, 0, 0, 'kannan', '2014-12-23 08:47:59'),
(13, 38, '09/12/2014', 'ABC expenses', 800.00, 0, 0, 'kannan', '2014-12-23 08:47:51'),
(14, 38, '23/12/2014', 'Hardware Expenses', 900.00, 0, 0, 'kannan', '2014-12-23 08:28:32'),
(15, 38, '23/12/2014', 'ABC expenses', 800.00, 0, 0, 'kannan', '2014-12-23 08:49:17'),
(16, 38, '18/11/2014', 'Test Exp', 900.00, 0, 0, 'kannan', '2014-12-24 07:11:37'),
(17, 38, '11/11/2014', 'exp', 180.00, 0, 0, 'kannan', '2014-12-26 04:58:44'),
(18, 22, '01/01/2015', 'Drugs', 1.00, 0, 0, 'law', '2015-01-12 10:55:39'),
(19, 22, '08/01/2015', 'Drugs', 10000.00, 0, 0, 'law', '2015-01-13 03:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catfishincome`
--

CREATE TABLE IF NOT EXISTS `tbl_catfishincome` (
  `ciid` int(11) NOT NULL AUTO_INCREMENT,
  `csid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `pond_name` int(11) NOT NULL,
  `no_of_fish` int(11) NOT NULL,
  `total_cost` float(9,2) NOT NULL,
  `income_type` varchar(50) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  PRIMARY KEY (`ciid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `tbl_catfishincome`
--

INSERT INTO `tbl_catfishincome` (`ciid`, `csid`, `uid`, `pond_name`, `no_of_fish`, `total_cost`, `income_type`, `user_type`, `staff_id`, `created_by`, `created_date`) VALUES
(15, 29, 38, 22, 800, 8000.00, 'sales', 0, 0, 'kannan', '12/12/2014'),
(16, 30, 38, 23, 500, 6000.00, 'sales', 0, 0, 'kannan', '12/12/2014'),
(17, 31, 38, 22, 80, 7000.00, 'sales', 0, 0, 'kannan', '12/12/2014'),
(18, 32, 6, 7, 10, 333.00, 'sales', 0, 0, 'venkat', '12/12/2014'),
(19, 33, 6, 6, 10, 33.00, 'sales', 0, 0, 'venkat', '11/12/2014'),
(20, 34, 6, 6, 10, 33.00, 'sales', 0, 0, 'venkat', '11/12/2014'),
(21, 35, 6, 7, 1000, 33.00, 'sales', 0, 0, 'venkat', '01/12/2014'),
(27, 41, 6, 6, 416, 123.00, 'sales', 0, 0, 'venkat', '04/12/2014'),
(23, 37, 27, 16, 100, 12.00, 'sales', 0, 0, 'subburaj', '2014-12-15 06:30:44'),
(29, 43, 38, 22, 90, 900.00, 'sales', 0, 0, 'kannan', '19/11/2014'),
(28, 42, 38, 22, 60, 8000.00, 'sales', 0, 0, 'kannan', '23/12/2014'),
(30, 44, 6, 7, 100, 10001.00, 'sales', 0, 0, 'venkat', '23/12/2014'),
(31, 45, 38, 23, 15, 18000.00, 'sales', 0, 0, 'kannan', '11/11/2014'),
(32, 46, 38, 22, 800, 8000.00, 'sales', 0, 0, 'kannan', '10/11/2014'),
(33, 47, 38, 22, 79, 7800.00, 'sales', 0, 0, 'kannan', '01/11/2014'),
(34, 48, 38, 23, 90, 19000.00, 'sales', 0, 0, 'kannan', '10/11/2014'),
(35, 49, 22, 18, 20, 10000.00, 'sales', 0, 0, 'law', '2015-01-12 11:02:35'),
(36, 50, 6, 7, 100, 10001.00, 'sales', 0, 0, 'venkat', '14/01/2015'),
(37, 51, 6, 7, 100, 10001.00, 'sales', 0, 0, 'venkat', '23/01/2015'),
(38, 52, 6, 7, 100, 10001.00, 'sales', 0, 0, 'venkat', '13/01/2015'),
(39, 53, 6, 7, 100, 10001.00, 'sales', 0, 0, 'venkat', '13/01/2015'),
(40, 54, 38, 22, 700, 18.00, 'sales', 0, 0, 'kannan', '14/01/2015'),
(41, 55, 38, 22, 400, 18000.00, 'sales', 0, 0, 'kannan', '12/12/2014');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catfishsales`
--

CREATE TABLE IF NOT EXISTS `tbl_catfishsales` (
  `csid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `pond_name` int(11) NOT NULL,
  `no_of_fish` int(11) NOT NULL,
  `total_weight` float(9,2) NOT NULL,
  `total_cost` float(9,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`csid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `tbl_catfishsales`
--

INSERT INTO `tbl_catfishsales` (`csid`, `uid`, `date`, `pond_name`, `no_of_fish`, `total_weight`, `total_cost`, `user_type`, `staff_id`, `created_by`, `created_date`) VALUES
(37, 27, '15/12/2014', 16, 100, 1000.00, 12.00, 0, 0, 'subburaj', '2014-12-15 06:30:44'),
(41, 6, '04/12/2014', 6, 416, 654.00, 123.00, 0, 0, 'venkat', '2015-01-13 09:24:11'),
(35, 6, '01/12/2014', 7, 1000, 10.00, 33.00, 0, 0, 'venkat', '2015-01-13 09:24:18'),
(34, 6, '11/12/2014', 6, 10, 10.00, 33.00, 0, 0, 'venkat', '2015-01-13 09:25:08'),
(29, 38, '12/12/2014', 22, 800, 900.00, 8000.00, 0, 0, 'kannan', '2015-01-14 00:18:57'),
(30, 38, '12/12/2014', 23, 500, 600.00, 6000.00, 0, 0, 'kannan', '2015-01-14 00:18:49'),
(31, 38, '12/12/2014', 22, 80, 700.00, 7000.00, 0, 0, 'kannan', '2015-01-14 00:18:45'),
(32, 6, '12/12/2014', 7, 10, 100.00, 333.00, 0, 0, 'venkat', '2015-01-13 09:24:26'),
(33, 6, '11/12/2014', 6, 10, 10.00, 33.00, 0, 0, 'venkat', '2015-01-13 09:25:04'),
(42, 38, '23/12/2014', 22, 60, 800.00, 8000.00, 0, 0, 'kannan', '2015-01-14 00:18:39'),
(43, 38, '19/11/2014', 22, 90, 90.00, 900.00, 0, 0, 'kannan', '2015-01-14 00:18:34'),
(44, 6, '23/12/2014', 7, 100, 100.00, 10001.00, 0, 0, 'venkat', '2015-01-13 09:24:06'),
(45, 38, '11/11/2014', 23, 15, 17.00, 18000.00, 0, 0, 'kannan', '2015-01-14 00:18:29'),
(46, 38, '10/11/2014', 22, 800, 900.00, 8000.00, 0, 0, 'kannan', '2015-01-14 00:18:19'),
(47, 38, '01/11/2014', 22, 79, 70.00, 7800.00, 0, 0, 'kannan', '2015-01-14 00:18:15'),
(48, 38, '10/11/2014', 23, 90, 90.00, 19000.00, 0, 0, 'kannan', '2015-01-14 00:18:11'),
(49, 22, '01/01/2015', 18, 20, 25.00, 10000.00, 0, 0, 'law', '2015-01-12 11:02:35'),
(50, 6, '14/01/2015', 7, 100, 100.00, 10001.00, 0, 0, 'venkat', '2015-01-13 09:24:01'),
(51, 6, '23/01/2015', 7, 100, 100.00, 10001.00, 0, 0, 'venkat', '2015-01-13 09:23:55'),
(52, 6, '13/01/2015', 7, 100, 100.00, 10001.00, 0, 0, 'venkat', '2015-01-13 09:23:41'),
(53, 6, '13/01/2015', 7, 100, 10.00, 10001.00, 0, 0, 'venkat', '2015-01-13 09:31:52'),
(54, 38, '14/01/2015', 22, 700, 700.00, 18.00, 0, 0, 'kannan', '2015-01-14 00:16:53'),
(55, 38, '12/12/2014', 22, 400, 400.00, 18000.00, 0, 0, 'kannan', '2015-01-14 00:17:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catfishweight`
--

CREATE TABLE IF NOT EXISTS `tbl_catfishweight` (
  `cwid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `scid` int(11) NOT NULL,
  `avg_weight` float(9,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`cwid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_catfishweight`
--

INSERT INTO `tbl_catfishweight` (`cwid`, `uid`, `scid`, `avg_weight`, `user_type`, `created_by`, `created_date`, `staff_id`) VALUES
(2, 6, 7, 12.00, 0, 'venkat', '2014-12-02 02:12:21', 0),
(3, 6, 6, 12.00, 0, 'venkat', '2014-12-02 02:20:06', 0),
(4, 17, 8, 45.00, 0, 'madhan', '2014-12-02 05:45:14', 0),
(5, 17, 8, 23.00, 0, 'madhan', '2014-12-02 05:47:51', 0),
(6, 17, 11, 13.00, 0, 'madhan', '2014-12-02 06:59:49', 0),
(7, 17, 10, 34.00, 0, 'madhan', '2014-12-03 00:24:57', 0),
(8, 17, 12, 34.00, 0, 'madhan', '2014-12-04 02:43:36', 0),
(9, 22, 17, 1.20, 0, 'law', '2014-12-08 12:02:22', 0),
(10, 22, 19, 1.00, 0, 'law', '2014-12-08 12:02:35', 0),
(11, 22, 21, 12.00, 0, 'law', '2014-12-08 12:02:53', 0),
(13, 38, 22, 200.00, 0, 'kannan', '2014-12-10 08:09:29', 0),
(14, 6, 6, 50.00, 0, 'venkat', '2014-12-13 00:50:51', 0),
(15, 6, 14, 100.00, 0, 'venkat', '2014-12-13 02:43:35', 0),
(16, 6, 14, 200.00, 0, 'venkat', '2014-12-13 02:49:52', 0),
(17, 38, 23, 50.00, 0, 'kannan', '2014-12-13 04:51:04', 0),
(18, 38, 22, 40.00, 0, 'kannan', '2014-12-13 04:51:21', 0),
(19, 38, 23, 46.00, 0, 'kannan', '2014-12-13 04:51:29', 0),
(20, 38, 22, 23.00, 0, 'kannan', '2014-12-13 04:51:37', 0),
(21, 38, 23, 14.00, 0, 'kannan', '2014-12-13 05:04:37', 0),
(22, 22, 18, 12.00, 0, 'law', '2014-12-19 08:54:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catfish_mortality`
--

CREATE TABLE IF NOT EXISTS `tbl_catfish_mortality` (
  `cmid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `scid` int(11) NOT NULL,
  `mortality` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`cmid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `tbl_catfish_mortality`
--

INSERT INTO `tbl_catfish_mortality` (`cmid`, `uid`, `scid`, `mortality`, `user_type`, `created_by`, `created_date`, `staff_id`) VALUES
(23, 17, 11, 5, 0, 'madhan', '2014-12-03 02:41:02', 0),
(24, 17, 10, 2, 0, 'madhan', '2014-12-02 23:33:50', 0),
(25, 6, 13, 2, 0, 'venkat', '2014-12-04 02:20:31', 0),
(26, 22, 17, 2, 0, 'law', '2014-12-08 12:03:38', 0),
(27, 22, 18, 5, 0, 'law', '2014-12-08 12:03:49', 0),
(28, 22, 17, 2, 0, 'law', '2014-12-09 09:20:52', 0),
(29, 38, 22, 5, 0, 'kannan', '2014-12-10 23:32:14', 0),
(30, 38, 23, 4, 0, 'kannan', '2014-12-10 23:33:07', 0),
(31, 38, 23, 5, 0, 'kannan', '2014-12-12 07:33:57', 0),
(32, 6, 7, 12, 0, 'venkat', '2014-12-12 09:03:19', 0),
(33, 6, 7, 50, 0, 'venkat', '2014-12-12 09:03:38', 0),
(34, 6, 7, 34, 0, 'venkat', '2014-12-12 09:04:13', 0),
(35, 6, 7, 89, 0, 'venkat', '2014-12-17 05:33:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chickpurchased`
--

CREATE TABLE IF NOT EXISTS `tbl_chickpurchased` (
  `ckpid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `pen_name` int(11) NOT NULL,
  `total_number` int(11) NOT NULL,
  `total_cost` float(9,2) NOT NULL,
  `avg_weight` float(9,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`ckpid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_chickpurchased`
--

INSERT INTO `tbl_chickpurchased` (`ckpid`, `uid`, `date`, `pen_name`, `total_number`, `total_cost`, `avg_weight`, `user_type`, `staff_id`, `created_by`, `created_date`) VALUES
(12, 6, '22/12/2014', 28, 10, 1000.00, 100.00, 0, 0, 'venkat', '2014-12-24 06:14:56'),
(11, 38, '01/12/2014', 27, 10, 1000.00, 1000.00, 0, 0, 'kannan', '2014-12-24 06:10:36'),
(10, 38, '24/12/2014', 27, 23, 3444.00, 50.00, 0, 0, 'kannan', '2014-12-24 06:02:35'),
(13, 38, '18/11/2014', 27, 80, 80.00, 800.00, 0, 0, 'kannan', '2014-12-24 07:12:25'),
(14, 6, '26/12/2014', 30, 50, 100.00, 100.00, 0, 0, 'venkat', '2014-12-26 00:52:24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feedpurchased`
--

CREATE TABLE IF NOT EXISTS `tbl_feedpurchased` (
  `fpid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `no_of_bags` int(11) NOT NULL,
  `size` float(9,2) NOT NULL,
  `weight` float(9,2) NOT NULL,
  `avl_weight` float(9,2) NOT NULL,
  `brand` varchar(50) NOT NULL,
  `total_cost` float(9,2) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  PRIMARY KEY (`fpid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tbl_feedpurchased`
--

INSERT INTO `tbl_feedpurchased` (`fpid`, `uid`, `no_of_bags`, `size`, `weight`, `avl_weight`, `brand`, `total_cost`, `staff_id`, `created_date`, `created_by`) VALUES
(5, 17, 60, 55.00, 500.00, 500.00, 'NEW', 500.00, 0, '2014-12-05 23:53:19', 'madhan'),
(3, 17, 50, 45.00, 50.00, 50.00, 'NEW', 500.00, 0, '2014-12-04 01:59:04', 'madhan'),
(15, 38, 70, 8.00, 800.00, 800.00, 'BB Brand', 500.00, 0, '2014-12-23 08:49:45', 'kannan'),
(6, 22, 5, 6.00, 15.00, 15.00, 'COOPENS', 45000.00, 0, '2014-12-09 07:22:58', 'law'),
(8, 38, 60, 9.00, 80.00, 80.00, 'NEW Brand', 700.00, 0, '2015-01-19 04:59:00', 'kannan'),
(12, 6, 20, 100.00, 100.00, 90.00, 'chicken', 400.00, 0, '2014-12-12 05:13:58', 'venkat'),
(13, 6, 20, 10.00, 10.00, 0.00, 'golden', 500.00, 0, '2015-01-14 01:36:38', 'venkat'),
(16, 38, 10, 7.00, 100.00, 100.00, 'NEW GH Brand', 900.00, 0, '2014-12-23 08:27:03', 'kannan'),
(17, 6, 20, 10.00, 10.00, 0.00, 'golden', 500.00, 0, '2015-01-14 01:30:59', 'venkat'),
(18, 6, 20, 10.00, 10.00, 6.00, 'golden', 500.00, 0, '2015-01-14 01:31:15', 'venkat');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fingerlingpurchased`
--

CREATE TABLE IF NOT EXISTS `tbl_fingerlingpurchased` (
  `flpid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `pond_name` int(11) NOT NULL,
  `total_number` float(9,2) NOT NULL,
  `total_cost` float(9,2) NOT NULL,
  `avg_weight` float(9,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  PRIMARY KEY (`flpid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_fingerlingpurchased`
--

INSERT INTO `tbl_fingerlingpurchased` (`flpid`, `uid`, `date`, `pond_name`, `total_number`, `total_cost`, `avg_weight`, `user_type`, `staff_id`, `created_date`, `created_by`) VALUES
(12, 38, '16/12/2014', 22, 80.00, 800.00, 80.00, 0, 0, '2014-12-22 23:42:14', 'kannan'),
(11, 38, '10/12/2014', 23, 80.00, 900.00, 90.00, 0, 0, '2014-12-22 08:59:30', 'kannan'),
(10, 6, '22/12/2014', 7, 10.00, 500.00, 100.00, 0, 0, '2014-12-22 01:50:50', 'venkat'),
(13, 38, '23/12/2014', 22, 80.00, 80.00, 80.00, 0, 0, '2014-12-23 08:43:10', 'kannan'),
(14, 38, '23/12/2014', 23, 80.00, 800.00, 60.00, 0, 0, '2014-12-23 08:49:58', 'kannan'),
(15, 38, '18/11/2014', 22, 100.00, 1000.00, 100.00, 0, 0, '2014-12-24 07:12:02', 'kannan'),
(16, 38, '16/12/2014', 22, 50.00, 50.00, 50.00, 0, 0, '2014-12-26 00:09:56', 'kannan'),
(17, 22, '12/01/2015', 18, 50.00, 1500.00, 1.00, 0, 0, '2015-01-12 05:35:52', 'law'),
(18, 22, '13/01/2015', 18, 400.00, 12000.00, 4.00, 0, 0, '2015-01-13 03:18:03', 'law'),
(19, 38, '09/12/2014', 23, 800.00, 802.00, 80.00, 0, 0, '2015-01-13 09:23:57', 'kannan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_litterchange`
--

CREATE TABLE IF NOT EXISTS `tbl_litterchange` (
  `tlbid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sbid` int(11) NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`tlbid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_litterchange`
--

INSERT INTO `tbl_litterchange` (`tlbid`, `uid`, `sbid`, `user_type`, `created_by`, `created_date`, `staff_id`) VALUES
(19, 6, 30, 0, 'venkat', '2014-12-15 02:38:19', 0),
(15, 17, 18, 0, 'madhan', '2014-12-02 06:47:21', 0),
(8, 6, 20, 0, 'venkat', '2014-12-04 01:53:28', 0),
(17, 38, 27, 0, 'kannan', '2014-12-13 01:59:24', 0),
(18, 6, 28, 0, 'venkat', '2014-12-15 02:31:11', 0),
(16, 22, 23, 0, 'law', '2014-12-09 09:45:19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_livestock_broiler`
--

CREATE TABLE IF NOT EXISTS `tbl_livestock_broiler` (
  `lbid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sbid` int(11) NOT NULL,
  `stock_density` float(9,2) NOT NULL,
  `total_stocked` float(9,2) NOT NULL,
  `avg_weight` float(9,2) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  `mortality_addition` int(11) NOT NULL,
  PRIMARY KEY (`lbid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `tbl_livestock_broiler`
--

INSERT INTO `tbl_livestock_broiler` (`lbid`, `uid`, `sbid`, `stock_density`, `total_stocked`, `avg_weight`, `created_by`, `created_date`, `staff_id`, `mortality_addition`) VALUES
(25, 17, 14, 56.00, 50.00, 15.00, 'madhan', '2014-11-24 07:05:15', 0, 0),
(24, 17, 14, 99.00, 89.00, 99.00, 'madhan', '2014-11-25 07:28:02', 0, 0),
(22, 6, 8, 250.00, 150.00, 5.00, 'venkat', '2014-11-22 00:23:54', 0, 0),
(23, 17, 14, 120.00, 120.00, 120.00, 'madhan', '2014-11-24 02:32:07', 0, 0),
(29, 17, 21, 100.00, 100.00, 100.00, 'madhan', '2014-12-03 00:00:00', 0, 8),
(27, 17, 18, 89.00, 40.00, 8.00, 'madhan', '2014-12-02 07:56:31', 0, 1),
(31, 38, 25, 400.00, 400.00, 28.00, 'kannan', '2014-12-10 08:23:57', 0, 0),
(30, 22, 23, 12.00, 200.00, 12.00, 'law', '2014-12-09 09:35:19', 0, 12),
(34, 6, 26, 123.00, 11.00, 100.00, 'venkat', '2014-12-10 23:56:31', 0, 0),
(43, 38, 27, 10000.00, 10000.00, 10000.00, 'kannan', '2015-01-14 00:28:12', 0, 3),
(41, 6, 28, 1000.00, 1000.00, 1000.00, 'venkat', '2014-12-11 06:20:01', 0, 0),
(47, 6, 30, 20.00, 500.00, 50.00, 'venkat', '2014-12-26 01:34:31', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_livestock_catfish`
--

CREATE TABLE IF NOT EXISTS `tbl_livestock_catfish` (
  `lcid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `scid` int(11) NOT NULL,
  `stock_density` float(9,2) NOT NULL,
  `total_stocked` int(11) NOT NULL,
  `avg_weight` float(9,2) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  `stock_addition` int(11) NOT NULL,
  `stock_deletion` int(11) NOT NULL,
  `weight_addition` int(11) NOT NULL,
  `weight_deletion` int(11) NOT NULL,
  `mortality_addition` int(11) NOT NULL,
  PRIMARY KEY (`lcid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `tbl_livestock_catfish`
--

INSERT INTO `tbl_livestock_catfish` (`lcid`, `uid`, `scid`, `stock_density`, `total_stocked`, `avg_weight`, `created_by`, `created_date`, `staff_id`, `stock_addition`, `stock_deletion`, `weight_addition`, `weight_deletion`, `mortality_addition`) VALUES
(21, 6, 7, 1000.00, 1000, 4000.00, 'venkat', '2014-12-11 06:33:33', 0, 100, 10, 0, 10, 185),
(2, 6, 6, 350.00, 700, 80.00, 'venkat', '2014-11-28 07:17:40', 0, 499810, 0, 938, 0, 0),
(24, 6, 11, 20.00, 100, 100.00, 'venkat', '2014-12-26 01:36:34', 0, 0, 0, 0, 0, 0),
(4, 17, 10, 89.00, 80, 88.00, 'madhan', '2014-12-02 06:24:19', 0, 23, 0, 22, 0, 2),
(25, 38, 135, 800.00, 600, 600.00, 'kannan', '2015-01-14 02:32:48', 0, 0, 0, 0, 0, 0),
(5, 17, 12, 40.00, 50, 35.00, 'madhan', '2014-12-03 01:36:31', 0, 0, 0, 0, 0, 0),
(6, 6, 13, 70.00, 100, 150.00, 'venkat', '2014-12-04 02:12:25', 0, 0, 0, 0, 0, 2),
(7, 6, 14, 70.00, 1000, 150.00, 'venkat', '2014-12-11 06:33:07', 0, 0, 0, 0, 0, 0),
(20, 38, 23, 10000.00, 800, 8000.00, 'kannan', '2014-12-23 00:28:20', 0, 7, 21, 21, 56, 5),
(9, 22, 17, 10.00, 100, 20.00, 'law', '2014-12-08 11:51:11', 0, 12, 71, 56, 15, 4),
(10, 22, 18, 12.00, 100, 2.00, 'law', '2014-12-08 11:51:32', 0, 71, 13, 15, 11, 5),
(11, 22, 19, 13.00, 1222, 12.00, 'law', '2014-12-08 11:52:58', 0, 13, 12, 11, 56, 0),
(12, 22, 21, 12.00, 322, 12.00, 'law', '2014-12-08 11:53:56', 0, 0, 0, 0, 0, 0),
(16, 27, 16, 32.00, 500, 33.00, 'subburaj', '2014-12-10 23:26:39', 0, 0, 10, 0, 10, 0),
(17, 38, 22, 650.00, 900, 400.00, 'kannan', '2014-12-30 01:18:16', 0, 21, 7, 56, 21, 0),
(22, 27, 134, 100.00, 100, 100.00, 'subburaj', '2014-12-15 06:36:00', 0, 10, 0, 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_salaries`
--

CREATE TABLE IF NOT EXISTS `tbl_salaries` (
  `ssid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `staff` int(11) NOT NULL,
  `amount` float(9,2) NOT NULL,
  `user_type` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`ssid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `tbl_salaries`
--

INSERT INTO `tbl_salaries` (`ssid`, `uid`, `date`, `staff`, `amount`, `user_type`, `staff_id`, `created_by`, `created_date`) VALUES
(11, 6, '23/11/2014', 25, 5000.50, 0, 0, 'venkat', '2015-01-13 07:50:39'),
(16, 38, '24/12/2014', 45, 2500.00, 0, 0, 'kannan', '2014-12-24 06:18:48'),
(15, 6, '23/12/2014', 25, 10001.00, 0, 0, 'venkat', '2015-01-13 07:49:53'),
(14, 38, '24/12/2014', 44, 5000.00, 0, 0, 'kannan', '2014-12-24 06:07:38'),
(6, 17, '15/12/2014', 21, 6777.00, 0, 0, 'madhan', '2014-12-15 05:28:01'),
(18, 38, '11/11/2014', 44, 5000.00, 0, 0, 'kannan', '2014-12-26 04:56:46'),
(17, 38, '11/11/2014', 45, 400.00, 0, 0, 'kannan', '2014-12-26 00:01:12'),
(12, 6, '01/12/2014', 25, 5000.50, 0, 0, 'venkat', '2015-01-13 07:50:31'),
(19, 22, '01/01/2015', 48, 24000.00, 0, 0, 'law', '2015-01-12 11:04:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sorting`
--

CREATE TABLE IF NOT EXISTS `tbl_sorting` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `from_scid` int(11) NOT NULL,
  `to_scid` int(11) NOT NULL,
  `no_of_fish` int(11) NOT NULL,
  `total_weight` float(9,2) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `tbl_sorting`
--

INSERT INTO `tbl_sorting` (`sid`, `uid`, `from_scid`, `to_scid`, `no_of_fish`, `total_weight`, `created_by`, `created_date`, `staff_id`, `user_type`) VALUES
(6, 17, 11, 10, 8, 6.00, 'madhan', '2014-12-02 06:40:53', 0, 0),
(26, 27, 16, 134, 10, 10.00, 'subburaj', '2014-12-15 06:37:31', 0, 0),
(5, 17, 11, 10, 15, 16.00, 'madhan', '2014-12-02 06:34:07', 0, 0),
(25, 38, 23, 22, 21, 56.00, 'kannan', '2015-01-19 04:58:24', 0, 0),
(24, 6, 7, 6, 10, 10.00, 'venkat', '2014-12-12 09:00:01', 0, 0),
(11, 22, 17, 18, 21, 10.00, 'law', '2014-12-08 12:01:24', 0, 0),
(12, 22, 18, 19, 13, 11.00, 'law', '2014-12-08 12:01:44', 0, 0),
(13, 22, 19, 17, 12, 56.00, 'law', '2014-12-09 08:00:29', 0, 0),
(23, 6, 7, 11, 10, 10.00, 'venkat', '2014-12-11 01:06:31', 0, 0),
(22, 38, 22, 23, 10, 25.00, 'kannan', '2014-12-10 23:31:26', 0, 0),
(21, 6, 11, 6, 10, 30.00, 'venkat', '2014-12-10 23:22:49', 0, 0),
(27, 38, 22, 23, 7, 21.00, 'kannan', '2014-12-18 05:24:50', 0, 0),
(28, 22, 17, 18, 50, 5.00, 'law', '2015-01-12 06:02:10', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_structure`
--

CREATE TABLE IF NOT EXISTS `tbl_structure` (
  `fuid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `dimension` float(9,2) NOT NULL,
  `density` float(9,2) NOT NULL,
  PRIMARY KEY (`fuid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_structure_broiler`
--

CREATE TABLE IF NOT EXISTS `tbl_structure_broiler` (
  `sbid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `pen_name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `size` float(9,2) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`sbid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `tbl_structure_broiler`
--

INSERT INTO `tbl_structure_broiler` (`sbid`, `uid`, `pen_name`, `type`, `size`, `created_by`, `created_date`) VALUES
(27, 38, 'NEW BROILER PEN', 'Litter', 500.00, 'kannan', '2014-12-10 09:03:45'),
(28, 6, 'chicken', 'Litter', 123.00, 'venkat', '2014-12-11 00:25:26'),
(18, 17, 'Broiler New', 'Free range', 15.00, 'madhan', '2014-12-02 23:48:27'),
(23, 22, '1', 'Litter', 200.00, 'law', '2014-12-09 09:24:58'),
(21, 17, 'Broiler Litter', 'Litter', 45.00, 'madhan', '2014-12-02 23:48:57'),
(30, 6, 'Peice', 'Litter', 123.00, 'venkat', '2014-12-11 02:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_structure_catfish`
--

CREATE TABLE IF NOT EXISTS `tbl_structure_catfish` (
  `scid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `pond_name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `size` float(9,2) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  PRIMARY KEY (`scid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=136 ;

--
-- Dumping data for table `tbl_structure_catfish`
--

INSERT INTO `tbl_structure_catfish` (`scid`, `uid`, `pond_name`, `type`, `size`, `created_date`, `created_by`) VALUES
(13, 17, 'Pond Fish', 'Fiber Glass', 66.00, '2014-12-02 23:44:26', 'madhan'),
(10, 17, 'Fish Pond', 'Earthen', 66.00, '2014-12-02 23:42:29', 'madhan'),
(7, 6, 'colorfish', 'Fiber Glass', 66.00, '2014-11-25 09:04:26', 'venkat'),
(6, 6, 'fish', 'Fiber Glass', 66.00, '2014-11-25 07:37:01', 'venkat'),
(12, 17, 'New Pond', 'Plastic', 90.00, '2014-12-03 01:33:53', 'madhan'),
(11, 6, 'goldenfish', 'Fiber Glass', 100.00, '2014-12-04 02:10:46', 'venkat'),
(14, 6, 'jellybean', 'Fiber Glass', 100.00, '2014-12-04 02:11:15', 'venkat'),
(17, 22, 'A', 'Earthen', 10.00, '2014-12-08 11:41:47', 'law'),
(16, 27, 'ancy', 'Fiber Glass', 500.00, '2014-12-05 09:01:11', 'subburaj'),
(18, 22, 'B', 'Block', 24.00, '2014-12-08 11:42:39', 'law'),
(19, 22, 'C', 'Fiber Glass', 10.00, '2014-12-08 11:44:26', 'law'),
(20, 22, 'd', 'Plastic', 13.00, '2014-12-08 11:44:47', 'law'),
(21, 22, 'E', 'Block', 12.00, '2014-12-08 11:49:00', 'law'),
(22, 38, 'NEW FISH POND', 'Fiber Glass', 200.00, '2014-12-10 07:37:44', 'kannan'),
(23, 38, 'OLD FISH POND', 'Block', 400.00, '2014-12-10 07:42:03', 'kannan'),
(134, 27, 'subbu', 'Earthen', 500.00, '2014-12-11 01:04:19', 'subburaj'),
(135, 38, 'NEW POND ABC', 'Earthen', 700.00, '2015-01-14 01:05:14', 'kannan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) NOT NULL,
  `farmname` varchar(32) NOT NULL,
  `username` varchar(150) NOT NULL,
  `email` varchar(128) NOT NULL,
  `email_subscribe` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address1` varchar(250) NOT NULL,
  `marital_status` tinyint(4) NOT NULL,
  `verify_code` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-deactive,1-active',
  `livestock` varchar(50) NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `parent_uid` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `firstname`, `farmname`, `username`, `email`, `email_subscribe`, `phone`, `password`, `gender`, `address1`, `marital_status`, `verify_code`, `status`, `livestock`, `user_type`, `parent_uid`) VALUES
(24, 'teststaff', '', 'teststaff', '', 0, '', '0', '', '', 0, '', 1, '1,2', 1, 6),
(4, 'karthi', 'keyan', 'Karthick', 'karthickepub@gmail.com', 0, '9940988054', 'f3f2a2ee2d487f1a979cd371b79e09f46de076b6', '0', 'RJPM', 0, '', 1, '0', 0, 0),
(6, 'adminvenkat', '', 'venkat', 'techvenkats@gmail.com', 0, '8015725019', 'ee55e22c0eea2b952387a50eaf617bf11959ebd8', '0', '', 0, '', 1, '1,2', 0, 0),
(7, 'muthu', '', 'muthu', 'muthukumar@ancyinfotech.com', 0, '3333333333', '7c222fb2927d828af22f592134e8932480637c0d', '0', '', 0, '', 1, '0', 0, 0),
(21, 'staff', '', 'venkatstaff', '', 0, '', '7c222fb2927d828af22f592134e8932480637c0d', '', '', 0, '', 1, '1', 1, 6),
(9, 'Law', '', 'lawone', 'bokpokiri@hotmail.com', 0, '08037058714', '41251ec281c8997083209035260e08562d417376', '0', '', 0, '', 1, '1', 0, 0),
(22, 'w', 'ert', 'law', 'b@jd.com', 0, '1234567890', '7c222fb2927d828af22f592134e8932480637c0d', '0', 'afererte', 0, '', 1, '1', 0, 0),
(12, 'Lawrence', 'Test', 'Lawrence', 'lawrence@gmail.com', 0, '5858585858', 'ec59150f25791f1ad8355969136357d03b054542', '0', 'Nigeria', 0, '', 1, '1', 0, 0),
(17, 'Madhan', 'Ancy Farm', 'madhan', 'test@test.com', 0, '3434343434', 'd13149de00848eb013cad318d27829db64b965d7', '0', '455', 0, '', 1, '1', 0, 0),
(25, 'subbu', '', 'subbu', '', 0, '', '969dff52f87483693e22164cf558e45de4cc918c', '', '', 0, '', 1, '1', 1, 6),
(26, 'sdfadf', 'asdfasd', 'fsadf', 'sadfasd@dfasd.sfas', 0, '1234567844', '7c222fb2927d828af22f592134e8932480637c0d', '0', 'sfdfasdfas', 0, '', 1, '1', 0, 0),
(27, 'subburaj', 'subburaj', 'subburaj', 'subburaj6587@gmail.com', 0, '9698898778', '969dff52f87483693e22164cf558e45de4cc918c', '0', 'subburaj', 0, '', 1, '1', 0, 0),
(28, 'B', '', 'Bie', 'b@mail.com', 0, '12345567789', '7c222fb2927d828af22f592134e8932480637c0d', '0', '', 0, '', 1, '1', 0, 0),
(37, 'test', 'testfarm', 'test', 'techvenkat@gmail.com', 0, '9876543210', '7c222fb2927d828af22f592134e8932480637c0d', '0', '', 0, '', 1, '2', 0, 0),
(31, 'marichamy', 'Test Farm', 'marichami', 'm.madhanakrishnan@gmail.com', 0, '5454546466', 'd13149de00848eb013cad318d27829db64b965d7', '0', '789', 0, '', 1, '1', 0, 0),
(38, 'kannan', 'Test Farm', 'kannan', 'm.madhanakrishnan@yahoo.com', 0, '43453545454', 'd13149de00848eb013cad318d27829db64b965d7', '0', '567', 0, '', 1, '1,2', 0, 0),
(39, 'Lawrence', '', 'bin', 'biebele@gmail.com', 0, '08037058714', '7c222fb2927d828af22f592134e8932480637c0d', '0', '', 0, '', 1, '2', 0, 0),
(40, 'Lawrence', '', 'all', 'b@67.com', 0, '08037058714', '7c222fb2927d828af22f592134e8932480637c0d', '0', '', 0, '', 1, '1,2', 0, 0),
(41, 'stanley', 'mickgels farm', 'stanleynjoku', 'stannjo84@yahoo.co.uk', 0, '08185042776', '5181f38659c43d8e13d3ed362ba47e00608e452e', '0', 'Papals ground kubwa abuja', 0, '', 1, '1,2', 0, 0),
(45, 'ABC Staff', '', 'abc', '', 0, '', 'd13149de00848eb013cad318d27829db64b965d7', '', '', 0, '', 1, '1,2', 1, 38),
(44, 'New Staff', '', 'newstaff', '', 0, '', 'd13149de00848eb013cad318d27829db64b965d7', '', '', 0, '', 1, '1,2', 1, 38),
(46, 'X', 'X', 'X', 'x@odogwu.com', 0, '08033357789', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '0', 'X', 0, '', 1, '1', 0, 0),
(47, 'odogwu', 'ododgwu', 'odogwu', '1@odogwu.com', 0, '08000303030', '937eae110631479713e8d5d879077b3403e6d646', '0', 'oifkff', 0, '', 1, '1,2', 0, 0),
(48, 'Peter', '', 'peter', '', 0, '', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', '', '', 0, '', 1, '1', 1, 22);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_waterchange`
--

CREATE TABLE IF NOT EXISTS `tbl_waterchange` (
  `wcid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `scid` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`wcid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `tbl_waterchange`
--

INSERT INTO `tbl_waterchange` (`wcid`, `uid`, `scid`, `user_type`, `created_by`, `created_date`, `staff_id`) VALUES
(11, 6, 7, 0, 'venkat', '2014-12-04 01:49:37', 0),
(20, 6, 13, 1, 'teststaff', '2014-12-04 02:37:39', 24),
(12, 6, 7, 0, 'venkat', '2014-11-25 09:04:40', 0),
(17, 17, 10, 0, 'madhan', '2014-12-03 23:55:14', 0),
(14, 17, 8, 0, 'madhan', '2014-11-27 02:41:21', 0),
(15, 17, 8, 0, 'madhan', '2014-12-02 02:46:19', 0),
(16, 17, 8, 0, 'madhan', '2014-12-02 05:40:40', 0),
(18, 6, 0, 0, 'venkat', '2014-12-03 08:59:04', 0),
(19, 6, 6, 0, 'venkat', '2014-12-04 01:49:28', 0),
(21, 6, 6, 0, 'venkat', '2014-12-05 07:03:10', 0),
(22, 6, 6, 0, 'venkat', '2014-12-05 07:03:30', 0),
(23, 6, 7, 0, 'venkat', '2014-12-05 09:25:31', 0),
(24, 22, 17, 0, 'law', '2014-12-08 11:57:54', 0),
(25, 22, 18, 0, 'law', '2014-12-08 11:58:16', 0),
(26, 22, 20, 0, 'law', '2014-12-09 07:15:31', 0),
(27, 22, 20, 0, 'law', '2014-12-09 07:16:32', 0),
(29, 6, 7, 0, 'venkat', '2014-12-10 01:49:39', 0),
(30, 38, 22, 0, 'kannan', '2014-12-10 08:04:51', 0),
(31, 6, 11, 0, 'venkat', '2014-12-10 23:21:34', 0),
(32, 6, 7, 0, 'venkat', '2014-12-18 06:06:35', 0),
(33, 38, 22, 0, 'kannan', '2014-12-18 23:47:06', 0),
(34, 22, 18, 0, 'law', '2015-01-12 03:10:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `web_marketing`
--

CREATE TABLE IF NOT EXISTS `web_marketing` (
  `name` varchar(50) DEFAULT NULL,
  `val` decimal(10,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_marketing`
--

INSERT INTO `web_marketing` (`name`, `val`) VALUES
('Direct Sales', '20.00'),
('Search Engine Marketing', '15.00'),
('PPC Advertising', '15.00'),
('Website Marketing', '10.00'),
('Blog Marketing', '10.00'),
('Social Media Marketing', '10.00'),
('Email Marketing', '10.00'),
('Online PR', '2.50'),
('Multimedia Marketing', '2.50'),
('Mobile Marketing', '2.50'),
('Display Advertising', '2.50');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
