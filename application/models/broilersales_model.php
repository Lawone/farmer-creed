<?php
Class Broilersales_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function broilersales_add() {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilersales_add = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'pen_name' => $this->input->post('pen_name'),
			'poultry' => $this->input->post('poultry'),
			'total_weight' => $this->input->post('total_weight'),
			'total_cost' => $this->input->post('total_cost'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s'),	
		);
		
		$this->db->insert('tbl_broilersales', $broilersales_add);
		$incomeid = $this->db->insert_id();
		
		$broilerincome_add = array(
			'uid' => $uid,
			'bsid' => $incomeid,
			'pen_name' => $this->input->post('pen_name'),
			'poultry' => $this->input->post('poultry'),
			'total_cost' => $this->input->post('total_cost'),
			'income_type' => 'sales',			
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date'))))
		);
		
		$this->db->insert('tbl_broilerincome', $broilerincome_add);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function broilersales_edit($bsid) {
		if($this->session->userdata('user_type') == 1)
		{
			$uid = $this->session->userdata('parent_uid');
			$staff_id = $this->session->userdata('uid');
		}
		else
		{
			$uid = $this->session->userdata('uid');
			$staff_id = 0;
		}
		$broilersales_edit = array(
			'uid' => $uid,
			'date' => date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('date')))),
			'pen_name' => $this->input->post('pen_name'),
			'poultry' => $this->input->post('poultry'),
			'total_weight' => $this->input->post('total_weight'),
			'total_cost' => $this->input->post('total_cost'),
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),	
		);
			
		$this->db->where('bsid', $bsid);
		$this->db->update('tbl_broilersales',$broilersales_edit);

		$broilerincome_edit = array(
			'uid' => $uid,
			'pen_name' => $this->input->post('pen_name'),
			'poultry' => $this->input->post('poultry'),
			'total_cost' => $this->input->post('total_cost'),
			'income_type' => 'sales',			
			'user_type' => $this->session->userdata('user_type'),
			'staff_id' => $staff_id,
			'created_by' => $this->session->userdata('username'),
		);

		$this->db->where('bsid', $bsid);
		$this->db->update('tbl_broilerincome',$broilerincome_edit);
		
	}
	
	public function getBroilersales($bsid)
	{	
		$this->db->select('*')->from('tbl_broilersales')->where('bsid', $bsid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}

	public function getBroilersalespen()
	{	
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('uid' => $this->session->userdata('parent_uid')));
		}
		$this->db->select('sbid,pen_name')->from('tbl_structure_broiler');
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}	
	
	public function broilersales_delete($bsid){
		
		$where = array('bsid' => $bsid);
		$this->db->where($where);
		$this->db->delete('tbl_broilersales');
		
		$where = array('bsid' => $bsid);
		$this->db->where($where);
		$this->db->delete('tbl_broilerincome');
		
	}
	public function getStaffname($staff_id){
	
		$this->db->select('id,firstname')->from('tbl_users')->where('id', $staff_id);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;		
	}
}
