<?php 
/** File name   : cropincome.php
*	Author      : Muthukumar
*	Date        : 11th Aug 2014
*	Description : Controller for simulation crop income.
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('user_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	
	// domain view method
	public function edit($id) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		$this->form_validation->set_rules('email', 'User E-mail', 'trim|required|callback_alreadyExistEmailEdit|max_length[128]|xss_clean');
		if($this->input->post('current_password'))
			$this->form_validation->set_rules('current_password', 'current password', 'min_length[8]|max_length[20]|callback_checkUserPwd|sha1|xss_clean');
		$this->form_validation->set_rules('password', 'password', 'min_length[8]|max_length[20]|sha1|xss_clean');	
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[32]|xss_clean');
		//$this->form_validation->set_rules('farmname','Farm','trim|required|max_length[32]');
		$this->form_validation->set_rules('phone', 'Phone number', 'trim|required|max_length[40]|xss_clean');
		//$this->form_validation->set_rules('gender','Gender','trim|required|max_length[10]');
		//$this->form_validation->set_message('is_unique', 'Username is already Exists');
		//$this->form_validation->set_message('is_unique', 'User E-mail is already Exists');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->user_model->edit($id);
			$this->session->set_flashdata('message', 'Profile has been changed successfully.');
			redirect(base_url().'user/profile/edit/'.$id, 'refresh');
		}
		
		$data['profile_e'] = $this->user_model->getProfile($id);
		
		$this->load->view('user/profile_edit',$data);
	}
	
	public function alreadyExistUser() {
        $this->db->select('username')->from('tbl_users')->where(array('username' => $this->input->post('username')));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->form_validation->set_message('alreadyExistUser', 'Username already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function alreadyExistEmail() {
        $this->db->select('email')->from('tbl_users')->where(array('email' => $this->input->post('email')));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->form_validation->set_message('alreadyExistEmail', 'Email already exists.');
            return false;
        } else {
            return true;
        }
    }
	
	public function alreadyExistEmailEdit() {
        $this->db->select('email')->from('tbl_users')->where(array('email' => $this->input->post('email'),'id !=' => $this->session->userdata('uid')));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->form_validation->set_message('alreadyExistEmail', 'Email already exists.');
            return false;
        } else {
            return true;
        }
    }
	
	public function checkUserPwd()
	{
		$this->db->select('password')->from('tbl_users')->where(array('id' => $this->session->userdata('uid')));
		$query = $this->db->get();
		if($query->num_rows() > 0 && $query->row()->password == sha1($this->input->post('current_password')) && $this->input->post('current_password') !=''){
			return true;
		}else if($this->input->post('current_password') !=''){
			$this->form_validation->set_message('checkUserPwd', 'You are entered current password is incorrect. please enter the correct password.');
			return false;
		}
	}
	
}