<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		$this->load->library(array('email'));
		
	}
	
	public function sendEmail($info) {
		
		// Email settings to send email
		$this->email->clear(); // Clear previous cache
		//$this->email->set_newline("\r\n");
		$this->email->from('alert@creedapps.com', 'Farmers Creed Team'); // from alert@creedapps.com
		$this->email->to($info['to']);
		$this->email->subject($info['subject']);
		$this->email->message($info['message']);
		$this->email->send();
		$status = 'Email sent';
		return $status;
	}
	
	//template for user registration
	public function regTemp($info)
	{
		$userName = $info['firstname'];
		$email = $info['email'];
		$vfcnPath = base_url().'user/dashboard/emailVerification/'.$info['vfcnCode'];
		$logoPath = base_url().'img/logo.png';
		$currYear = date('Y');
		$template = '<!DOCTYPE html>
						<html>
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<meta name="viewport" content="initial-scale=1.0">
						<style type="text/css">
						::selection
						{
						color:#fff;
						background-color:#49A4A2;}
						::-moz-selection
						{
						color:#fff; background-color:#49A4A2;}
						.top_bar {
							display:none;
							}
						@media only screen and (max-width: 480px) {
							table[class=table], td[class=cell] { width: 300px !important; }
							table[class=table], td[class=cell] { width: 300px !important; }
						}
						.link-btn {
							background-color: #797979;
							border-radius: 15px;
							box-shadow: none;
							color: #FFFFFF;
							float: right;
							font-family: Arial,Helvetica,sans-serif;
							font-size: 16px;
							font-weight: bold;
							height: auto;
							padding: 5px 40px;
							min-width: 50px;
						}
						</style>
							<title>Welcome to Farmers Creed</title>
						</head>
						<body style="margin:0; padding:0; background-color:#F4F4F4; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:14px;">	

							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
							<tr>
								<td bgcolor="#F4F4F4" align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td><div style="max-width:700px; margin:0 auto;">
										<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="center" bgcolor="#656565" style="padding-top:10px; padding-bottom:10px;"><img src="'.$logoPath.'" style="width:161px;"></td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="30">&nbsp;</td>
														<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td height="20">&nbsp;</td>
														</tr>
														<tr>
															<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear '.$userName.' </strong></td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left">Thank you for registering with Farmer`s Creed.<br><br>
															To verify your email address and continue, please click the following link:<br><br>
															</td>
														</tr>
														  <tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left">
															<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7" style="border:1px solid #F0F0F0; -webkit-box-shadow: 2px 2px 3px 1px #9E9E9E;
														box-shadow: 2px 2px 3px 1px #9E9E9E;">
														  <tr>
															<td style="font-size:13px; padding-top:7px; padding-right:10px; padding-bottom:5px; padding-left:10px;">
															
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
														  <tr>
															<td width="100%" style="word-wrap:break-word;  font-family:Arial, Helvetica, sans-serif; padding-right:10px;" class="link">
															<a href="'.$vfcnPath.'" target="_blank" style="font-size:13px; color:#2C6CB8;" >'.$vfcnPath.'</a></td>
															<td width="80"><a href="'.$vfcnPath.'" class="link-btn" > Go </a></td>
														  </tr>
														</table>

															</td>
														  </tr>
														</table>

															</td>
														  </tr>
						  <tr>
							<td style="font-size:13px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left"><br>
						<br>

							Your Email ID: 
							<a href="#" style="color:#2C6CB8; font-size:13px; text-decoration:none;">'.$email.'</a>
						</td>
						  </tr>
						  <tr>
							<td style="font-size:15px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left"><br>
						<br>
						<strong>creedapps.com</strong><br>
						<br>

						</td>
						  </tr>

						</table></td>
							<td width="30">&nbsp;</td>
						  </tr>
						</table>


							
							
						</td>
						  </tr>
						  
						</table>
						</td>
										</tr>
										<tr>
										<td bgcolor="#656565" style="padding-top:22px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="19%">&nbsp;</td>
							<td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#459d75" style="-webkit-border-radius: 15px;
						border-radius: 15px;">
						  <tr>
							<td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
							Copyright © '.$currYear.' Creed Apps, All rights reserved.</td>
						  </tr>
						</table>
						</td>
							<td width="19%">&nbsp;</td>
						  </tr>
						</table>

									   
										</td>
										</tr>
										<tr>
										<td bgcolor="#656565" style="padding-top:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td style="color:#d2f736; font-size:11px; font-family:Arial, Helvetica, sans-serif;" align="center">You received this email because you registered at Creed Apps<br><br>

						<br>
						</td>
						  </tr>
						</table>


									   
										</td>
										</tr>
										
									  </table>
									</div></td>
								</tr>
							  </table>
							  </td>
						  </tr>
						</table>
						</body>
						</html>';
						return $template;
	}
	
	public function regInfoAdmin($info)
	{
		$firstname = $info['firstname'];
		$username = $info['username'];
		$farmname = $info['farmname'];
		$email = $info['email'];
		$phone = $info['phone'];
		$address = $info['address'];
		$logoPath = base_url().'img/logo.png';
		$currYear = date('Y');
		$template = '<!DOCTYPE html>
						<html>
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<meta name="viewport" content="initial-scale=1.0">
						<!-- So that mobile webkit will display zoomed in -->
						<!-- CSS for mobile devices. Attribute selector used so that Yahoo! mail will ignore styles -->
						<style type="text/css">
						::selection
						{
						color:#fff;
						background-color:#49A4A2;}
						::-moz-selection
						{
						color:#fff; background-color:#49A4A2;}
						.top_bar {
							display:none;
							}
						@media only screen and (max-width: 480px) {
							table[class=table], td[class=cell] { width: 300px !important; }
							table[class=table], td[class=cell] { width: 300px !important; }
						}
						.link-btn {
							background-color: #797979;
							border-radius: 15px;
							box-shadow: none;
							color: #FFFFFF;
							float: right;
							font-family: Arial,Helvetica,sans-serif;
							font-size: 16px;
							font-weight: bold;
							height: auto;
							padding: 5px 40px;
							width: auto;
						}
						</style>
							<title>Welcome to Uptimeweb</title>
						</head>
						<body style="margin:0; padding:0; background-color:#F4F4F4; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:14px;">	

							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
							<tr>
								<td bgcolor="#F4F4F4" align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td><div style="max-width:700px; margin:0 auto;">
										<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="center" bgcolor="#656565" style="padding-top:10px; padding-bottom:10px;"><img src="'.$logoPath.'" style="width:161px;"></td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="30">&nbsp;</td>
														<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td height="20">&nbsp;</td>
														</tr>
														<tr>
															<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear admin, </strong></td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left"><strong>User information mentioned below.</strong>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Name<span style="margin-left:8.1em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$firstname.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Email<span style="margin-left:8.2em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$email.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																User name<span style="margin-left:5.7em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$username.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Farm name<span style="margin-left:5.4em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$farmname.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Phone<span style="margin-left:7.6em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$phone.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Address<span style="margin-left:6.8em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$address.'</span>
															</td>
														</tr>
						</table></td>
							<td width="30">&nbsp;</td>
						  </tr>
						</table>


							
							
						</td>
						  </tr>
						  
						</table>
						</td>
										</tr>
										<tr>
										<td bgcolor="#656565" style="padding:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="19%">&nbsp;</td>
							<td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#459d75" style="-webkit-border-radius: 15px;
						border-radius: 15px;">
						  <tr>
							<td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
							Copyright © '.$currYear.' Creed Apps,All rights reserved.</td>
						  </tr>
						</table>
						</td>
							<td width="19%">&nbsp;</td>
						  </tr>
						</table>

									   
										</td>
										</tr>
										
									  </table>
									</div></td>
								</tr>
							  </table>
							  </td>
						  </tr>
						</table>
						</body>
						</html>';
						return $template;
	}
	
	public function adminReg($info)
	{
		$userFirstName = $info['firstname'];
		$userName = $info['username'];
		$pwd = $info['pwd']; 
		$logoPath = base_url().'img/logo.png';
		$loginPath = base_url().'admin/dashboard/login';
		$currYear = date('Y');
		$template = '<!DOCTYPE html>
						<html>
						<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<meta name="viewport" content="initial-scale=1.0">
						<!-- So that mobile webkit will display zoomed in -->
						<!-- CSS for mobile devices. Attribute selector used so that Yahoo! mail will ignore styles -->
						<style type="text/css">
						::selection
						{
						color:#fff;
						background-color:#49A4A2;}
						::-moz-selection
						{
						color:#fff; background-color:#49A4A2;}
						.top_bar {
							display:none;
							}
						@media only screen and (max-width: 480px) {
							table[class=table], td[class=cell] { width: 300px !important; }
							table[class=table], td[class=cell] { width: 300px !important; }
						}
						.link-btn {
							background-color: #797979;
							border-radius: 15px;
							box-shadow: none;
							color: #FFFFFF;
							float: right;
							font-family: Arial,Helvetica,sans-serif;
							font-size: 16px;
							font-weight: bold;
							height: auto;
							padding: 5px 40px;
							width: auto;
						}
						</style>
							<title>Welcome to Cloud Simulation</title>
						</head>
						<body style="margin:0; padding:0; background-color:#F4F4F4; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:14px;">	

							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
							<tr>
								<td bgcolor="#F4F4F4" align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr>
									<td><div style="max-width:700px; margin:0 auto;">
										<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td align="center" bgcolor="#D7D7D7" style="padding-top:10px; padding-bottom:10px;"><img src="'.$logoPath.'"></td>
												</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="30">&nbsp;</td>
														<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr>
															<td height="20">&nbsp;</td>
														</tr>
														<tr>
															<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:10px;" align="left"><strong>Dear '.$userFirstName.' </strong></td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;" align="left">Thank you for joining with Cloud Simulation.<br><br>
															Our admin has been created your account with temporary username and password. You must change the username and password the next time sign in. <br><br>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left"><strong>Login information mentioned below.</strong>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																Username<span style="margin-left:4em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$userName.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																password<span style="margin-left:4.1em;">:&nbsp;&nbsp;&nbsp;&nbsp;'.$pwd.'</span>
															</td>
														</tr>
														<tr>
															<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
																please&nbsp;&nbsp;<a href="'.$loginPath.'" target="_blank" style="font-size:14px; color:#2C6CB8;" >click here</a>&nbsp;&nbsp;to login your account
															</td>
														</tr>

						</table></td>
							<td width="30">&nbsp;</td>
						  </tr>
						</table>


							
							
						</td>
						  </tr>
						  
						</table>
						</td>
										</tr>
										<tr>
										<td bgcolor="#D7D7D7" style="padding:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="19%">&nbsp;</td>
							<td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333" style="-webkit-border-radius: 15px;
						border-radius: 15px;">
						  <tr>
							<td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
							Copyright © '.$currYear.' Cloud simulation. All rights reserved.</td>
						  </tr>
						</table>
						</td>
							<td width="19%">&nbsp;</td>
						  </tr>
						</table>

									   
										</td>
										</tr>
										
									  </table>
									</div></td>
								</tr>
							  </table>
							  </td>
						  </tr>
						</table>
						</body>
						</html>';
						return $template;
	}
	
	public function fgtPwdTemp($info)
	{
		$userName = $info['firstname'];
		$password = $info['password'];
		$logoPath = base_url().'img/logo.png';
		$loginPath = base_url().'user/dashboard/login';
		$currYear = date('Y');
		$template = '<!DOCTYPE html>
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="initial-scale=1.0">
		<!-- So that mobile webkit will display zoomed in -->
		<!-- CSS for mobile devices. Attribute selector used so that Yahoo! mail will ignore styles -->
		<style type="text/css">
		::selection
		{
		color:#fff;
		background-color:#49A4A2;}
		::-moz-selection
		{
		color:#fff; background-color:#49A4A2;}
		.top_bar {
			display:none;
			}
		@media only screen and (max-width: 480px) {
			table[class=table], td[class=cell] { width: 300px !important; }
			table[class=table], td[class=cell] { width: 300px !important; }
		}
		.link-btn {
			background-color: #797979;
			border-radius: 15px;
			box-shadow: none;
			color: #FFFFFF;
			float: right;
			font-family: Arial,Helvetica,sans-serif;
			font-size: 16px;
			font-weight: bold;
			height: auto;
			padding: 5px 40px;
			width: auto;
		}
		</style>
			<title>Welcome to Uptimeweb</title>
		</head>
		<body style="margin:0; padding:0; background-color:#F4F4F4; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:14px;">	

			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F4F4">
			<tr>
				<td bgcolor="#F4F4F4" align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td><div style="max-width:700px; margin:0 auto;">
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:700px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;" bgcolor="#FFFFFF">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center" bgcolor="#656565" style="padding-top:10px; padding-bottom:10px;"><img src="'.$logoPath.'" style="width:161px;"></td>
								</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cccccc;">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="30">&nbsp;</td>
										<td width="92%"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td height="20">&nbsp;</td>
										</tr>
										<tr>
											<td style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:rgb(218,33,40); padding-bottom:3px;" align="left"><strong>Dear '.$userName.', </strong></td>
										</tr>
										<tr>
											<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:3px;" align="left">
												<p style="line-height:20px;"><strong>Your password was reset successfully.</strong></p>
											</td>
										</tr>
										<tr>
											<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:18px;" align="left">
												Your new password for farmers creed is :&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.$password.'</strong>
											</td>
										</tr>
										<tr>
											<td style="font-size:14px;line-height:20px;font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:18px;" align="left">
												Once logged in, please change your password to something you can remember.Never share your password anybody.
											</td>
										</tr>
										<tr>
											<td style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#4A4A4A;padding-bottom:10px;" align="left">
												In order to login with new password, please&nbsp;<a href="'.$loginPath.'">click here</a>
											</td>
										</tr>
		</table></td>					
			<td width="30">&nbsp;</td>
		  </tr>
		</table>


			
			
		</td>
		  </tr>
		  
		</table>
		</td>
						</tr>
						<tr>
						<td bgcolor="#656565" style="padding:15px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="19%">&nbsp;</td>
			<td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#459d75" style="-webkit-border-radius: 15px;
			border-radius: 15px;">
			  <tr>
				<td style="font-size:13px; color:#fff; padding-top:5px; padding-bottom:5px; padding-left:8px; padding-right:8px; font-family:Arial, Helvetica, sans-serif;"  align="center">
				Copyright © '.$currYear.' Creed Apps,All rights reserved.</td>
			  </tr>
			</table>
			</td>
			<td width="19%">&nbsp;</td>
		  </tr>
		</table>

					   
						</td>
						</tr>
						
					  </table>
					</div></td>
				</tr>
			  </table>
			  </td>
		  </tr>
		</table>
		</body>
		</html>';
		return $template;
	}
		
}