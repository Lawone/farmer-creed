<?php 
error_reporting(0);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class expense_broiler extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('broilersales_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	
	
	// domain view method
	public function expensebroiler_view()
	 {
             if (isset($_REQUEST['search'])) {
			 
				 $f_arr = array();
				 $date_arr = array();
				 $feedQuery = "SELECT con.bfpid,sum(con.feedweight) as feedweight,date(con.created_date) as created_date,pur.weight,pur.total_cost,pur.no_of_bags FROM tbl_broilerfeed con, tbl_broilerfeedpurchased pur WHERE";
				 $salaryQuery = "SELECT sum(amount) as amount,date(date) as created_date FROM tbl_salaries WHERE";
				 $salaryCountQuery = "SELECT count(created_date) as salcount FROM tbl_salaries WHERE";
				 $totalStockQuery = "SELECT sum(total_stocked) as total_stock FROM tbl_livestock_broiler WHERE";
				 $totalStockPondQuery = "SELECT sum(total_stocked) as total_pondstock FROM tbl_livestock_broiler WHERE";
				 $chickQuery = "SELECT date(date) as created_date,sum(total_cost) as chickAmt FROM tbl_chickpurchased WHERE";
				 $otherExpQuery = "SELECT date(date) as created_date,sum(amount) as othExpAmt FROM tbl_broilergenpurchased WHERE";
						 
				 if((isset($_POST['pen_name'])) ){
					$feedQuery .=  "  `sbid` = '".$_POST['pen_name']."' and con.uid = '".$this->session->userdata('uid')."' and con.bfpid = pur.bfpid and con.uid = pur.uid ";
					$salaryQuery .=  "  uid = '".$this->session->userdata('uid')."' ";
					$salaryCountQuery .=  "  uid = '".$this->session->userdata('uid')."' ";
					$totalStockQuery .=  "  uid = '".$this->session->userdata('uid')."' ";
					$totalStockPondQuery .=  "  uid = '".$this->session->userdata('uid')."' and sbid = '".$_POST['pen_name']."' ";
					$chickQuery .=  "  `pen_name` = '".$_POST['pen_name']."' and uid = '".$this->session->userdata('uid')."' ";
					$otherExpQuery .=  "  `uid` = '".$this->session->userdata('uid')."' ";
				 }
			  
				 if((!empty($_POST['pen_name'])) &&  (!empty($_POST['start-date']))  &&  (!empty($_POST['end-date']))) {
		  
					$feedQuery .=  "  AND (date(con.created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."') ";
					$salaryQuery .=  "  AND (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
					$salaryCountQuery .=  "  AND (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
					$totalStockQuery .=  "  AND (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
					$totalStockPondQuery .=  "  AND (date(created_date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
					$chickQuery .=  "  AND (date(date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
					$otherExpQuery .=  "  AND (date(date) BETWEEN '".$_POST['start-date']."' AND '".$_POST['end-date']."')  ";
				   
				 }
				 $feedQuery .= "group by con.bfpid,date(con.created_date)";
				 $salaryQuery .= "group by date(date)";
				 $chickQuery .= "group by date(date)";
				 $otherExpQuery .= "group by date(date)";
				 
				 
				 //echo $feedQuery.'<br/>'.$salaryQuery.'<br/>'.$salaryCountQuery.'<br/>'.$totalStockQuery.'<br/>'.$totalStockPondQuery.'<br/>'.$chickQuery.'<br/>'.$otherExpQuery;
					//Feeding expense calculation
					$query = $this->db->query($feedQuery);
					$result = $query->result_array();
					
					$totalExp = 0;
					foreach ($result as $key => $value) {
						if(in_array($value['created_date'], $date_arr)){
								$now_count = array_search ($value['created_date'], $date_arr);
							}  else {
								$now_count = count($f_arr);
								$date_arr[$now_count] = $value['created_date'];
								$f_arr[$now_count]['salaryExp'] = 0;
								$f_arr[$now_count]['chickExp'] = 0;
								$f_arr[$now_count]['otherExp'] = 0;
							}
						//echo $value['feedweight'].'---'.$value['weight'].'----'.$value['total_cost'];
						$feedCost = ($value['feedweight'] / ($value['no_of_bags'] * $value['weight'])) * $value['total_cost'];
						$f_arr[$now_count]['date'] = $value['created_date'];
						$totalExp += $f_arr[$now_count]['feedExpense'] += number_format($feedCost,2,'.','');
						
						
					}
			  

					//echo $sql_query1 .= " GROUP BY date( `created_date` )";

					//Salary expense calculation
					$query1 = $this->db->query($salaryQuery);
					$result1 = $query1->result_array();
					
					//get days count salary
					$salCountQuery = $this->db->query($salaryCountQuery);
					$salCountData = $salCountQuery->row_array();
					$salCount = ($salCountQuery -> num_rows() > 0) ? $salCountData['salcount'] : 0;
					
					//get user total stock
					$stockTotQuery = $this->db->query($totalStockQuery);
					$stockTotData = $stockTotQuery->row_array();
					$totStock = ($stockTotQuery -> num_rows() > 0) ? $stockTotData['total_stock'] : 0;
					
					//get user with pond total stock
					$stockPondTotQuery = $this->db->query($totalStockPondQuery);
					$stockPondTotData = $stockPondTotQuery->row_array();
					$totPondStock = ($stockPondTotQuery -> num_rows() > 0) ? $stockPondTotData['total_pondstock'] : 0;
					if($totStock > 0 && $totPondStock > 0){
						foreach ($result1 as $key => $value) {
							$salCost1 = ($value['amount']) / $totStock;
							$salCost = number_format(($salCost1 * $totPondStock),2,'.','');
							//echo $value['amount'].'---'.$salCount.'----'.$totStock.'----'.$totPondStock.'<br/>';
							if(in_array($value['created_date'], $date_arr)){
								$now_count = array_search ($value['created_date'], $date_arr);
							}  else {
								$now_count = count($f_arr);
								$date_arr[$now_count] = $value['created_date'];
								$f_arr[$now_count]['feedExpense'] = 0;
								$f_arr[$now_count]['chickExp'] = 0;
								$f_arr[$now_count]['otherExp'] = 0;
							}
							
							$f_arr[$now_count]['date'] = $value['created_date'];
							$totalExp += $f_arr[$now_count]['salaryExp'] = number_format($salCost,2,'.','');
							
												   
						}
					}
					
					$query3 = $this->db->query($chickQuery);
					$result3 = $query3->result_array();
					if($totStock > 0 && $totPondStock > 0){
						foreach ($result3 as $key => $value) {
							if(in_array($value['created_date'], $date_arr)){
								$now_count = array_search ($value['created_date'], $date_arr);
							}  else {
								$now_count = count($f_arr);
								$date_arr[$now_count] = $value['created_date'];
								$f_arr[$now_count]['feedExpense'] = 0;
								$f_arr[$now_count]['salaryExp'] = 0;
								$f_arr[$now_count]['otherExp'] = 0;
							}
							
							$f_arr[$now_count]['date'] = $value['created_date'];
							$totalExp += $f_arr[$now_count]['chickExp'] = number_format($value['chickAmt'],2,'.','');
							
												   
						}
					}
					
					$query4 = $this->db->query($otherExpQuery);
					$result4 = $query4->result_array();
					foreach ($result4 as $key => $value) {
						$othCost = ($value['othExpAmt'] / $totStock) * $totPondStock;
						//echo $value['othExpAmt'].'---'.$totStock.'----'.$totPondStock;
						if(in_array($value['created_date'], $date_arr)){
							$now_count = array_search ($value['created_date'], $date_arr);
						}  else {
							$now_count = count($f_arr);
							$date_arr[$now_count] = $value['created_date'];
							$f_arr[$now_count]['feedExpense'] = 0;
							$f_arr[$now_count]['salaryExp'] = 0;
							$f_arr[$now_count]['chickExp'] = 0;
						}
						
						$f_arr[$now_count]['date'] = $value['created_date'];
						$totalExp += $f_arr[$now_count]['otherExp'] = number_format($othCost,2,'.','');
						
											   
					}
                    

		     $data['expenses'] = $f_arr;
		     $data['totalExp'] = $totalExp;
		}
         $data['getpondname']=$this->broilersales_model->getBroilersalespen();
        $this->load->view('user/expense_broiler', $data);
    }
}
	
