<?php
Class Structlivestockcatfish_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function add() {
		$catfish_info = array(
			'uid' => $this->session->userdata('uid'),
			'scid' => $this->input->post('scid'),
			'stock_density' => $this->input->post('stock_density'),
			'total_stocked' => $this->input->post('total_stocked'),
			'avg_weight' => $this->input->post('avg_weight'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('tbl_livestock_catfish', $catfish_info);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function edit($uid) {
		$livestockcatfishedit_info = array(
			'uid' => $this->session->userdata('uid'),
			'scid' => $this->input->post('scid'),
			'stock_density' => $this->input->post('stock_density'),
			'total_stocked' => $this->input->post('total_stocked'),
			'avg_weight' => $this->input->post('avg_weight'),
			'created_by' => $this->session->userdata('username')
		);
			
		$this->db->where('lcid', $uid);
		$this->db->update('tbl_livestock_catfish',$livestockcatfishedit_info);
		
	}
	
	//change profile
	public function getStructlivestock($lcid)
	{
		$this->db->select('*')->from('tbl_livestock_catfish')->where('lcid', $lcid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	public function getStructlivestockcatfish()
	{
		$this->db->select('scid,pond_name')->from('tbl_structure_catfish')->where('uid', $this->session->userdata('uid'));
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : false;
	}
	public function delete($scid){
		
		$where = array('lcid' => $scid);
		$this->db->where($where);
		$this->db->delete('tbl_livestock_catfish');
		
	}
	
}
