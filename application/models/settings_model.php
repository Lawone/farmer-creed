<?php
Class Settings_model extends CI_Model
{

    //var $CI;

    function __construct()
    {
        parent::__construct();
		$this->load->model(array('mail_model'));
    }

	function add_live(){
	
	
	
	if($livestockCount > 0){
	
	$hdr_info = array(
			'catfish' => $catfish,
			'layers' => $layers,
			'broilers' => $broilers,
			);
		$this->db->where('uid',$this->session->userdata('uid'));
			$this->db->update('tbl_existing_farmer',$hdr_info);
			
			}
			else{
			$hdr_info = array(
			'uid'   => $this->session->userdata('uid'),
			'catfish' => $catfish,
			'layers' => $layers,
			'broilers' => $broilers,
			);
		$this->db->insert('tbl_existing_farmer', $hdr_info);
		 $ihid = $this->db->insert_id();
		}
	}
	public function getlivestockCount(){
		$this->db->select('count(uid) as total')->from('tbl_existing_farmer')->where(array('uid' => $this->session->userdata('uid')));
		$query = $this->db->get();
		$queryRow = $query->row_array();
		return ($query->num_rows() > 0) ? $queryRow['total'] : 0;
			}
			public function getlivestock(){
		$this->db->select('*')->from('tbl_existing_farmer')->where(array('uid' => $this->session->userdata('uid')));
		$query = $this->db->get();
		$queryRow = $query->row_array();
		return ($query->num_rows() > 0) ? $queryRow : false;
			}
	function add_site($ssid=""){
	if($ssid != '')
		{
		$uid = $ssid;
		$siteCount = $this->getsite($ssid);
		}
		else
		{
		$uid = $this->session->userdata('uid');
		$siteCount = $this->getsite();
				}
	if($siteCount > 0){
	$site_info = array(
			'access_reliable_source_water' => $this->input->post('access_reliable_source_water'),
			'waterof_good_quality' => $this->input->post('waterof_good_quality'),
			'access_drainage_waster_water' => $this->input->post('access_drainage_waster_water'),
			'close_proximity_market' => $this->input->post('close_proximity_market'),
			'secured_pilfering_predators' => $this->input->post('secured_pilfering_predators'),
			);
		$this->db->where('uid',$uid);
			$this->db->update('tbl_existing_farmer',$site_info);
			}
			else{
			$site_info = array(
			'access_reliable_source_water' => $this->input->post('access_reliable_source_water'),
			'waterof_good_quality' => $this->input->post('waterof_good_quality'),
			'access_drainage_waster_water' => $this->input->post('access_drainage_waster_water'),
			'close_proximity_market' => $this->input->post('close_proximity_market'),
			'secured_pilfering_predators' => $this->input->post('secured_pilfering_predators'),
			);
		$this->db->insert('tbl_existing_farmer', $site_info);
		 $ihid = $this->db->insert_id(); 
		}
		}
		public function getsite($ssid=''){
		if($ssid != ''){
			$this->db->where(array('uid' => $ssid));
					}else{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		$this->db->select('count(uid) as total')->from('tbl_existing_farmer');
		$query = $this->db->get();
		$queryRow = $query->row_array();
		return ($query->num_rows() > 0) ? $queryRow['total'] : 0;
	}
		

	function add_structure($stid=""){
if($stid != '')
		{
		$uid = $stid;
		$structureCount = $this->getstructure($stid);
		}
		else
		{
		$uid = $this->session->userdata('uid');
		$structureCount = $this->getstructure();
				}
	if($structureCount > 0){
		$structure_info = array(
			'land_rented' => $this->input->post('land_rented'),
			'land_used_other_things' => $this->input->post('land_used_other_things'),
			'land_inthat_loc_expensive' => $this->input->post('land_inthat_loc_expensive'),
			'access_tomore_land_if_needed' => $this->input->post('access_tomore_land_if_needed'),
			);
			 $this->db->where('uid',$uid);
			$this->db->update('tbl_existing_farmer',$structure_info);
	}
	else{
	$structure_info = array(
			'land_rented' => $this->input->post('land_rented'),
			'land_used_other_things' => $this->input->post('land_used_other_things'),
			'land_inthat_loc_expensive' => $this->input->post('land_inthat_loc_expensive'),
			'access_tomore_land_if_needed' => $this->input->post('access_tomore_land_if_needed'),
			);
			$this->db->insert('tbl_existing_farmer', $structure_info);
			$ihid = $this->db->insert_id(); 
	}
	}
	public function getstructure($stid=''){
		if($stid != ''){
			$this->db->where(array('uid' => $stid));
					}else{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		$this->db->select('count(uid) as total')->from('tbl_existing_farmer');
		$query = $this->db->get();
		$queryRow = $query->row_array();
		return ($query->num_rows() > 0) ? $queryRow['total'] : 0;
	}
	
	
	
	
	
	function add_stocking($stsid=""){
	if($stsid != '')
		{
		$uid = $stsid;
		$stockingCount = $this->getstructure($stsid);
		}
		else
		{
		$uid = $this->session->userdata('uid');
		$stockingCount = $this->getstocking();
				}
	if($stockingCount > 0){
		$stocking_info = array(
			'allin_allout' => $this->input->post('allin_allout'),
			'batch' => $this->input->post('batch'),
			);
			 $this->db->where('uid',$uid);
			$this->db->update('tbl_existing_farmer',$stocking_info);
	}
	else{
	$structure_info = array(
			'allin_allout' => $this->input->post('allin_allout'),
			'batch' => $this->input->post('batch'),
			);
		
		$this->db->insert('tbl_existing_farmer', $stocking_info);
		 $ihid = $this->db->insert_id(); 
		}
	}
	public function getstocking($stsid=''){
		if($stsid != ''){
			$this->db->where(array('uid' => $stsid));
					}else{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		$this->db->select('count(uid) as total')->from('tbl_existing_farmer');
		$query = $this->db->get();
		$queryRow = $query->row_array();
		return ($query->num_rows() > 0) ? $queryRow['total'] : 0;
	}
		


function add_running($reid=""){
	if($reid != '')
		{
		$uid = $reid;
		$runningCount = $this->getrunning($reid);
		}
		else
		{
		$uid = $this->session->userdata('uid');
		$runningCount = $this->getrunning();
				}
	if($runningCount > 0){
		$running_info = array(
			'cost_of_fingerlings' => $this->input->post('cost_of_fingerlings'),
			'cost_of_feed' => $this->input->post('cost_of_feed'),
			'total_weight_of_feed' => $this->input->post('total_weight_of_feed'),
			'labour_cost' => $this->input->post('labour_cost'),
					);
			 $this->db->where('uid',$uid);
			$this->db->update('tbl_existing_farmer',$running_info);
	}
	else{
	$running_info = array(
			'cost_of_fingerlings' => $this->input->post('cost_of_fingerlings'),
			'cost_of_feed' => $this->input->post('cost_of_feed'),
			'total_weight_of_feed' => $this->input->post('total_weight_of_feed'),
			'labour_cost' => $this->input->post('labour_cost'),
			);

		$this->db->insert('tbl_existing_farmer', $running_info);
		 $ihid = $this->db->insert_id();
		}
	}
	public function getrunning($reid=''){
		if($reid != ''){
			$this->db->where(array('uid' => $reid));
					}else{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		$this->db->select('count(uid) as total')->from('tbl_existing_farmer');
		$query = $this->db->get();
		$queryRow = $query->row_array();
		return ($query->num_rows() > 0) ? $queryRow['total'] : 0;
	}
	
	function add_selling($spid=""){
	if($spid != '')
		{
		$uid = $spid;
		$sellingCount = $this->getselling($spid);
		}
		else
		{
		$uid = $this->session->userdata('uid');
		$sellingCount = $this->getselling();
				}
	if($sellingCount > 0){
		$selling_info = array(
			'selling_price' => $this->input->post('selling_price'),
			);
			 $this->db->where('uid',$uid);
			$this->db->update('tbl_existing_farmer',$selling_info);
	}
	else{
	$selling_info = array(
			'selling_price' => $this->input->post('selling_price'),
			);
			$this->db->insert('tbl_existing_farmer', $selling_info);
			$ihid = $this->db->insert_id();
		}
	}
	public function getselling($spid=''){
		if($spid != ''){
			$this->db->where(array('uid' => $reid));
					}else{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		$this->db->select('count(uid) as total')->from('tbl_existing_farmer');
		$query = $this->db->get();
		$queryRow = $query->row_array();
		return ($query->num_rows() > 0) ? $queryRow['total'] : 0;
	}
	

}