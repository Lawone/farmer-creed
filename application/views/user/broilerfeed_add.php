<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
 @media (max-width : 360px){
 .mob {
		margin-top:-2.5em !important;
		margin-left:12em !important;
}
 @media (max-width : 320px){
 .mob {
		margin-top:-2.5em !important;
		margin-left:11em !important;
}
}
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:62%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-7" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2>Add Broiler Feeding</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >
							<fieldset>
								<div class="row">
									<section class="col col-10">
									<label class="label">Pen Name</label>
									<label class="select">
										<select name="sbid">
										<option value="" selected="" disabled="">Please select Pen name</option>
										<?php
											 if ($getpondname != false) {
												 foreach ($getpondname as $getpondnames) {
													echo '<option value="'.$getpondnames['sbid'].'">'.$getpondnames['pen_name'].'</option>';
												}
											}
											?>
										</select> <i></i>
										<?php echo form_error('sbid'); ?>
										</label>
									</section>
									<!--<section class="col col-10">
										<div class="form-group">
											<label>Time</label>
											<div class="input-group">
												<input class="form-control" id="time" name="time" type="text" placeholder="Select time">
												<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>									</div>
										</div>
									</section>-->
                                                                                                                                                              <!--<section class="col col-10">
									<label class="label">Feed Type</label>
									<label class="select">
										<select name="feedtype">
											<option value="" selected="" disabled="">Please Select FeedType</option>
											<option value="1">Starter</option>
											<option value="2">Grower</option>
                                                                                                                                                                                                  <option value="3">Finisher</option>
										</select> <i></i>
									</label>
									<?php echo form_error('feedtype'); ?>
									</section>-->
                                                                    
                                                                    
                                                                        <?php /*
									<section class="col col-10">
									<label class="label">Feed Brand</label>
									<label class="select">
										<select name="feedbrand">
										<option value="" selected="" disabled="">Please select Feeding Brand</option>
										<?php
											 if ($getbroilerfeedbrand != false) {
												 foreach ($getbroilerfeedbrand as $getbroilerfeedbrands) {
													echo '<option value="'.$getbroilerfeedbrands['brand'].'">'.$getbroilerfeedbrands['brand'].'</option>';
												}
											}
											?>
	
										</select> <i></i>
										<?php echo form_error('feedbrand'); ?>
										</label>
									</section>
                                                                        
                                                                         * 
                                                                         */ ?>
                                                                        <section class="col col-10">
                                                                            <label class="label">Feed Brand - Available (Ref #)</label>
                                                                            <label class="select">
                                                                                <?php //echo "<pre>";  print_r($getbroilerfeedbrand); echo "</pre>";?>
                                                                                <select name="feedid" onchange="avlfeedweightfun(this.options[this.selectedIndex].getAttribute('aw'),  this.options[this.selectedIndex].getAttribute('ab'))">
                                                                                    <option value="" selected="" disabled="">Please select Feeding Size</option>
                                                                                    <?php
                                                                                    if ($getbroilerfeedbrand != false) {
                                                                                        foreach ($getbroilerfeedbrand as $getcatfishfeedbrands) {
                                                                                            echo '<option aw="' . $getcatfishfeedbrands['avl_weight'] . '"  ab="' . $getcatfishfeedbrands['brand'] . '" value="' . $getcatfishfeedbrands['bfpid'] . '">' .  $getcatfishfeedbrands['brand'] .' - ' .$getcatfishfeedbrands['avl_weight']. ' (' . $getcatfishfeedbrands['bfpid'] . ')' . '</option>';
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </select> <i></i>
                                                                                <input type="hidden" name="feedbrand" id="feedbrand" value="" />
                                                                                <input type="hidden" name="feedaw" id="feedaw" value="" />
                                                                                <?php echo form_error('feedsize'); ?>
                                                                            </label>
                                                                        </section>
                                                                    
                                                                    
									<section class="col col-10">
										<label class="label">Feed Weight</label>
										<label class="input">
											<input type="text" name="feedweight" id="feedweight" style="text-align: right">
										</label>
										<?php echo form_error('feedweight');?>
										</section>
										<section class="col col-2">
								<label class="label"> </label>
								<label class="input mob">
								<span style="float:left; margin-left:-24px;margin-top:23px">kg</span>
								</label>
								</section>
								</div>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary">
									Save
								</button>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" onclick="window.history.back();" class="btn btn-default" type="button">
													Back
												</button>
							</footer>
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>
			</div>

		</div>
		<script type="text/javascript">
                    function avlfeedweightfun(aw,ab) {
                        //alert(aw);
                        document.getElementById("feedaw").value = aw;
                        document.getElementById("feedbrand").value = ab;
                    }
			runAllForms();

			$(function() {

				// Time 
				$('#time').timepicker();

				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						sbid : {
							required : true
						},
						feedtype : {
							required : true
						},
						feedbrand : {
							required : true
						},
						feedweight : {
							required : true,
							number : true
						}
					},

					// Messages for form validation
					messages : {
						sbid : {
							required : 'Please Select Pen Name'
						},
						feedtype : {
							required : 'Please Select Feed Type'
						},
						feedbrand : {
							required : 'Please Select Feed Brand'
						},
						feedweight : {
							required : 'Please Enter Feed Weight',
							number : 'Please Enter Valid Number Only'
						},
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
		

		
