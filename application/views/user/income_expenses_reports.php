<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:70%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-10" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2>Income and Expenses Report for Catfish</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >
							<fieldset>
								<div class="row">
									<section class="col col-6">
									<label class="label">Pond Name</label>
									<label class="select">
										<select name="pond_name">
										<option value="" selected="" disabled="">Please select Pond name</option>
										<?php
											 if ($getpondname != false) {
												 foreach ($getpondname as $getpondnames) {
													echo '<option '.(($_POST['pond_name']==$getpondnames['scid'])?"selected":"").' value="'.$getpondnames['scid'].'">'.$getpondnames['pond_name'].'</option>';
												}
											}
											?>
										</select>
										</section>
										<section class="col col-8">
										
										</section>
									<section class="col col-6">
										<div class="form-group">
											<label>Start Date</label>
											   <div class="input-group">
												<input type="text" name="date" class="form-control datepicker" data-dateformat="dd/mm/yy">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</section>
									<section class="col col-6">
										<div class="form-group">
											<label>End Date</label>
											   <div class="input-group">
												<input type="text" name="date" class="form-control datepicker" data-dateformat="dd/mm/yy">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</section>
									</div>
							</fieldset>
							<div>
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
									</div>	
									<p>&#160;</p>
									<p>&#160;</p>
									
									<!-- end widget edit box -->
				              <center><button type="submit" name="search" style="margin:23px 5px 4px 2px; padding: 4px; background-color: #3399ff" class="btn btn-primary">
                                                        Search
                                                    </button>
                                                    <button  style="margin:23px 5px 4px 2px; padding: 4px; background-color: #a90329; color:#ffffff;" onclick="window.history.back();" class="btn btn-default" type="button">
                                                        Cancel
                                                    </button></center>
								<script type="text/javascript">
		
			runAllForms();

			$(function() {

				// Time 
				$('#time').timepicker();

				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
					
						pond_name : {
							required : true
						},
					
					},

					// Messages for form validation
					messages : {
						pond_name : {
							required : 'Please Select Pond Name'
						},
						},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
				
				
		$(document).ready(function() {
			
			$(".headerDiv").fadeOut();
			$(".searchMe").fadeOut();
			
			/*
			 * BASIC
			 */
			var aoColumns = [
			  				{ "bSortable": false },
				null,
				null,
				
				
				{ "bSortable": false }
			];
			$('#dt_basic').dataTable({
				"sPaginationType" : "bootstrap_full",
				//"aLengthMenu": [[1, 2, 3, -1], [1, 2, 3, "All"]],
				"aoColumnDefs" : [ {
						"bVisible": false, 
						"aTargets": [0]
					} ],
				"aoColumns": aoColumns,
				"aaSorting" : [[0, 'desc']]
			});
	
			/* END BASIC */
			
			//Delete user
			$('.DeleteMe').click(function(e){
				e.preventDefault();
				var gid = $(this).attr('href').split('#')[1];
				jConfirm('Are you sure you want to delete this broiler?', 
						 'Confirmation',
						 function(r) {
							if(r){
								window.location = "<?php echo base_url().'user/taskbroiler/broilerfeed_delete/'; ?>"+gid;	
							}
						 }
				);	
			});
			
			pageSetUp();			
	
			/* Add the events etc before DataTables hides a column */
			$("#datatable_fixed_column thead input").keyup(function() {
				oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
			});
	
			$("#datatable_fixed_column thead input").each(function(i) {
				this.initVal = this.value;
			});
			$("#datatable_fixed_column thead input").focus(function() {
				if (this.className == "search_init") {
					this.className = "";
					this.value = "";
				}
			});
			$("#datatable_fixed_column thead input").blur(function(i) {
				if (this.value == "") {
					this.className = "search_init";
					this.value = this.initVal;
				}
			});		
			
	
			var oTable = $('#datatable_fixed_column').dataTable({
				"sDom" : "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
				//"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
				"oLanguage" : {
					"sSearch" : "Search all columns:"
				},
				"bSortCellsTop" : true
			});		
				
			/* TABLE TOOLS */
			$('#datatable_tabletools').dataTable({
				"sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
				"oTableTools" : {
					"aButtons" : ["copy", "print", {
						"sExtends" : "collection",
						"sButtonText" : 'Save <span class="caret" />',
						"aButtons" : ["csv", "xls", "pdf"]
					}],
					"sSwfPath" : "<?php echo base_url(); ?>js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
				},
				"fnInitComplete" : function(oSettings, json) {
					$(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
						$(this).addClass('btn-sm btn-default');
					});
				}
			});
		
		/* END TABLE TOOLS */
		
		});
		
				
			});
		</script>
									<header>
									<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
									<h2>Bar Chart</h2>

								</header>


		
		<script type="text/javascript">
		$(document).ready(function() {
			var options = {
	            chart: {
	                renderTo: 'container',
	                type: 'column'
	            },
	            title: {
	                text: 'Project Requests',
	                x: -20 //center
	            },
	            subtitle: {
	                text: '',
	                x: -20
	            },
	            xAxis: {
	                categories: []
	            },
	            yAxis: {
	                min: 0,
	                title: {
	                    text: 'Requests'
	                },
	                labels: {
	                    overflow: 'justify'
	                }
	            },
	            tooltip: {
	                formatter: function() {
	                        return '<b>'+ this.series.name +'</b><br/>'+
	                        this.x +': '+ this.y;
	                }
	            },
                     credits: {
                      enabled: false
                    },

	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'top',
	                x: -10,
	                y: 100,
	                borderWidth: 0
	            }, 
	            plotOptions: {
	                bar: {
	                    dataLabels: {
	                        enabled: true
	                    }
	                }
	            },
	            series: []
	        }
	        
	        $.getJSON("http://ancyinfotech.byethost10.com/farmerscreed/user/incomeexpense_piechart/piechart", function(json) {
				options.xAxis.categories = json[0]['data'];
	        	options.series[0] = json[1];
	        	options.series[1] = json[2];
	        	options.series[2] = json[3];
		        chart = new Highcharts.Chart(options);
	        });
	    });
		</script>
	    <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
		<div id="container" style="min-width: auto; height:300px; margin: 0 auto"></div>
	</body>
</html>
								</div>
							
							
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>


			
											</div>
		
		
