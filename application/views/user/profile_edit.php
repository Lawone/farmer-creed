<!DOCTYPE html>
<html lang="en-us">
    <head>
        <title> Farmerscreed </title>
        <?php echo $this->load->view('user/includes_view'); ?>
    </head>
    <style>
        .copy {

            text-align:center;
            margin: auto;
            color:#000;
            font-family: open sans;
        }
    </style>
    <body id="" class="animated fadeInDown" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
        <?php echo $this->load->view('user/header'); ?>
        <?php echo $this->load->view('user/sidebar_menu'); ?>
        <div id="main"  role="main">

            <!-- MAIN CONTENT -->
            <div id="content" >        
                <article class="col-sm-12 col-md-12 col-lg-12" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
                    
                    <?php
                    if ($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp' . $this->session->flashdata('message') . '</div>';
                    endif;
                    ?>
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
                        <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                
                                data-widget-colorbutton="false"	
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true" 
                                data-widget-sortable="false"
                                
                        -->
                        <header style="color:#fff;background:#3399ff; border-color:#3399ff;">
                            <h2>Profile</h2>				

                        </header>
						
                        <!-- widget div-->
                        <div>
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">
								<form action="" id="smart-form-register" class="smart-form client-form" method="post" >
								<fieldset>
									<section class="col col-6">
									<label class="input"> <span style="color: #fff;padding:3px" class="label label-default"><?php echo set_value('username', isset($profile_e['username']) ? $profile_e['username'] : ''); ?></span>
									</section>
									<section class="col col-6">
									<label class="input"> <i class="icon-append fa fa-envelope"></i>
											<input type="email" name="email" placeholder="email address" style="padding-left: 2.5em" value="<?php echo set_value('email', isset($profile_e['email']) ? $profile_e['email'] : ''); ?>">
											<b class="tooltip tooltip-bottom-right">Please enter your email address</b> </label>
											<?php echo form_error('email'); ?>
									</section>
									<section class="col col-6">
									<label class="input"> <i class="icon-append fa fa-user"></i>

												<input type="text" name="firstname" placeholder="name" style="padding-left: 2.5em" value="<?php echo set_value('firstname', isset($profile_e['firstname']) ? $profile_e['firstname'] : ''); ?>">
												<b class="tooltip tooltip-bottom-right">Please enter your Name</b>
											</label>
											<?php echo form_error('firstname'); ?>
										</section>
										<section class="col col-6">
										<label class="input"> <i class="icon-append fa fa-user"></i> 
												<input type="text" name="farmname" placeholder="farm name" style="padding-left: 2.5em" value="<?php echo set_value('farmname', isset($profile_e['farmname']) ? $profile_e['farmname'] : ''); ?>">
												<b class="tooltip tooltip-bottom-right">Please enter your Farm Name</b>
											</label>
										</section>
											
										<section class="col col-6">
										<label class="input"> <i class="icon-append fa fa-user"></i>
												<input type="text" name="phone" placeholder="phone number" style="padding-left: 2.5em" value="<?php echo set_value('phone', isset($profile_e['phone']) ? $profile_e['phone'] : ''); ?>">
												<b class="tooltip tooltip-bottom-right">Please enter your Phonenumber</b>
											</label>
											<?php echo form_error('phone'); ?>
										</section>	
										<section class="col col-6">
										<label class="textarea"> 
												<textarea name="address1" id="address1" placeholder="farm address" rows="2"><?php echo set_value('address1', isset($profile_e['address1']) ? $profile_e['address1'] : ''); ?></textarea>
												<b class="tooltip tooltip-bottom-right">Please enter your Address</b>
											</label>
										</section>
										
								</fieldset>
								
								<header>
								<span style="color: #71843f;">Change password</span>
							</header>
							<fieldset>
									<section>
										<label class="label">Current password</label>
										<label class="input">
											<input type="password" name="current_password" id="current_password" value="<?php echo set_value('current_password'); ?>">
										</label>
										<?php echo form_error('current_password'); ?>
									</section>							
								<div class="row">
									<section class="col col-6">
										<label class="label">Password&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="" rel="popover-hover" data-placement="right" data-original-title="Change password" data-content="If you want to change the password and enter the new password here. Otherwise it will be a blank."><i class="fa fa-question-circle" style="font-size:15px"></i></a></label>
										<label class="input">
											<input type="password" name="password" id="password" value="">
										</label>
										<?php echo form_error('password'); ?>
									</section>
									<section class="col col-6">
										<label class="label">Confirm password</label>
										<label class="input">
											<input type="password" name="confirm_password" id="confirm_password" value="">
										</label>
										<?php echo form_error('confirm_password'); ?>
									</section>
								</div>
							</fieldset>

									<button type="submit" name="register" class="btn btn-primary" style="float:right; border-color:#2c699d; padding:3.5px 15px; margin: 0 2.5em 1.5em 0; background-color: #3276b1; color:#fff;">
										Change
									</button>
							</form>
                            </div>
						</div>
                        <!-- end widget div -->
						</div>
						<!-- widget div-->
						</article>
			<?php
			$this->load->view('user/footer');
			?>
            </div>
        </div>
		<script type="text/javascript">
			$(document).ready(function()
			{
			
			runAllForms();
			
			// Model i agree button
			$("#i-agree").click(function(){
				$this=$("#terms");
				if($this.checked) {
					$('#myModal').modal('toggle');
				} else {
					$this.prop('checked', true);
					$('#myModal').modal('toggle');
				}
			});
			
			// Validation
			$(function() {
				
				// Validation
				$("#smart-form-register").validate({

					// Rules for form validation
					rules : {
						email : {
							required : true,
							email : true
						},
						current_password : {
							minlength : 8,
							maxlength : 20,
							required: {
								depends: function(element){
									return $("#password").val()!=""
								}
							},
						},
						password : {
							minlength : 8,
							maxlength : 20,
							required: {
								depends: function(element){
									return $("#current_password").val()!=""
								}
							},
						},
						confirm_password : {
							minlength : 8,
							maxlength : 20,
							equalTo : '#password'
						},
						firstname : {
							required : true
						},
						phone: {
							required : true,
							minlength : 10
						},
						/* livestock : {
							required : function(element) {
								 if (!$('#livestock').is(":checked") && $('#livestock').is(":checked")) {		
							return true;
							} 
						},
						},
						livestock : {
							required : function(element) {
								 if (!$('#livestock').is(":checked") && $('#livestock').is(":checked")) {		
							return true;
							} 
						},
						}, */
					},

					// Messages for form validation
					messages : {
						email : {
							required : 'Please enter your email address',
							email : 'Please enter a VALID email address'
						},
						/* current_password : {
							required: {
								depends: function(element){
									return $("#password").val()!=""
								}
							},
						},
						password : {
							required: {
								depends: function(element){
									return $("#current_password").val()!=""
								}
							},
						}, */
						confirm_password : {
							equalTo : 'Please enter the same password as above'
						},
						firstname : {
							required : 'Please enter your name'
						},
						phone: {
							required : 'Please enter your Phone Number',
							minlength: 'Please enter a valid phone no.'
						},
						/* livestock : {
							required : function(element) {
								 if (!$('#livestock').is(":checked") && $('#livestock').is(":checked")) {		
							return 'Please check any one';
							} 
						},
						},
						livestock : {
							required : function(element) {
								 if (!$('#livestock').is(":checked") && $('#livestock').is(":checked")) {		
							return 'Please check any one';
							} 
						},
						}, */
						
					},

					// Ajax form submition
					/* submitHandler : function(form) {
						$(form).ajaxSubmit({
							success : function() {
								$("#smart-form-register").addClass('submited');
							}
						});
					}, */

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});

			});
		});
		</script>
    </body>
</html>