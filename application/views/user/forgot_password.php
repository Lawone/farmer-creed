<!DOCTYPE html>
<html lang="en-us">
	<head>
	<title> Farmerscreed </title>
	<?php echo $this->load->view('user/includes_view'); ?>
	<style>
	#login-header-space {
		display: block;
		float: right;
		margin-top: 15px !important;
		text-align: right;
		vertical-align: middle;
	}
		.copy {
			color:white;
			text-align:center;
			   margin-top: -21px;
			
			font-family: open sans;
		}

	</style>
	</head>
	<body id="login" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<header style="margin-top:0.8em;">
			<!--<span id="logo"></span>-->

			<span id="login-header-space"> <!--<span class="hidden-mobile">Already registered?</span>--> <a href="<?php echo base_url();?>user/dashboard/login" class="btn btn-login">LogIn</a> </span>

		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-5 hidden-xs hidden-sm">
						<!--<h1 class="txt-color-red login-header-big">SmartAdmin</h1>
						<div class="hero">

							<div class="pull-left login-desc-box-l">
								<h4 class="paragraph-header">It's Okay to be Smart. Experience the simplicity of SmartAdmin, everywhere you go!</h4>
								<div class="login-app-icons">
									<a href="javascript:void(0);" class="btn btn-danger btn-sm">Frontend Template</a>
									<a href="javascript:void(0);" class="btn btn-danger btn-sm">Find out more</a>
								</div>
							</div>
							
							<img src="<?php echo base_url();?>img/demo/iphoneview.png" class="pull-right display-image" alt="" style="width:210px">

						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h5 class="about-heading">About SmartAdmin - Are you up to date?</h5>
								<p>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.
								</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h5 class="about-heading">Not just your average template!</h5>
								<p>
									Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi voluptatem accusantium!
								</p>
							</div>
						</div>-->

					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-3">
						
						<div class="no-padding">
						<div style="text-align:center">
						<span id="logo"> <img src="<?php echo base_url();?>img/logo.png" alt="SmartAdmin"> </span>
						</div>						
							<form action="" id="login-form" class="smart-form client-form" method="post">
							<!--<div class="label label-danger" style="margin-top: 9px; padding: 7px; width: 100%; color: rgb(255, 255, 255);">Forgot Password</div>-->
								<?php if($this->session->flashdata('user_login_error')) echo $this->session->flashdata('user_login_error');
								if($this->session->flashdata('success_message')): echo '<div class="alert alert-success" style="margin-top:10px"><button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('success_message').'</div>'; endif;
								if($this->session->flashdata('failure')): echo '<div class="alert alert-danger" style="margin-top:10px"><button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('failure').'</div>'; endif;
								?>
								<fieldset>
									<section>
									<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="username" placeholder="username" style="padding-left: 3em" value="<?php echo set_value('username'); ?>">
											<b class="tooltip tooltip-bottom-right">Please enter your User Name</b> </label>
											<?php echo form_error('username'); ?>
									</section>
									<section>
									<label class="input"><i class="icon-append fa fa-envelope"></i>
											<input type="text" name="email" placeholder="email address" style="padding-left: 3em" value="<?php echo set_value('email'); ?>">
											<b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter email address. We'll email you the new password.</b></label>
											<?php echo form_error('email'); ?>
									</section>
									
								</fieldset>
								<footer style="background: none;">
									<button type="submit" class="btn btn-primary">
										<i class="fa fa-refresh"></i> Reset Password
									</button>
								</footer>
							</form>

						</div>
						
					</div>
				</div>
			</div>

		</div>
		<script type="text/javascript">
			runAllForms();

			$(function() {
				// Validation
				$("#login-form").validate({
					// Rules for form validation
					rules : {
						username : {
							required : true
						},
						email : {
							required : true,
							email : true
						}
					},

					// Messages for form validation
					messages : {
						username : {
							required : 'Please enter your login'
						},
						email : {
							required : 'Please enter your email address',
							email : 'Please enter a VALID email address'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
	</body>
</html>