<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broilersales extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('broilersales_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function broilersales_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pen_name', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('poultry', 'Poultry', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_weight', 'Total Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->broilersales_model->broilersales_add();
			$this->session->set_flashdata('message', 'Broiler Sales has been added successfully.');
			redirect(base_url().'user/broilersales/broilersales_add', 'refresh');
		}
		$data['getpenname']=$this->broilersales_model->getBroilersalespen();
		$this->load->view('user/broilersales_add',$data);
	}
	
	//user edit for notification
	public function broilersales_edit($bsid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('date', 'Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pen_name', 'Pen Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('poultry', 'Poultry', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_weight', 'Total Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->broilersales_model->broilersales_edit($bsid);
			$this->session->set_flashdata('message', 'Broiler Sales has been changed successfully.');
			redirect(base_url().'user/broilersales/broilersales_view', 'refresh');
		}
		$data['broilersales_e']=$this->broilersales_model->getBroilersales($bsid);
		$data['getpenname']=$this->broilersales_model->getBroilersalespen();
		$this->load->view('user/broilersales_edit',$data);
	}
	
	// domain view method
	public function broilersales_view() {
		$this->db->select('bs.bsid,date(bs.date) as date,bs.poultry,bs.total_weight,bs.total_cost,bs.user_type,sb.pen_name,bs.staff_id')->from('tbl_broilersales bs');
		$this->db->join('tbl_structure_broiler sb','bs.pen_name = sb.sbid');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('bs.uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('bs.staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['broilersales_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/broilersales_view', $data);
	}
	
	//user delete method
	public function broilersales_delete($bsid){
		
		$this->broilersales_model->broilersales_delete($bsid);
		$this->session->set_flashdata('message', 'Broiler Sales has been deleted successfully.');
		redirect(base_url().'user/broilersales/broilersales_view', 'refresh');
	}
}
