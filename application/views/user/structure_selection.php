<!DOCTYPE html>
<html lang="en-us">
<head><title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>

</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0 ;min-height: 540px" >

			<!-- MAIN CONTENT -->
			<div id="content" class="container" >

				<div class="row" >
					<!-- NEW COL START -->
					<?php
					if($this->session->flashdata('message')): echo '<div class="alert alert-success" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
					if($this->session->flashdata('error_message')): echo '<div class="alert alert-danger" style="margin: 2% 0 -2% 26%;width: 48%;"><button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('error_message').'</div>'; endif;
		?>
					<center>
						<article class="col-sm-12 col-md-12 col-lg-6" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
				
							<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2 style="font-size: 100%">Please answer the questions to determine the  best structure that suits your budget, location and goals.</h2>
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >
							<fieldset>
									<div>
									<section class="col col-6">
									<label>Is the land rented?</label>
									</section>
									<section class="col col-6">
									 <label>
											<input type="checkbox"  name="land_rented" id="land_rented"  value="1" <?php echo set_checkbox('land_rented', '1',(['land_rented']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
									<section class="col col-6">
									<label>Is the land to be used for other things in future?</label>
									</section>
									<section class="col col-6" style="line-height: 40px;">
										<label>
											<input type="checkbox" name="land_used_other_things" id="land_used_other_things"  value="1" <?php echo set_checkbox('land_used_other_things', '1',(['land_used_other_things']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
									<section class="col col-6">
									<label>Is land in that location expensive?</label>
									</section>
									<section class="col col-6">
										<label>
											<input type="checkbox" name="land_inthat_loc_expensive" id="land_inthat_loc_expensive"  value="1" <?php echo set_checkbox('land_inthat_loc_expensive', '1',(['land_inthat_loc_expensive']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
									<section class="col col-6">
									<label>Do you have access to more land if needed?</label>
									</section>
									<section class="col col-6">
										<label>
											<input type="checkbox" name="access_tomore_land_if_needed" id="access_tomore_land_if_needed"  value="1" <?php echo set_checkbox('access_tomore_land_if_needed', '1',(['access_tomore_land_if_needed']=='1') ? TRUE : FALSE); ?>/>
										</label>
										</section>
									</div>
										</fieldset>
																	<footer>
								<a class="btn btn-primary" href="<?php echo base_url().'user/settings/stocking_system'?>">Next</a>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" class="btn btn-default" type="submit">
													Submit
												</button>
							</footer>
										</form>
										
										
									<!--<script type="text/javascript">
		
			runAllForms();

			$(function() {
			
			
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						land_rented   : {
							required : true
													},
						land_used_other_things : {
							required : true
													},
						land_inthat_loc_expensive : {
							required : true
													},
						access_tomore_land_if_needed : {
							required : true
							},
					},

					// Messages for form validation
					messages : {
						land_rented  : {
							required : 'Please Enter Is the land rented',
							},
						land_used_other_things : {
							required : 'Please Enter Is the land to be used for other things in future',
							},
						land_inthat_loc_expensive : {
							required : 'Please Enter Is land in that location expensive',
							},
						access_tomore_land_if_needed : {
							required : 'Please Enter Do you have access to more land if needed',
							},
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script> -->
		