<?php
Class Structbroiler_model extends CI_Model
{
	
	public function __construct() {
		parent::__construct();
		//$this->load->model('mail_model');
	}
	
	public function add() {
		$broiler_info = array(
			'uid' => $this->session->userdata('uid'),
			'pen_name' => $this->input->post('pen_name'),
			'type' => $this->input->post('type'),
			'size' => $this->input->post('size'),
			'created_by' => $this->session->userdata('username'),
			'created_date' => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('tbl_structure_broiler', $broiler_info);
		$ihid = $this->db->insert_id();
		
	}
	
	//update profile
	public function edit($uid) {
		$broileredit_info = array(
			'uid' => $this->session->userdata('uid'),
			'pen_name' => $this->input->post('pen_name'),
			'type' => $this->input->post('type'),
			'size' => $this->input->post('size'),
			'created_by' => $this->session->userdata('username')
		);
			
		$this->db->where('sbid', $uid);
		$this->db->update('tbl_structure_broiler',$broileredit_info);
		
	}
	public function view() {
		
		$this->db->select('*')->from('tbl_structure_broiler');
		$query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }
	//change profile
	public function getStructbroiler($sbid)
	{
		$this->db->select('*')->from('tbl_structure_broiler')->where('sbid', $sbid);
		$query=$this->db->get();
		return ($query->num_rows() > 0) ? $query->row_array() : false;
	}
	
	public function delete($sbid){
		
		$where = array('sbid' => $sbid);
		$this->db->where($where);
		$this->db->delete('tbl_structure_broiler');
		
	}
	
}
