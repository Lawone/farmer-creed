<!DOCTYPE html>
<html lang="en-us">
<head>
<title> Farmerscreed </title>
<?php echo $this->load->view('user/includes_view'); ?>
</head>
<style>
#login #main {
			background: none !important;
			margin: -11px 0 0;
			min-height: 676px;
			padding-top: 98px;
		}
.textAlignRight {
	text-align:right;
}
</style>
<body id="" class="animated fadeInDown">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<?php echo $this->load->view('user/header'); ?>
		<?php echo $this->load->view('user/sidebar_menu'); ?>
		<div id="main1" role="main" style="background: url('<?php echo base_url(); ?>img/body_bg.jpg') repeat scroll 0;min-height: 540px">

			<!-- MAIN CONTENT -->
			<div id="content" class="container" style="width:62%" >

				<div class="row" >
					<!-- NEW COL START -->
					
					<center>
						<article class="col-sm-12 col-md-12 col-lg-10" style="float:none !important; margin-top: 50px!important;text-align: left !important;">
						<?php if($this->session->flashdata('message')): echo '<div class="alert alert-success"><button class="close" data-dismiss="alert">
						×
					</button>
					<i class="fa-fw fa fa-check"></i>&nbsp&nbsp&nbsp'.$this->session->flashdata('message').'</div>'; endif;
						?>
							<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false" style="box-shadow: 8px 8px 30px rgb(0, 0, 0);">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header style="color:#fff;background:#3399ff; border-color:#3399ff;">
					<h2><?php echo 'Catfish Sorting on ' .date('d/m/Y') ?></h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form action="" id="smart-form-farmer" class="smart-form" method="post" >

							
							<fieldset>
								<div class="row">
									<section class="col col-6">
									<label class="label">From</label>
									<label class="select">
										<select name="from_scid" id="from_scid">
											<option value="" selected="" disabled="">From Pondname</option>
											<?php
											 if ($getpondname != false) {
												 foreach ($getpondname as $getpondnames) {
													echo '<option value="'.$getpondnames['scid'].'">'.$getpondnames['pond_name'].'</option>';
												}
											}
											?>
										</select> <i></i>
										<?php echo form_error('from_scid'); ?>
									</label>
									</section>
									<section class="col col-6">
									<label class="label">To</label>
									<label class="select">
										<select name="to_scid" id="to_scid">
											<option value="" selected="" disabled="">To Pondname</option>
											<?php
											 if ($getpondname != false) {
												 foreach ($getpondname as $getpondnames) {
													echo '<option value="'.$getpondnames['scid'].'">'.$getpondnames['pond_name'].'</option>';
												}
											}
											?>
										</select> <i></i>
								 	</label>
									<?php echo form_error('to_scid'); ?>
									</section>
								</div>
								<div class="row">
									<section class="col col-6">
									<label class="label">No.of Fish</label>
										<label class="input">
											<input type="text" name="no_of_fish" id="no_of_fish" style="text-align:right;" placeholder="0">
										</label>
										<?php echo form_error('no_of_fish'); ?>
									</section>
									<section class="col col-6">
									<label class="label">Total Weight</label>
										<label class="input">
											<input type="text" name="total_weight" id="total_weight" style="text-align:right;" placeholder="0.00">
										</label>
										<?php echo form_error('total_weight'); ?>
									</section>
								</div>
							</fieldset>
							<footer>
								<button type="submit" class="btn btn-primary">
									Save
								</button>
								<button style="color:#fff;border-color:#a90329;background-color:#a90329;border-radius:3px !important; border-radius:3px;" onclick="window.history.back();" class="btn btn-default" type="button">
													Back
												</button>
							</footer>
						</form>						
						
					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
				
						</article>
						</center>
						<!-- END COL -->
				</div>
			</div>

		</div>
		<script type="text/javascript">
		
			runAllForms();

			$(function() {
			
			
				// Validation
				$("#smart-form-farmer").validate({
					// Rules for form validation
					rules : {
						from_scid : {
							required : true
						},
						to_scid : {
							required : true
						},
						no_of_fish : {
							required : true,
							number: true
						},
						total_weight : {
							required : true,
							number: true
						}
					},

					// Messages for form validation
					messages : {
						from_scid : {
							required : 'Please enter From pond'
						},
						to_scid : {
							required : 'Please enter To pond'
						},
						no_of_fish : {
							required : 'Please enter No.of Fish',
							number: 'Please enter valid number only'
						},
						total_weight : {
							required : 'Please enter Total weight',
							number: 'Please enter valid number only'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
			});
		</script>
		<?php
			$this->load->view('user/footer');
		?>
		
