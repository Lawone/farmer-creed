
<?php

error_reporting(0);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class incomeexpenses_barchart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->app_access->user(); // check access permission for user
        $this->load->model(array('catfishsales_model'));
    }

    // Registration
    public function index() {



        $this->load->view('user/income_expenses_reports for barchart');
    }

    // domain view method
    public function barchart_view($pond_name, $start_date, $end_date) {
	
			$f_arr = array();
            $date_arr = array();
			$incomeQuery = "SELECT sum(`total_cost`) as `total_cost`, date_format(created_date,'%M') as month FROM `tbl_catfishincome` WHERE `uid` = '".$this->session->userdata('uid')."' and ";
			$feedQuery = "SELECT con.fpid,sum(con.feedweight) as feedweight,date_format(con.created_date,'%M') as month,pur.weight,pur.total_cost,pur.no_of_bags FROM tbl_catfishfeed con, tbl_feedpurchased pur WHERE";
			$salaryQuery = "SELECT sum(amount) as amount,date_format(date,'%M') as month FROM tbl_salaries WHERE";
			$totalStockQuery = "SELECT sum(total_stocked) as total_stock FROM tbl_livestock_catfish WHERE";
			$totalStockPondQuery = "SELECT sum(total_stocked) as total_pondstock FROM tbl_livestock_catfish WHERE";
			$fingerlingQuery = "SELECT date_format(date,'%M') as month,sum(total_cost) as fingerlingAmt FROM tbl_fingerlingpurchased WHERE";
			$otherExpQuery = "SELECT date_format(date,'%M') as month,sum(amount) as othExpAmt FROM tbl_catfishgenpurchased WHERE";
			
			if ($pond_name != '' && $start_date != '' && $end_date != '') {
				$incomeQuery .=  "(date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') and pond_name = '".$pond_name."' ";
				$feedQuery .=  "  `scid` = '".$pond_name."' and con.uid = '".$this->session->userdata('uid')."' AND (date(con.created_date) BETWEEN '".$start_date."' AND '".$end_date."') and con.fpid = pur.fpid and con.uid = pur.uid ";
				$salaryQuery .=  "  uid = '".$this->session->userdata('uid')."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
				$totalStockQuery .=  "  uid = '".$this->session->userdata('uid')."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
				$totalStockPondQuery .=  "  uid = '".$this->session->userdata('uid')."' and scid = '".$pond_name."' AND (date(created_date) BETWEEN '".$start_date."' AND '".$end_date."') ";
				$fingerlingQuery .=  "  `pond_name` = '".$pond_name."' and uid = '".$this->session->userdata('uid')."' AND (date(date) BETWEEN '".$start_date."' AND '".$end_date."') ";
				$otherExpQuery .=  "  `uid` = '".$this->session->userdata('uid')."' AND (date(date) BETWEEN '".$start_date."' AND '".$end_date."') ";
			}
			
			$incomeQuery .= "group by date_format(created_date,'%M')";
			$feedQuery .= "group by con.fpid,date_format(con.created_date,'%M')";
			$salaryQuery .= "group by date_format(date,'%M')";
			$fingerlingQuery .= "group by date_format(date,'%M')";
			$otherExpQuery .= "group by date_format(date,'%M')";
			
			//echo $incomeQuery.'<br/>'.$feedQuery.'<br/>'.$salaryQuery.'<br/>'.$salaryCountQuery.'<br/>'.$totalStockQuery.'<br/>'.$totalStockPondQuery.'<br/>'.$fingerlingQuery.'<br/>'.$otherExpQuery;
			
			$incomeData = $this->db->query($incomeQuery);
			$incomeResult = $incomeData->result_array();
			
			$totalExp = 0;
			foreach ($incomeResult as $key => $value) {
				if(in_array($value['month'], $date_arr)){
						$now_count = array_search ($value['month'], $date_arr);
					}  else {
						$now_count = count($f_arr);
						$date_arr[$now_count] = $value['month'];
						$f_arr[$now_count]['expAmt'] = 0;
					}
				//echo $value['feedweight'].'---'.$value['weight'].'----'.$value['total_cost'];
				$f_arr[$now_count]['month'] = $value['month'];
				$f_arr[$now_count]['incAmt'] = number_format($value['total_cost'],2,'.','');				
			}
			
			$query = $this->db->query($feedQuery);
			$result = $query->result_array();
			
			$totalExp = 0;
			foreach ($result as $key => $value) {
				$feedCost = ($value['feedweight'] / ($value['no_of_bags'] * $value['weight'])) * $value['total_cost'];
				if(in_array($value['month'], $date_arr)){
						$now_count = array_search ($value['month'], $date_arr);
						$f_arr[$now_count]['expAmt'] += number_format($feedCost,2,'.','');
					}  else {
						$now_count = count($f_arr);
						$date_arr[$now_count] = $value['month'];
						$f_arr[$now_count]['incAmt'] = 0;
						$f_arr[$now_count]['expAmt'] = number_format($feedCost,2,'.','');
					}
				//echo $value['feedweight'].'---'.$value['weight'].'----'.$value['total_cost'];
				
				$f_arr[$now_count]['month'] = $value['month'];
				
				
				
			}
	  

			//echo $sql_query1 .= " GROUP BY date( `created_date` )";

			//Salary expense calculation
			$query1 = $this->db->query($salaryQuery);
			$result1 = $query1->result_array();
						
			//get user total stock
			$stockTotQuery = $this->db->query($totalStockQuery);
			$stockTotData = $stockTotQuery->row_array();
			$totStock = ($stockTotQuery -> num_rows() > 0) ? $stockTotData['total_stock'] : 0;
			
			//get user with pond total stock
			$stockPondTotQuery = $this->db->query($totalStockPondQuery);
			$stockPondTotData = $stockPondTotQuery->row_array();
			$totPondStock = ($stockPondTotQuery -> num_rows() > 0) ? $stockPondTotData['total_pondstock'] : 0;
			if($totStock > 0 && $totPondStock > 0){
				foreach ($result1 as $key => $value) {
					$salCost1 = ($value['amount']) / $totStock;
					$salCost = number_format(($salCost1 * $totPondStock),2,'.','');
					//echo $value['amount'].'---'.$salCount.'----'.$totStock.'----'.$totPondStock.'<br/>';
					if(in_array($value['month'], $date_arr)){
						$now_count = array_search ($value['month'], $date_arr);
						$f_arr[$now_count]['expAmt']+=$salCost;
					}  else {
						$now_count = count($f_arr);
						$date_arr[$now_count] = $value['month'];
						$f_arr[$now_count]['incAmt'] = 0;
						$f_arr[$now_count]['expAmt'] = $salCost;
					}
					
					$f_arr[$now_count]['month'] = $value['month'];
					
					
										   
				}
			}
			
			$query3 = $this->db->query($fingerlingQuery);
			$result3 = $query3->result_array();
			if($totStock > 0 && $totPondStock > 0){
				foreach ($result3 as $key => $value) {
					if(in_array($value['month'], $date_arr)){
						$now_count = array_search ($value['month'], $date_arr);
						$f_arr[$now_count]['expAmt']+=number_format($value['fingerlingAmt'],2,'.','');
					}  else {
						$now_count = count($f_arr);
						$date_arr[$now_count] = $value['month'];
						$f_arr[$now_count]['incAmt'] = 0;
						$f_arr[$now_count]['expAmt'] = number_format($value['fingerlingAmt'],2,'.','');
					}
					
					$f_arr[$now_count]['month'] = $value['month'];
					
					
										   
				}
			}
			
			$query4 = $this->db->query($otherExpQuery);
			$result4 = $query4->result_array();
			foreach ($result4 as $key => $value) {
				$othCost = ($value['othExpAmt'] / $totStock) * $totPondStock;
				//echo $value['othExpAmt'].'---'.$totStock.'----'.$totPondStock;
				if(in_array($value['month'], $date_arr)){
					$now_count = array_search ($value['month'], $date_arr);
					$f_arr[$now_count]['expAmt']+=number_format($othCost,2,'.','');
				}  else {
					$now_count = count($f_arr);
					$date_arr[$now_count] = $value['month'];
					$f_arr[$now_count]['incAmt'] = 0;
					$f_arr[$now_count]['expAmt'] = number_format($othCost,2,'.','');
				}
				
				$f_arr[$now_count]['month'] = $value['month'];
				
				
									   
			}
			
			$series2 = array();
			$series2['name'] = 'Expenses';

			$series1 = array();
			$series1['name'] = 'Income';
			$rows = array();
			
			foreach ($f_arr as $f_result) {
				$category['data'][] = $f_result['month'];
				$series1['data'][] = $f_result['incAmt'];
				$series2['data'][] = $f_result['expAmt'];
			}
			array_push($rows, $category);
			array_push($rows, $series1);
			array_push($rows, $series2);
			array_push($rows, $series3);

        //echo "<pre>";                print_r($data); echo "<pre>";  

        print json_encode($rows, JSON_NUMERIC_CHECK);
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
