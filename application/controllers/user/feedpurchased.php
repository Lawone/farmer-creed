<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedpurchased extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->app_access->user(); // check access permission for user
		$this->load->model(array('feedpurchased_model'));
	}
	
	// Registration
	public function index() {
		
	}
	
	//user add for notification
	public function feedpurchased_add() {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('no_of_bags', 'No Of Bags', 'trim|required|xss_clean');
		$this->form_validation->set_rules('size', 'Size', 'trim|required|xss_clean');
		$this->form_validation->set_rules('weight', 'Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
				
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->feedpurchased_model->feedpurchased_add();
			$this->session->set_flashdata('message', 'Catfish Feed Purchased has been added successfully.');
			redirect(base_url().'user/feedpurchased/feedpurchased_add', 'refresh');
		}
		$this->load->view('user/feedpurchased_add');
	}
	
	//user edit for notification
	public function feedpurchased_edit($fpid) {
		// loading libraries and helpers
		$this->load->library(array('form_validation'));
		$this->load->helper(array('form'));
		
		// validation rules
		$this->form_validation->set_rules('no_of_bags', 'No Of Bags', 'trim|required|xss_clean');
		$this->form_validation->set_rules('size', 'Size', 'trim|required|xss_clean');
		$this->form_validation->set_rules('weight', 'Weight', 'trim|required|xss_clean');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_cost', 'Total Cost', 'trim|required|xss_clean');
		
				
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
		
		// Form validation action
		if($this->form_validation->run() == TRUE){
			$this->feedpurchased_model->feedpurchased_edit($fpid);
			$this->session->set_flashdata('message', 'Catfish Feed Purchased has been changed successfully.');
			redirect(base_url().'user/feedpurchased/feedpurchased_view', 'refresh');
		}
		$data['feedpurchased_e']=$this->feedpurchased_model->getFeedpurchased($fpid);
		$this->load->view('user/feedpurchased_edit',$data);
	}
	
	// domain view method
	public function feedpurchased_view() {
		$this->db->select('*')->from('tbl_feedpurchased');
		if($this->session->userdata('user_type') == 0)
		{
			$this->db->where(array('uid' => $this->session->userdata('uid')));
		}
		else
		{
			$this->db->where(array('staff_id' => $this->session->userdata('uid')));
		}
		$query = $this->db->get();
		$data['feedpurchased_v'] = ($query->num_rows() > 0) ? $query->result_array() : false;

		$this->load->view('user/feedpurchased_view', $data);
	}
	
	//user delete method
	public function feedpurchased_delete($fpid){
		
		$this->feedpurchased_model->feedpurchased_delete($fpid);
		$this->session->set_flashdata('message', 'Catfish Feed Purchased has been deleted successfully.');
		redirect(base_url().'user/feedpurchased/feedpurchased_view', 'refresh');
	}
}
